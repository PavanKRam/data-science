MP Cannibalisation Documentation
===================================

Current version uses similarity score based on hardcoded values of features


- Price Slabs: [0-2], [1-3], [2-4], [3-5], [4-6], [5-7], [6-8], [7-9], [9+]
- Body Type: [Hatchback], [Sedan], [MPV], [SUV]
- Fuel Type: [Petrol], [Diesel]
- Age slabs: [0,1,2], [3,4],[5,6,7],[7+]
- Owners: 1, 1+
- Mileage: [0-20000], [20001-40000], [40001-60000],[60001-80000], [80000+]
- Colours: [White, Silver, Grey], [Black, Others], {Baaki Saari color_ids]
- Model
- Variant


A threshold on Similarity Score and Price difference is used to label listings as Cannibalising or Non-Cannibalising.

### Cannibalisation V2
For every TD listing, MP listings will be judged cannibalising or non-cannibalising on four levels:

- Variant
- Model x Fuel x Transmission Type
- Model x Fuel
- Model
- Price x Body Type

### Level I: Variant
Primary dof: Price P, Manufacturing Year Y
Secondary dof: Mileage, Colour, Owner, TrueScore

Primary dof difference:
Year difference: -1 to +1
Price difference: <+15%

Secondary dof extremes:
Mileage difference: +35k kms
Owner difference: > 2
TrueScore: Less than lowest TD integer rating in that cluster

Algorithm:

* Consider all MP listings eligible for removal satisfying Primary DOF difference criteria
* Allow MP listings with Secondary DOF extremes to stay
* Adjust the price for year difference, mileage difference, owner difference & TrueScore difference (Price adjustment explained below)
* If adjusted price is less than TD listing’s price, expire them.


### Level II: Model x Fuel x Transmission Type
* Adjust price for variant change using ORP normalisation 
* Different difference thresholds

### Level III: Model x Fuel
* Adjust price for variant change using ORP normalisation 
* Different difference thresholds

### Level IV: Model
* Adjust price for variant change using ORP normalisation 
* Different difference thresholds


### Level V: Price x Body Type

* Price Slab of (P-20%, P+10%)
* Adjust price for variant change using ORP normalisation 
* Different difference thresholds


#### Feedback loop on Buy Conversion

Each city’s target buy conversion will be fed and based on the ongoing buy conversion, auto-expiry day b/w 150-270 days will be chosen.
TD LPS along with Inv. CRF% will be a factor here.
If ongoing < target: 270 days
ongoing> target: Reduce day linearly


#### Price Adjustment:
Find the TD listing with nearest year (if multiple TD listings present having same manufacturing year, take one with max Price (ORP normalised)). Compute the rate △Price / △Year and use it.
If no nearest listing exist, use the basic depreciation rate.


Use TruePrice depreciation curves for mileage, owners & TrueScore (Condition).

Detailed Explanation:

* Single point available at each level

Extend the Price-Year graph in either direction using the Pricing Engine to calculate depreciation for each model per year in either direction (average depreciation in either direction) and keeping in mind that at age = 1 the drop is higher than that of other years. After building this graph, we are shifting this graph up by a Blanket percentage (3%) as we want to take in all the points that are just above are points and may be cannibalising. Henceforth, this graph is mentioned as the TP Curve (TruePrice Curve).

Primary DoF:
We have 2 Primary DoF - Price and Year
We are more concentrated towards Price so we are having 2 Price limits:
P_Low = -15%
P_High = +15%

Based on these we are calculating the upper and lower limits of Year that we want to incorporate using the TP Curve that we have. These are called Y_Low ang Y_High. We are also keeping min, max hard limits on these so that for very new and very old cars, Y_Low and Y_High have at least some value (1) for new cars and also so that for old cars these don’t deviate very much from the year of the TD car.
Now the area under the curve is the area that has potentially cannibalising cars. Some of these are hard cannibalising (we are expiring these) and some are soft cannibalising (we are pushing these down). For defining these, we are defining two limits, Pricing Criteria High and Low (PC_High and PC_Low). These lines start from the P_High, Y_High intersection of the higher side and P_Low, Y_Low intersection of the lower side.

All the cars below the PC_High line are expired. 
All cars in below the TP curve and in the intersection between P_Low, P_High, Y_low, Y_high are also expired.
All the other cars below the TP curve are Pushed Down.

* Multiple points available at each level

When multiple points are available, we will still be following the same protocol of creating a TP curve but the curve creation will be a little different.

Firstly, for each year we will take the car with the max price (after ORP adjustment). Then we will connect these points with a straight line. For the rightmost and leftmost points, we will extend the curves same as the previous case. Blanket will be applicable here also.


Now for each point we will repeat the same procedure we did in the first case, ie, using P_High and P_Low for each point, find their Y_High and Y_Low. 


Here the hard limits on Y_High and Y_Low are 2 Years as this will be needed when the graph is decreasing because we won’t be able to find an intersection between P_High, P_Low and TP for many points.


PC_High and PC_Low will apply at the extreme points. 


Around each point at the TP curve, we will find the area between Y_Low and Y_High below the curve for that point and mark the cars in there as expired. (Area 1)


For all other points, we will count PC_High lines above the MP point and points with greater than 2 lines passing above it is expired and all the other point below the curve are pushed down.


***
`Not currently in use,for future purposes`

For the TD car with the lowest price of the whole curve, we will create PC_High at this point till the end of the graph and mark all the cars below that as expired.
For the rightmost and leftmost cars, the cars between TP and PC_High and PC_Low and TP will be pushed down.
Also, between different Area 1’s of different TD cars, there are cars that may or may not be cannibalising other cars. We don’t want cannibalisation to be very aggressive, so we will mark the cars in these areas as pushed down. (Area 2)
*** 

The basic construct of the algo is above and is same for all the levels. The difference will be that for levels other than variants; before plotting the graph, all the cars will be normalised on the basis of ORP and their criterias will become stringent because as we go up as the number of cars increase at each level.