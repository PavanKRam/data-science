import numpy as np
import pandas as pd

from cannibalisation.utils.helpers import get_plot_extrema, get_intersection_points, CbData
from dynamic_pricing_code.helpers import add_log


class DepreciationData:
    """
    Creates depreciation data for all TD segments Price X Year wise
    td_data : Primary TD data
    depreciation_data : City and model wise depreciation data
    price_high_percentage : Maxima price high limit
    """

    td_data = max_ages = pd.DataFrame()
    depreciation_factors = {}
    price_factor = 0

    def __init__(self):
        self.cb_params = CbData()
        self.current_year = self.cb_params.today.year

    def set_cluster_helper_data(self, primary_td_data, model_depreciation_factors, city, level):
        """
        Sets td and model depreciation data
        :param primary_td_data: TD data frame
        :param model_depreciation_factors: City and model wise price depreciation data : {year_diff: price_%}
        :param city: City Id
        :param level: Name of the level
        """
        price_high_percentage = self.cb_params.cannibalisation_params['levels'][
            level]['price_high_limit_percentage'][str(city)]
        self.price_factor = 1 + int(price_high_percentage) * 0.01
        self.td_data = primary_td_data
        self.depreciation_factors = {int(key): value for key, value in model_depreciation_factors.items()}

    def get_depreciation_frame(self, car_price, age, maxima=False):
        """
        Fetches depreciation data year wise and calculates for subsequent ages
        :param car_price: TD normalised price for that age
        :param age: Year
        :param maxima: If calculating for maxima appreciation data or minima depreciation data
        :return: list of extrema depreciation/appreciation data frames
        """
        age_in_years = self.current_year - age
        # If maxima get appreciation data till mp max year, else get depreciation data till mp min year
        if maxima:
            dep_data_below_age = {key: val for key, val in sorted(self.depreciation_factors.items(), reverse=True)
                                  if key <= age_in_years}
        else:
            dep_data_below_age = {key: val for key, val in sorted(self.depreciation_factors.items())
                                  if key >= age_in_years}

        dep_data_values, dep_data_keys = list(dep_data_below_age.values()), list(dep_data_below_age.keys())
        """
        Get consecutive depreciation data ratio, year wise
        Set age point as first value and calculate subsequent year data using depreciation factor ratios
        """
        base_val = dep_data_values[0]
        depr_diff = [round((s / base_val) * car_price) for s in dep_data_values]

        dep_data = pd.DataFrame()
        dep_data['age'] = self.current_year - pd.Series(dep_data_keys).astype('int')
        dep_data['dep_price'] = pd.Series(depr_diff).round()
        return dep_data

    @staticmethod
    def get_intersection_limited_data(intersection_points, depreciation_data, col_name):
        if len(intersection_points) > 1:
            intersecting_age, intersecting_price = intersection_points[1][0], intersection_points[1][1]
            depreciation_data = depreciation_data.append({'age': intersecting_age, col_name: intersecting_price},
                                                         ignore_index=True)
            depreciation_data = depreciation_data[depreciation_data.age <= intersecting_age].sort_values('age')

        return depreciation_data

    def get_price_high_cannibalising_segments(self):
        price_high = self.td_data.normalised_price * self.price_factor
        td_interpolated_data = self.get_td_interpolated_data()
        all_segments = []
        for age, price in zip(self.max_ages, price_high):
            app_data = pd.DataFrame()
            app_data['age'] = list(range(age + 1, self.current_year + 1))
            app_data['price_high'] = price
            app_data = app_data.astype('float').merge(td_interpolated_data[['age', 'normalised_price']],
                                                      on='age', how='left')
            app_data['price_high'] = app_data[['price_high', 'normalised_price']].min(axis=1)
            app_data.drop('normalised_price', axis=1, inplace=True)
            all_segments.append(app_data)

        return all_segments

    def get_minimas_depreciation_data(self, global_age):
        """
        Finds depreciation data for global minima and minimum TD age
        global_age: global minima age
        :return: List of depreciation data frames
        """
        minimas_dep_data = {}
        min_td_age = self.td_data.age.min()
        for min_age in np.unique([global_age, min_td_age]):
            listing_price = self.td_data[self.td_data['age'] == min_age]['normalised_price'].iloc[0]
            dep_min_data = self.get_depreciation_frame(listing_price, min_age)
            minimas_dep_data[min_age] = dep_min_data
        return minimas_dep_data

    def get_dep_max_data(self, age):
        """
        Calculates maxima's appreciation data
        age: maxima ages
        :return: List of appreciation data frames
        """
        listing_price = self.td_data[self.td_data['age'] == age]['normalised_price'].iloc[0]
        dep_max_data = self.get_depreciation_frame(listing_price, age, maxima=True)
        price_high_age = np.ceil(np.interp(listing_price * self.price_factor,
                                           dep_max_data.dep_price.values, dep_max_data.age.values)).astype('int')
        dep_max_data.loc[dep_max_data.age >= price_high_age, 'dep_price'] = listing_price * self.price_factor
        return dep_max_data

    def get_dep_data_till_intersection(self, maximas_dep_data):
        """
        Limits maxima's appreciation data till min(TD intersection points, Maxima intersection points)
        maximas_dep_data: Maxima appreciation data
        :return: Limited appreciation data frame
        """
        intersecting_dep_data = []
        for frame_index in range(len(maximas_dep_data) - 1):
            dep_max_data = maximas_dep_data[frame_index]
            td_intersection_points = get_intersection_points(dep_max_data, self.td_data[['age', 'normalised_price']])
            max_dep_intersection_points = self.get_dep_max_intersection_points(dep_max_data,
                                                                               maximas_dep_data[frame_index + 1:])
            intersection_points = td_intersection_points + max_dep_intersection_points
            intersection_points = sorted(intersection_points, key=lambda x: x[0])
            dep_max_data = self.get_intersection_limited_data(intersection_points, dep_max_data, 'dep_price')
            intersecting_dep_data.append(dep_max_data)
        intersecting_dep_data.append(maximas_dep_data[-1])
        return intersecting_dep_data

    def get_minimas_data(self, minima):
        """
        Calculates overall appreciation/depreciation data of all minimas in TD curve
        minima: TD minima data
        :return: List of all minima data, List of lower limit minima data (Data Frames)
        """
        add_log("MINIMA DEPRECIATION CALLED")
        global_minima_index = minima.idxmin()
        global_minima_age = self.td_data.loc[global_minima_index, 'age']
        minima_depreciation_data = self.get_minimas_depreciation_data(global_minima_age)
        minima_lower_limit = self.get_minima_lower_limit_data(minima.index)
        overall_minima_data = list(minima_depreciation_data.values()) + minima_lower_limit
        add_log("MINIMA DEPRECIATION COMPLETED")
        return overall_minima_data, minima_lower_limit

    def get_maximas_data(self, maxima):
        """
        Calculates overall appreciation data of all maximas in TD curve
        maxima: TD maxima data
        """
        add_log("MAXIMA APPRECIATION CALLED")
        maxima_indices = list(maxima.index)
        max_age_index = self.td_data.age.idxmax()
        max_dep_data = {}
        for max_age in np.unique(self.td_data.loc[maxima_indices + [max_age_index], 'age']):
            max_dep_data[max_age] = self.get_dep_max_data(int(max_age))
        dep_data = self.get_dep_data_till_intersection(list(max_dep_data.values()))
        add_log("MAXIMA APPRECIATION COMPLETED")
        return dep_data

    def get_overall_depreciation_data(self, max_ages):
        """
        Returns overall depreciation/appreciation data for TD extrema's
        """
        add_log("DEPRECIATION FUNCTION CALLED")
        self.max_ages = max_ages
        td_minima, td_maxima = get_plot_extrema(self.td_data.normalised_price)
        minima_data, minima_lower_limit = self.get_minimas_data(td_minima)
        segments_appr_data = self.get_price_high_cannibalising_segments()
        maximas_appreciation_data = self.get_maximas_data(td_maxima)
        depr_data = maximas_appreciation_data + minima_data + segments_appr_data
        add_log("DEPRECIATION FUNCTION EXECUTED")
        return depr_data, minima_lower_limit, segments_appr_data

    @staticmethod
    def get_dep_max_intersection_points(dep_data, maximas_dep_data):
        """
        Finds intersection points of maxima data with consecutive maxima's appreciation data
        :param dep_data: Under consideration maxima data
        :param maximas_dep_data: Consecutive maxima appreciation data
        :return: List of intersection points : [(x_coordinate, y_coordinate)]
        """
        max_dep_intersection_points = []
        for dep_frame in maximas_dep_data:
            if len(dep_frame) > 1:
                dep_intersection_points = get_intersection_points(dep_data, dep_frame)
                max_dep_intersection_points.extend(dep_intersection_points)
        return max_dep_intersection_points

    def get_minima_lower_limit_data(self, minima_indices):
        """
        Finds minima's (except global) depreciation data till previous available TD point
        :param minima_indices: Indices of minima's in TD frame
        :return: List of depreciation data frames
        """
        age_indices = np.setdiff1d(minima_indices, [0])
        minima_data = self.td_data.loc[age_indices, ['age', 'normalised_price']]

        # Get prev minima data only when there is no previous TD point available
        minima_dep_list = []
        for i in age_indices:
            h_age = self.max_ages.loc[i-1]
            prev_age = minima_data.loc[i, 'age'] - 1
            normalised_price = minima_data.loc[i, 'normalised_price']

            if prev_age > h_age:
                age_range = list(range(h_age, prev_age + 1))
                price = [normalised_price] * len(age_range)
                minima_y_low_data = pd.DataFrame()
                minima_y_low_data['age'] = age_range
                minima_y_low_data['dep_price'] = price
                minima_dep_list.append(minima_y_low_data)

        return minima_dep_list

    def get_td_interpolated_data(self):
        td_ages = self.td_data.age
        min_td_age, max_td_age = td_ages.min(), td_ages.max()
        min_listing_price = self.td_data[self.td_data['age'] == min_td_age]['normalised_price'].iloc[0]
        min_td_age_dep = self.get_depreciation_frame(min_listing_price, min_td_age)
        max_td_age_dep = self.get_dep_max_data(int(max_td_age))
        td_dep_data = self.td_data.iloc[:, :2].values
        columns = ['age', 'normalised_price']
        all_age_data = np.concatenate([td_dep_data, min_td_age_dep.values, max_td_age_dep.values], axis=0)
        dep_data = pd.DataFrame(all_age_data, columns=columns)
        dep_data = dep_data.sort_values('age').drop_duplicates().reset_index(drop=True)
        return dep_data
