import numpy as np

from cannibalisation.utils.constants import LISTINGS_DATA_FETCH_SUBQUERY, EXPIRED_MP_DATA_QUERY, ACTIVE_MP_DATA_QUERY, \
    SUBSCRIBED_SELLERS_QUERY, AUTO_EXPIRY_QUERY
from cannibalisation.utils.helpers import CbData
from dynamic_pricing_code.helpers import sql_result, add_log


class CannibalisationListingsData:

    def __init__(self):
        self.cb_params = CbData()
        self.status_days_range = self.cb_params.cannibalisation_params['global_parameters']['status_days_range']

    def get_days_range_subquery(self, status_type):
        """
        Generated city wise days range subquery on basis of provided condition
        :param status_type: Type of subquery
        :return: subquery string
        :rtype: str
        """
        status_days_range = self.status_days_range
        start_days = status_days_range['start_day']
        end_days = status_days_range['end_day']
        # City wise condition for count of cities available
        city_condition_string = 'WHEN %s THEN %s ' * len(end_days)
        start_subquery = 'BETWEEN CASE lo.city_id {beg_query} END AND '
        end_subquery = 'CASE lo.city_id {end_query} END'
        subquery_string = start_subquery + end_subquery
        stacked_end_data = np.hstack(list(end_days.items()))
        end_days_condition = city_condition_string % tuple(stacked_end_data)

        if status_type == 'auto_expiry':
            # 'CASE lo.city_id WHEN 1 THEN 270 END'
            subquery_string = end_subquery.format(end_query=end_days_condition)
        else:
            # 'BETWEEN CASE lo.city_id WHEN 1 THEN 0 END AND CASE lo.city_id WHEN 1 THEN 270 END'
            stacked_beg_data = np.hstack(list(start_days[status_type].items()))
            start_days_condition = city_condition_string % tuple(stacked_beg_data)
            subquery_string = subquery_string.format(beg_query=start_days_condition, end_query=end_days_condition)
        return subquery_string

    def get_mp_listings_to_be_activated(self):
        """
        :return: To be activated MP listings data frame
        """
        active_subquery = self.get_days_range_subquery('to_active')
        expired_mp_frame = sql_result(EXPIRED_MP_DATA_QUERY.format(active_subquery=active_subquery,
                                                                   data_fetch_subquery=LISTINGS_DATA_FETCH_SUBQUERY))
        return expired_mp_frame

    def get_mp_listings_to_be_expired(self):
        """
        :return: To be expired MP listings data frame
        """
        expiry_subquery = self.get_days_range_subquery('to_expire')
        active_mp_frame = sql_result(ACTIVE_MP_DATA_QUERY.format(expiry_subquery=expiry_subquery,
                                                                 data_fetch_subquery=LISTINGS_DATA_FETCH_SUBQUERY))
        subscribed_sellers_listings = self.get_subscribed_seller_listings()
        active_mp_frame = active_mp_frame[
            ~active_mp_frame['id'].isin(subscribed_sellers_listings)].reset_index(drop=True)

        return active_mp_frame

    def auto_expire_listings(self):
        """
        :return: Listings data frame needed to be auto expired
        """
        expiry_days_limit_subquery = self.get_days_range_subquery('auto_expiry')
        seller_listings = self.get_subscribed_seller_listings()

        to_expire_listings = sql_result(AUTO_EXPIRY_QUERY.format(expiry_days_limit_subquery=expiry_days_limit_subquery))
        to_expire_listings = to_expire_listings[~to_expire_listings['id'].isin(seller_listings)]
        to_expire_listings.drop('city_id', 1, inplace=True)
        to_expire_listings['status'], to_expire_listings['source'] = ["'expired'", "'cron'"]
        return to_expire_listings

    def get_listings_data(self):
        """
        :returns: List of [active_td, expired_mp, active_mp] data frames, Auto expiry dataframe
        """
        add_log("DATA PREPARATION CALLED")
        active_td_data = self.get_active_td_listings()
        expired_mp_listings = self.get_mp_listings_to_be_activated()
        active_mp_listings = self.get_mp_listings_to_be_expired()
        listings_data = [active_td_data, expired_mp_listings, active_mp_listings]
        auto_expiry_data = self.auto_expire_listings()
        add_log("DATA PREPARATION COMPLETED")
        return listings_data, auto_expiry_data

    @staticmethod
    def get_active_td_listings():
        """
        :return: Active TD listings data frame
        """
        td_active_query = """{data_fetch_subquery} WHERE l.is_inventory = 1 AND l.inventory_type_id in (1, 2, NULL) AND 
        l.status IN ('active' , 'refurbishing') GROUP BY l.id"""
        active_td_listings = sql_result(td_active_query.format(data_fetch_subquery=LISTINGS_DATA_FETCH_SUBQUERY))
        return active_td_listings

    @staticmethod
    def get_subscribed_seller_listings():
        """
        :return: Listings data frame of subscribed sellers
        """
        subscribed_seller_listings = sql_result(SUBSCRIBED_SELLERS_QUERY)['id'].tolist()
        return subscribed_seller_listings
