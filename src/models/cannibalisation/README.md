## Purpose

This script marks marketplace listings to active/expired on the basis of their ability to cannibalize inventory 
listings i.e, affects the overall lead conversion on those listings 

## Execution

cd ~/data-science/src/models && `python -m cannibalisation.cannibalisation` ;
## Note

This script is compatible with python 3.7.3
- Refer to `docs/cannibalisation` for documentation
- Refer to `notebooks/cannibalisation/Visualize.ipynb` for data visualization
- Refer to `https://plot.ly/~Anshu_Sharma/51` for single model vector visualisation on `plotly` and other 
demonstrations available on the same profile