LISTINGS_DATA_FETCH_SUBQUERY = """SELECT l.id, l.status AS listing_status, l.body_type_id AS body_type,
l.price, v.model_id, lo.city_id, SUM(DATEDIFF(IFNULL(DATE(lsl.end_time), DATE(NOW())), DATE(lsl.start_time))) AS days_since_active,
YEAR(COALESCE(l.manufacturing_date, l.registration_date)) AS age, v.id AS variant_id, vo.orp, v.name, l.mileage,
l.owners, l.rating FROM listings l JOIN variants v ON l.variant_id = v.id JOIN localities lo ON l.locality_id = lo.id
JOIN listing_status_lifeline lsl ON l.id = lsl.listing_id LEFT JOIN variant_orps vo ON v.id = vo.variant_id AND
lo.city_id = vo.city_id"""

EXPIRED_MP_DATA_QUERY = """{data_fetch_subquery} WHERE l.is_inventory = 0 AND l.price > 10000 AND lsl.status IN 
('active' , 'refurbishing') AND l.status IN ('expired') AND DATEDIFF(IFNULL(lsl.end_time, NOW()), lsl.start_time) >= 4 
GROUP BY lsl.listing_id HAVING DATEDIFF(NOW(), MIN(lsl.start_time)) {active_subquery}"""

ACTIVE_MP_DATA_QUERY = """{data_fetch_subquery} WHERE l.is_inventory = 0 AND l.price > 10000 AND lsl.status IN 
('active' , 'refurbishing') AND l.status IN ('active' , 'refurbishing') GROUP BY lsl.listing_id HAVING 
SUM(DATEDIFF(IFNULL(lsl.end_time, NOW()), lsl.start_time)) {expiry_subquery}"""

SUBSCRIBED_SELLERS_QUERY = """SELECT  l.id FROM listings l JOIN sellers s ON l.seller_id = s.id JOIN users u ON 
CAST(s.mobile AS SIGNED) = u.mobile JOIN user_subscriptions us ON us.user_id = u.id WHERE us.subscription_id = 6 
AND CURDATE() <= DATE_ADD(DATE(us.end_date), INTERVAL 15 DAY) AND l.status='active'"""

AUTO_EXPIRY_QUERY = """SELECT l.id, lo.city_id AS city_id FROM listings l JOIN localities lo ON l.locality_id=lo.id JOIN 
listing_status_lifeline lst ON l.id=lst.listing_id WHERE l.is_inventory != 1 AND l.status = 'active' AND lst.status=
'active' AND DATEDIFF(IFNULL(lst.end_time, NOW()), lst.start_time) >= 4 
GROUP BY lst.listing_id HAVING DATEDIFF(NOW(), min(lst.start_time)) > {expiry_days_limit_subquery}"""

USER_FEEDBACK_FILTER_QUERY = """SELECT sq.listing_id,
    SUM(CASE WHEN sq.feedback = 'Car is sold/not available' THEN 1 ELSE 0 END) AS cs_count,
    SUM(CASE WHEN sq.feedback = 'Seller not reachable' THEN 1 ELSE 0 END) AS nr_count
FROM (SELECT DISTINCT listing_id, user_id, feedback FROM user_listing_feedbacks) AS sq
GROUP BY listing_id"""

MAX_TD_PRICE_QUERY = """SELECT l.body_type_id AS body_type, MAX(l.price) AS max_td_price, MIN(l.price) AS min_td_price
FROM listings l JOIN localities lcl ON l.locality_id = lcl.id WHERE l.status IN ('active', 'refurbishing')
AND l.body_type_id is not null AND lcl.city_id = {city_id} AND l.inventory_type_id IN (1, 2)
AND l.is_inventory = 1 GROUP BY l.body_type_id"""

AVOID_COUNT_COLUMNS = ['max_data', 'min_data', 'status', 'id', 'listing_status', 'body_type', 'price', 'model_id',
                       'city_id', 'age', 'variant_id', 'orp', 'name', 'mileage', 'owners', 'rating', 'mp_price',
                       'days_since_active']

COLUMNS_NEEDED = ['id', 'listing_status', 'city_id', 'days_since_active']
