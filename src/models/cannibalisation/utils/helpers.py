import json
import datetime

import numpy as np
from scipy.signal import argrelextrema
from scipy.interpolate import interp1d
from shapely.geometry import LineString, MultiPoint

from dynamic_pricing_code.helpers import sql_result
from ds_helpers.helpers import get_active_cities_dict


class CbData:

    def __init__(self):
        self.today = datetime.date.today()
        self.city_id_name_dict = get_active_cities_dict()
        self.cannibalisation_params = self.get_cannibalisation_params()

    @staticmethod
    def get_status_dict():
        status_logging_data = {"table_name": "cannibalisation_logs", "changed_column": "current_status",
                               "db_column": "status", "identifier_name": "listing_id"}
        return status_logging_data

    @staticmethod
    def get_cannibalisation_params():
        """
        :return: Json of cannibalisation_params from database
        """
        cannibalisation_params = json.loads(
            sql_result("""SELECT value FROM global_constants WHERE name="cannibalisation_params" """).iloc[0, 0])
        return cannibalisation_params


def get_plot_extrema(series_data):
    """
    :returns: extrema data of a pandas column
    """
    min_data = series_data.iloc[argrelextrema(series_data.values, np.less_equal)]
    max_data = series_data.iloc[argrelextrema(series_data.values, np.greater_equal)]
    return min_data, max_data


def get_list_of_tuples(data_frame):
    return list(data_frame.itertuples(index=False, name=None))


def get_intersection_points(curve_1, curve_2):
    """
    Finds intersection points between two curves(curve_1, curve_2)
    :return: list of tuples [(x_coordinate, y_coordinate)]
    """
    if len(curve_1) > 1 and len(curve_2) > 1:
        curve_1 = LineString(get_list_of_tuples(curve_1))
        curve_2 = LineString(get_list_of_tuples(curve_2))
        intersection_points = curve_1.intersection(curve_2)
        if intersection_points.is_empty:
            return []

        # Type formatting check to see if there are multiple points of intersection
        if '.point' in str(type(intersection_points)):
            intersection_points = [intersection_points]
        elif '.LineString' in str(type(intersection_points)):
            intersection_points = intersection_points.coords
        else:
            intersection_points = intersection_points[0].coords
        intersection_points = MultiPoint(intersection_points)
        list_of_points = [(point.x, point.y) for point in intersection_points]
    else:
        list_of_points = []
    return list_of_points


def interpolate_missing_ages_price(data_frame, td_data, column):
    """
    Replaces unavailable(NULL) values for particular ages using td data
    :param data_frame: Dataframe to fill values in
    :param td_data: Base data
    :param column: name of column to modify
    :return: modified dataframe
    """
    empty_rows = data_frame.loc[data_frame[column].isnull()]
    if len(empty_rows):
        interpolate_prices = interp1d(td_data.age.values, td_data.normalised_price.values, fill_value='extrapolate')
        missing_data_ages = empty_rows.age.values.astype('int')
        normalised_prices = np.round(interpolate_prices(missing_data_ages)).astype('int')
        data_frame.loc[empty_rows.index, column] = normalised_prices
    return data_frame
