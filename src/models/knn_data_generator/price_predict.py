import os

import redis
import requests
import sentry_sdk

from knn_data_generator.constants import ACTIVE_REFURBISH_CARS_QUERY

os.environ['SLACK_LOGGING'] = 'TRUE'
os.environ['LOG_FILENAME'] = 'price_predict.log'

from dynamic_pricing_code.helpers import config_data, add_log, sql_result


def get_active_refurbishing_listings():
    """
    :return: divides all eligible listings into chunks of batch_size, for easier api calls
    """
    batch_size = 20
    eligible_listings = sql_result(ACTIVE_REFURBISH_CARS_QUERY)
    eligible_listings_list = eligible_listings['id'].values.tolist()
    chunks_listing_ids = [eligible_listings_list[index:index + batch_size] for index in
                          range(0, len(eligible_listings_list), batch_size)]
    return chunks_listing_ids, eligible_listings_list


def get_json_response(chunk_of_ids):
    """
    :param chunk_of_ids: list of list of listing ids
    :return: list of dictionary containing all listing details returned by the api
    """
    base_url = config_data['price_engine']['base_url']
    base_url += 'price_engine/'
    listing_details = []
    for list_ids in chunk_of_ids:
        listings_str = ','.join(list(map(str, list_ids)))
        response = requests.get(url=base_url, params={'listing_id': listings_str})
        if response.status_code != 200:
            continue
        response_dict = response.json()
        if 'results' in response_dict:
            listing_details += response_dict['results']
        else:
            listing_detail = dict()
            listing_detail['listing_id'] = response_dict['listing_id']
            listing_detail['predicted_price'] = response_dict['predicted_price']
            listing_detail['procurement_price'] = response_dict['procurement_price']
            listing_detail['accuracy_flag'] = response_dict['accuracy_flag']
            listing_details += [listing_detail]
    return listing_details


def prepare_redis_dict(listing_details):
    """
    :param listing_details: list of dictionaries containing listing_id and predicted_prices
    :return: final_listing_price dict
    """
    listing_price_dict = {}
    for listing_detail in listing_details:
        price = listing_detail['predicted_price']
        if price:
            listing_price_dict['pe:' + str(listing_detail['listing_id'])] = price

    return listing_price_dict


def remove_redis_data(listing_ids, redis_con):
    """
    :param listing_ids: list of all active/refurb listing_ids
    :param redis_con: redis connection instance
    :return: doesn't return anything
    """
    key_pattern = 'pe:'
    for redis_key in redis_con.scan_iter(key_pattern + '*'):
        if int(redis_key.decode('utf-8').strip(key_pattern)) not in listing_ids:
            redis_con.delete(redis_key)


def set_redis_data(listing_dict, redis_con):
    """
    :param listing_dict: list of dictionary containing all listing details returned by the api
    :param redis_con: redis connection instance
    :return: doesn't return anything
    """
    redis_con.mset({key: value for key, value in listing_dict.items()})


if __name__ == '__main__':
    SENTRY_URL = config_data['sentry']['url']
    sentry_sdk.init(SENTRY_URL)
    try:
        redis_url = config_data['redis']['price_engine']
        redis_connection = redis.Redis.from_url(url=redis_url)
        chunks_of_listing_ids, listings = get_active_refurbishing_listings()
        json_responses = get_json_response(chunks_of_listing_ids)
        prepared_listing_dict = prepare_redis_dict(json_responses)
        set_redis_data(prepared_listing_dict, redis_connection)
        remove_redis_data(listings, redis_connection)
    except Exception as e:
        add_log(str(e), True)
