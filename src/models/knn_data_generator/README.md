# Purpose

These scripts provide auxiliary support to the pricing engine

The script `asp_data.py` inserts and updates the database daily with the latest token dates and actual_selling_prices of cars for all the cities.

The script `price_predict.py` inserts the predicted price for all eligible cars into database.

## Execution

```cd ~/data-science/src/models && python -m knn_data_generator.asp_data```

```cd ~/data-science/src/models && python -m knn_data_generator.price_predict```

### Note

This script is compatible with python 3.7.2