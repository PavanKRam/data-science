import os

import numpy as np
import pandas as pd
import sentry_sdk

from knn_data_generator.constants import EXISTING_DATA_QUERY, EXISTING_ENTRY_CHECK_QUERY, UPDATE_EXISTING_ENTRY_QUERY, \
    INSERT_NEW_ENTRY_QUERY, EXISTING_LISTING_CHECK_QUERY, EXISTING_LISTING_TYPE_QUERY, UPDATE_LISTING_TYPE_QUERY, \
    EXISTING_PNS_LISTINGS_QUERY

os.environ['SLACK_LOGGING'] = 'TRUE'
os.environ['LOG_FILENAME'] = 'asp_data_sync.log'
from dynamic_pricing_code.helpers import read_sheet, config_data, add_log, sql_result, PROD_CREDENTIALS, \
    get_sql_connection, get_cursor, sql_insert_execution
from dp_z_score_generator.dynamic_pricing_score import listToStr


def listing_verification(asp_data):
    if asp_data.empty:
        return asp_data
    car_list = asp_data['car_id'].values.tolist()
    car_id_list = listToStr(car_list)
    car_id_list = sql_result(EXISTING_LISTING_CHECK_QUERY.format(car_list=car_id_list))
    car_id_list = car_id_list['id'].values.tolist()

    mask = np.in1d(asp_data['car_id'].values, car_id_list)
    final_asp_data = asp_data[mask]
    return final_asp_data


def data_validation(sales_data_df):
    car_id_list = sales_data_df['car_id'].values.tolist()
    car_id_list = listToStr(car_id_list)
    existing_db_data = sql_result(EXISTING_DATA_QUERY.format(car_list=car_id_list))
    existing_db_data['token_date'] = pd.to_datetime(existing_db_data['token_date'])
    master_data = pd.merge(sales_data_df, existing_db_data, how='outer',
                           left_on=['car_id', 'car_deal_value', 'date_of_token_payment'],
                           right_on=['listing_id', 'actual_selling_price', 'token_date'], indicator=True)
    insertion_data = master_data[master_data['_merge'] == 'left_only']
    insertion_data = insertion_data.drop(columns=['listing_id', 'actual_selling_price', 'token_date', '_merge'])

    return insertion_data


def db_insertion(insertion_df):
    if insertion_df.empty:
        return
    values_to_insert = []
    for row in insertion_df.itertuples():
        listing_id = row.car_id
        asp = row.car_deal_value
        token_date = str(row.date_of_token_payment).split(' ')[0]
        existing_entry_check = sql_result(EXISTING_ENTRY_CHECK_QUERY.format(listing_id=listing_id))

        if existing_entry_check.empty:
            values_to_insert.append((str(listing_id), str(asp), token_date))
        else:
            try:
                sql_insert_execution(CURSOR, UPDATE_EXISTING_ENTRY_QUERY.format(asp=asp, token_date=token_date,
                                                                                listing_id=listing_id))
            except Exception as e:
                add_log(str(e), True)
                continue
    if len(values_to_insert):
        try:
            query = INSERT_NEW_ENTRY_QUERY + """,""".join("""(%s, %s, %s)""" for number in values_to_insert)
            flattened_values = [item for sublist in values_to_insert for item in sublist]
            CURSOR.execute(query, flattened_values)

        except Exception as e:
            add_log(str(e) + query, True)


def prepare_raw_data(sheet_key, sheet_name):
    sales_data_list = read_sheet(sheet_key, sheet_name)

    column_names = []
    for column_name in sales_data_list[0]:
        column_names.append(column_name.strip().lower().replace(' ', '_'))

    sales_data_df = pd.DataFrame(sales_data_list[1:], columns=column_names)
    sales_data_df = sales_data_df.filter(['car_id', 'car_deal_value', 'date_of_token_payment'])
    sales_data_df['car_id'] = pd.to_numeric(sales_data_df['car_id'], errors='coerce', downcast='float')
    sales_data_df['car_deal_value'] = pd.to_numeric(sales_data_df['car_deal_value'], errors='coerce') * 100000
    sales_data_df['date_of_token_payment'] = pd.to_datetime(sales_data_df['date_of_token_payment'], errors='coerce')
    sales_data_df = sales_data_df.dropna().drop_duplicates(subset='car_id', keep='last').reset_index(drop=True)
    sales_data_df['car_id'] = sales_data_df['car_id'].apply(lambda listing_id: int(listing_id))
    sales_data_df['car_deal_value'] = sales_data_df['car_deal_value'].apply(lambda price: round(price))

    return sales_data_df


def get_sold_listings():
    car_id_type_df = sql_result(EXISTING_LISTING_TYPE_QUERY)
    return car_id_type_df


def get_listing_type_from_sheet(sheet_key, sheet_name):
    procurement_list_of_list = read_sheet(sheet_key, sheet_name)
    columns = []
    for element in procurement_list_of_list[1]:
        columns.append(element.strip().lower().replace(' ', '_'))
    inventory_df = pd.DataFrame(procurement_list_of_list[2:], columns=columns)
    inventory_df = inventory_df.filter(['website_listing_id', 'type'])
    inventory_df['website_listing_id'] = pd.to_numeric(inventory_df['website_listing_id'], errors='coerce')
    inventory_df = inventory_df.dropna()
    inventory_df['type'] = inventory_df['type'].replace(
        to_replace={'B2B/Offloading': 'offloading', 'Truebil': 'truebil', 'Park and Sell': 'pns'})
    return inventory_df


def listing_type_update(df):
    df = df.drop(columns=['website_listing_id'])
    df_truebil = df.loc[df['type'] == 'truebil'].reset_index(drop=True)
    df_offloading = df.loc[df['type'] == 'offloading'].reset_index(drop=True)
    df_pns = df.loc[df['type'] == 'pns'].reset_index(drop=True)
    df_list = [df_truebil, df_offloading, df_pns]
    for df in df_list:
        if df.empty:
            continue
        listings = listToStr(df['listing_id'].tolist())
        sql_result(UPDATE_LISTING_TYPE_QUERY.format(listing_type=df['type'].iloc[0], listings=listings))


def fill_other_pns_type():
    car_id_type_df = sql_result(EXISTING_PNS_LISTINGS_QUERY)
    if car_id_type_df.empty:
        return
    listings = listToStr(car_id_type_df['listing_id'].tolist())
    sql_result(UPDATE_LISTING_TYPE_QUERY.format(listing_type='pns', listings=listings))


if __name__ == '__main__':
    global CURSOR
    SENTRY_URL = config_data['sentry']['url']
    sentry_sdk.init(SENTRY_URL)
    SALES_SHEET_KEYS = config_data['sheet_keys']['sales_sheet']['cities']
    SALES_SHEET_NAME = config_data['sheet_keys']['sales_sheet']['name'] + '!D:P'
    PROCUREMENT_SHEET_KEYS = config_data['sheet_keys']['procurement_sheet']['cities']
    PROCUREMENT_SHEET_NAME = config_data['sheet_keys']['procurement_sheet']['name']
    CONNECTION = get_sql_connection(PROD_CREDENTIALS)
    CURSOR = get_cursor(CONNECTION)
    for sheet_key in SALES_SHEET_KEYS:
        try:
            sales_data = prepare_raw_data(SALES_SHEET_KEYS[sheet_key], SALES_SHEET_NAME)
            data_to_insert = data_validation(sales_data)
            data_to_insert = listing_verification(data_to_insert)
            db_insertion(data_to_insert)
            CONNECTION.commit()
        except Exception as e:
            add_log(str(e), is_error=True)

    inventory_df_list = []
    for sheet_key in PROCUREMENT_SHEET_KEYS:
        try:
            inventory_type_df = get_listing_type_from_sheet(PROCUREMENT_SHEET_KEYS[sheet_key], PROCUREMENT_SHEET_NAME)
            inventory_df_list.append(inventory_type_df)
        except Exception as e:
            add_log(str(e), is_error=True)

    try:
        sold_listings = get_sold_listings()
        inventory_type_df = pd.concat([inventory_df_list[0], pd.concat([inventory_df_list[1], inventory_df_list[2]])])
        inventory_type_df = inventory_type_df.drop_duplicates().reset_index(drop=True)
        eligible_listings = pd.merge(sold_listings, inventory_type_df, how='inner', left_on='listing_id',
                                     right_on='website_listing_id')
        listing_type_update(eligible_listings)
        fill_other_pns_type()
    except Exception as e:
        add_log(str(e), is_error=True)
    CONNECTION.close()
