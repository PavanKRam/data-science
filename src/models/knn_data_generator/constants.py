EXISTING_DATA_QUERY = """SELECT listing_id, actual_selling_price, token_date FROM sold_listings WHERE listing_id IN {car_list}"""

EXISTING_ENTRY_CHECK_QUERY = """SELECT listing_id FROM sold_listings WHERE listing_id = {listing_id}"""

UPDATE_EXISTING_ENTRY_QUERY = """UPDATE sold_listings SET actual_selling_price = {asp}, token_date = '{token_date}' WHERE listing_id = {listing_id}"""

INSERT_NEW_ENTRY_QUERY = """INSERT INTO sold_listings (listing_id, actual_selling_price, token_date) VALUES """

EXISTING_LISTING_CHECK_QUERY = """SELECT id FROM listings WHERE id in {car_list}"""

ACTIVE_REFURBISH_CARS_QUERY = """SELECT l.id from listings l WHERE l.status IN ('active', 'refurbishing')"""

EXISTING_LISTING_TYPE_QUERY = """SELECT listing_id FROM sold_listings WHERE listing_type IS NULL"""

UPDATE_LISTING_TYPE_QUERY = """UPDATE sold_listings set listing_type = '{listing_type}' where listing_id in {listings} """

EXISTING_PNS_LISTINGS_QUERY = """SELECT sl.listing_id, l.inventory_type_id FROM sold_listings sl 
JOIN listings l ON sl.listing_id = l.id WHERE sl.listing_type IS NULL AND l.is_inventory=1 AND l.inventory_type_id=1"""

TRUEPRICE_ADDITION_QUERY = """UPDATE {table} SET trueprice = {trueprice}, suggested_procurement_price = {procurement_price} WHERE id = {listing_id}"""

B2B_LISTINGS_QUERY = """SELECT l.id, l.variant_id, l.mileage, l.owner_serial_number AS owner, l.color_id, 
loc.city_id AS city_id, 5 AS rating, COALESCE(l.registration_year, l.manufacturing_year) AS year
FROM b2b_listings l
JOIN b2b_sellers s ON s.id = l.seller_id
JOIN localities loc ON loc.id = s.locality_id
JOIN variants v ON l.variant_id = v.id
JOIN models m ON m.id = v.model_id WHERE m.trueprice_accuracy = 1"""

LISTINGS_QUERY = """SELECT l.id FROM listings l JOIN variants v ON l.variant_id=v.id JOIN models m ON m.id = v.model_id
WHERE m.trueprice_accuracy = 1"""
