import os

import requests
import sentry_sdk

from knn_data_generator.constants import TRUEPRICE_ADDITION_QUERY, B2B_LISTINGS_QUERY, LISTINGS_QUERY
from knn_data_generator.price_predict import get_json_response

os.environ['SLACK_LOGGING'] = 'TRUE'
os.environ['LOG_FILENAME'] = 'trueprice.log'

from dynamic_pricing_code.helpers import config_data, add_log, sql_result, PROD_CREDENTIALS, get_sql_connection, \
    get_cursor, sql_insert_execution


def database_entry_update(listing_data):
    for listing_detail in listing_data:
        sql_insert_execution(CURSOR, TRUEPRICE_ADDITION_QUERY.format(
            table='listings', listing_id=listing_detail['listing_id'],
            trueprice=listing_detail['predicted_price'] if listing_detail['predicted_price'] else 'Null',
            procurement_price=listing_detail['procurement_price'] if listing_detail['procurement_price'] else 'Null'))


def get_all_car_ids():
    batch_size = 20
    listings = sql_result(LISTINGS_QUERY)
    listings_list = listings['id'].values.tolist()
    chunk_listing_ids = [listings_list[index:index + batch_size] for index in range(0, len(listings_list), batch_size)]
    return chunk_listing_ids


def get_response_set_data(df):
    df = df.fillna(0).astype(int)
    dict_of_params = df.to_dict('index')

    base_url = config_data['price_engine']['base_url']
    base_url += 'price_engine/'

    for b2b_id in dict_of_params:
        response = requests.get(url=base_url, params=dict_of_params[b2b_id])
        if response.status_code != 200:
            continue
        response_dict = response.json()
        sql_insert_execution(CURSOR, TRUEPRICE_ADDITION_QUERY.format(
            table='b2b_listings', listing_id=b2b_id,
            trueprice=response_dict['predicted_price'] if response_dict['predicted_price'] else 'Null',
            procurement_price=response_dict['procurement_price'] if response_dict['procurement_price'] else 'Null'))


if __name__ == '__main__':
    global CURSOR
    SENTRY_URL = config_data['sentry']['url']
    sentry_sdk.init(SENTRY_URL)
    CONNECTION = get_sql_connection(PROD_CREDENTIALS)
    CURSOR = get_cursor(CONNECTION)
    try:
        chunk_of_listing_ids = get_all_car_ids()
        json_responses = get_json_response(chunk_of_listing_ids)
        database_entry_update(json_responses)
        CONNECTION.commit()
    except Exception as e:
        add_log(str(e), True)

    try:
        b2b_listings_df = sql_result(B2B_LISTINGS_QUERY).set_index('id')
        get_response_set_data(b2b_listings_df)
        CONNECTION.commit()
    except Exception as e:
        add_log(str(e), True)
    CONNECTION.close()
