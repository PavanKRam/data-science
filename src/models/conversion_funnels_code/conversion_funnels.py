import json
import gspread
import pymysql
import pandas as pd
import os
import time
import datetime
from google.oauth2 import service_account
from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
SERVICE_ACCOUNT_FILE = 'credentials.json'


def style_sheet():
    # Includes style (|) in the sheet between two rows for better visualiastion

    sheet_cell_list = ['B12', 'B13', 'D12', 'D13', 'B16', 'B17', 'D16', 'D17', 'G12', 'G13', 'I12', 'I13', 'K12', 'K13',
                       'M12', 'M13',
                       'G16', 'G17', 'I16', 'I17', 'K16', 'K17', 'M16', 'M17']

    sheet_value_list = []
    for i in range(len(sheet_cell_list)):
        sheet_value_list.append('|')

    write_in_sheet(sheet_cell_list, sheet_value_list)

    print("Sheet_name to be looked at:", sheet_name)


def mar_sheet_relative_data():
    # Writes relative MarketPlace Data in the sheet

    l1 = l_inv
    l2 = len(mp_list)

    l = l1 + l2

    if l != 0:
        j6 = float(l2) / l
        j6 *= 100
    else:
        j6 = 0

    l3 = len(poten_list)
    l4 = len(non_poten_list)

    if l2 != 0:
        i5 = float(l3) / l2
        k5 = float(l4) / l2
    else:
        i5 = k5 = 0

    if l != 0:
        mar = float(l2) / l
        mar *= 100
    else:
        mar = 0

    l3 = len(poten_list)
    l4 = len(non_poten_list)
    l = len(mp_list)
    if l != 0:
        p = float(l3) / l
        np = float(l4) / l
    else:
        p = np = 0

    h8 = p * mar
    l8 = np * mar

    t1 = web_count
    t2 = web_visit_done_count
    t3 = web_vb_sold_count

    q1 = non_web_count
    q2 = non_web_visit_done_count
    q3 = non_web_vb_sold_count

    p1 = web_count_1
    p2 = web_visit_done_count_1
    p3 = web_vb_sold_count_1

    r1 = non_web_count_1
    r2 = non_web_visit_done_count_1
    r3 = non_web_vb_sold_count_1

    if l3 != 0:
        g8 = float(t1) / l3
        i8 = float(q1) / l3
    else:
        g8 = i8 = 0

    if l4 != 0:
        m8 = float(r1) / l4
        k8 = float(p1) / l4
    else:
        m8 = k8 = 0

    g11 = g8 * h8
    k11 = k8 * l8
    m11 = m8 * l8
    i11 = i8 * h8

    if t1 != 0:
        f12 = float(t2) / t1
    else:
        f12 = 0

    if q1 != 0:
        h12 = float(q2) / q1
    else:
        h12 = 0

    if p1 != 0:
        j12 = float(p2) / p1
    else:
        j12 = 0

    if r1 != 0:
        n12 = float(r2) / r1
    else:
        n12 = 0

    g15 = f12 * g11
    i15 = h12 * i11
    k15 = j12 * k11
    m15 = n12 * m11

    if t2 != 0:
        f16 = float(t3) / t2
    else:
        f16 = 0

    if q2 != 0:
        h16 = float(q3) / q2
    else:
        h16 = 0

    if p2 != 0:
        j16 = float(p3) / p2
    else:
        j16 = 0

    if r2 != 0:
        n16 = float(r3) / r2
    else:
        n16 = 0

    g19 = f16 * g15
    i19 = h16 * i15
    k19 = j16 * k15
    m19 = n16 * m15

    sheet_value_list = ['Market Place', j6, i5, k5, 'Potential', 'Non Potential', h8, l8, g8, m8, i8, k8,
                        'Web', 'Web', 'Non Web', 'Non Web', g11, i11, k11, m11, f12, h12, j12, n12, 'VD', 'VD', 'VD',
                        'VD',
                        g15, i15, k15, m15, f16, h16, j16, n16, 'Sold', 'Sold', 'Sold', 'Sold', g19, i19, k19, m19]
    sheet_cell_list = ['J5', 'J6', 'I5', 'K5', 'H7', 'L7', 'H8', 'L8', 'G8', 'M8', 'I8', 'K8',
                       'G10', 'K10', 'I10', 'M10', 'G11', 'I11', 'K11', 'M11', 'F12', 'H12', 'J12', 'N12', 'G14', 'K14',
                       'I14', 'M14',
                       'G15', 'I15', 'K15', 'M15', 'F16', 'H16', 'J16', 'N16', 'G18', 'K18', 'I18', 'M18', 'G19', 'I19',
                       'K19', 'M19']

    write_in_sheet(sheet_cell_list, sheet_value_list)

    print("Relative MarketPlace Data (in terms of %'s ) has been written into the sheet :)")

    style_sheet()


def inv_sheet_relative_data():
    # Writes relative Inventory Data in the sheet

    l1 = l_inv
    l2 = len(mp_list)

    l = l1 + l2

    if l != 0:
        c6 = float(l1) / l
        c6 *= 100
    else:
        c6 = 0

    a1 = web_count_inv
    a2 = web_visit_done_count_inv
    a3 = web_vb_sold_count_inv

    b1 = non_web_count_inv
    b2 = non_web_visit_done_count_inv
    b3 = non_web_vb_sold_count_inv

    if l1 != 0:
        b7 = float(a1) / l1
        d7 = float(b1) / l1
    else:
        b7 = d7 = 0

    b11 = c6 * b7
    d11 = c6 * d7

    if a1 != 0:
        a12 = float(a2) / a1
    else:
        a12 = 0

    if b1 != 0:
        e12 = float(b2) / b1
    else:
        e12 = 0

    b15 = a12 * b11
    d15 = e12 * d11

    if a2 != 0:
        a16 = float(a3) / a2
    else:
        a16 = 0

    if b2 != 0:
        e16 = float(b3) / b2
    else:
        e16 = 0
    b19 = a16 * b15
    d19 = e16 * d15

    sheet_cell_list = ['C5', 'C6', 'B7', 'D7', 'B10', 'D10', 'B11', 'D11', 'A12', 'E12', 'B14', 'D14', 'B15', 'D15',
                       'B18', 'D18', 'A16',
                       'E16', 'B19', 'D19']

    sheet_value_list = ['inventory', c6, b7, d7, 'Web', 'Non Web', b11, d11, a12, e12, 'VD', 'VD', b15, d15, 'Sold',
                        'Sold', a16,
                        e16, b19, d19]

    write_in_sheet(sheet_cell_list, sheet_value_list)

    print("\nRelative Inventory Data (in terms of %'s ) has been written into the sheet :)")

    mar_sheet_relative_data()


def sheet_relative_data():
    # Writes header in the sheet

    sheet_cell_list = ['G3', 'G4']
    sheet_value_list = ['leads', '100']

    write_in_sheet(sheet_cell_list, sheet_value_list)

    inv_sheet_relative_data()


def mar_sheet_abs_data():
    # Writes absolute MarketPlace Data in the sheet

    j6 = len(mp_list)
    l3 = len(poten_list)
    l4 = len(non_poten_list)
    l = len(mp_list)

    if l != 0:
        i5 = float(l3) / l
        k5 = float(l4) / l
        h8 = l3
        l8 = l4
        p = float(l3) / l
        np = float(l4) / l
    else:
        i5 = k5 = h8 = l8 = p = np = 0

    t1 = web_count
    t2 = web_visit_done_count
    t3 = web_vb_sold_count

    q1 = non_web_count
    q2 = non_web_visit_done_count
    q3 = non_web_vb_sold_count

    p1 = web_count_1
    p2 = web_visit_done_count_1
    p3 = web_vb_sold_count_1

    r1 = non_web_count_1
    r2 = non_web_visit_done_count_1
    r3 = non_web_vb_sold_count_1

    if l3 != 0:
        g8 = float(t1) / l3
        i8 = float(q1) / l3
    else:
        g8 = i8 = 0

    if l4 != 0:
        k8 = float(p1) / l4
        m8 = float(r1) / l4
    else:
        k8 = m8 = 0

    g11 = t1
    i11 = q1
    k11 = p1
    m11 = r1

    if t1 != 0:
        f12 = float(t2) / t1
    else:
        f12 = 0

    if q1 != 0:
        h12 = float(q2) / q1
    else:
        h12 = 0

    if p1 != 0:
        j12 = float(p2) / p1
    else:
        j12 = 0

    if r1 != 0:
        n12 = float(r2) / r1
    else:
        n12 = 0

    g15 = t2
    i15 = q2
    k15 = p2
    m15 = r2

    if t2 != 0:
        f16 = float(t3) / t2
    else:
        f16 = 0

    if q2 != 0:
        h16 = float(q3) / q2
    else:
        h16 = 0

    if p2 != 0:
        j16 = float(p3) / p2
    else:
        j16 = 0

    if r2 != 0:
        n16 = float(r3) / r2
    else:
        n16 = 0

    g19 = t3
    i19 = q3
    k19 = p3
    m19 = r3

    sheet_value_list = ['Market Place', j6, i5, k5, 'Potential', 'Non Potential', h8, l8, g8, m8, i8, k8,
                        'Web', 'Web', 'Non Web', 'Non Web', g11, i11, k11, m11, f12, h12, j12, n12, 'VD', 'VD', 'VD',
                        'VD',
                        g15, i15, k15, m15, f16, h16, j16, n16, 'Sold', 'Sold', 'Sold', 'Sold', g19, i19, k19, m19]
    sheet_cell_list = ['J5', 'J6', 'I5', 'K5', 'H7', 'L7', 'H8', 'L8', 'G8', 'M8', 'I8', 'K8',
                       'G10', 'K10', 'I10', 'M10', 'G11', 'I11', 'K11', 'M11', 'F12', 'H12', 'J12', 'N12', 'G14', 'K14',
                       'I14', 'M14',
                       'G15', 'I15', 'K15', 'M15', 'F16', 'H16', 'J16', 'N16', 'G18', 'K18', 'I18', 'M18', 'G19', 'I19',
                       'K19', 'M19']

    write_in_sheet(sheet_cell_list, sheet_value_list)

    print("Absolute MarketPlace Data has been written into the sheet :)")

    style_sheet()


def inv_sheet_abs_data():
    # Writes absolute Inventory Data in the sheet

    l1 = l_inv
    l2 = len(mp_list)

    l = l1 + l2

    c6 = l1

    a1 = web_count_inv
    a2 = web_visit_done_count_inv
    a3 = web_vb_sold_count_inv

    b1 = non_web_count_inv
    b2 = non_web_visit_done_count_inv
    b3 = non_web_vb_sold_count_inv

    if l1 != 0:
        b7 = float(a1) / l1
        d7 = float(b1) / l1
    else:
        b7 = d7 = 0

    b11 = a1
    d11 = b1

    if a1 != 0:
        a12 = float(a2) / a1
    else:
        a12 = 0

    if b1 != 0:
        e12 = float(b2) / b1
    else:
        e12 = 0

    b15 = a2
    d15 = b2

    if a2 != 0:
        a16 = float(a3) / a2
    else:
        a16 = 0

    if b2 != 0:
        e16 = float(b3) / b2
    else:
        e16 = 0

    b19 = a3
    d19 = b3

    sheet_cell_list = ['C5', 'C6', 'B7', 'D7', 'B10', 'D10', 'B11', 'D11', 'A12', 'E12', 'B14', 'D14', 'B15', 'D15',
                       'B18', 'D18', 'A16',
                       'E16', 'B19', 'D19']

    sheet_value_list = ['inventory', c6, b7, d7, 'Web', 'Non Web', b11, d11, a12, e12, 'VD', 'VD', b15, d15, 'Sold',
                        'Sold', a16,
                        e16, b19, d19]

    write_in_sheet(sheet_cell_list, sheet_value_list)

    print("\nAbsolute Inventory Data has been written into the sheet :)")

    mar_sheet_abs_data()


def sheet_abs_data():
    # Writes header in the sheet

    l1 = l_inv
    l2 = len(mp_list)

    g4 = l1 + l2

    sheet_cell_list = ['G3', 'G4']
    sheet_value_list = ['leads', g4]

    write_in_sheet(sheet_cell_list, sheet_value_list)
    inv_sheet_abs_data()


def write_in_sheet(sheet_cell_list, sheet_value_list):
    # Writes data in to the sheet
    # sheet_cell_list: Contains the cell numbers in which values to be written
    # sheet_value_list: Contains the actual values

    global spread_sheet, sheet_name, file

    wks = file.open(spread_sheet).worksheet(sheet_name)

    for i in range(len(sheet_cell_list)):
        wks.update_acell(sheet_cell_list[i], sheet_value_list[i])
        time.sleep(1)


def get_credentials():
    # Validates Credentials for Writing into the Sheet

    # json credentials you downloaded earlier
    scope = ['https://www.googleapis.com/auth/drive']
    credential_dir = os.path.join('', SERVICE_ACCOUNT_FILE)
    # get email and key from creds
    creds = ServiceAccountCredentials.from_json_keyfile_name(credential_dir, scope)
    # authenticate with Google
    global file
    file = gspread.authorize(creds)


def get_spreadsheet_service():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    credential_dir = os.path.join('', SERVICE_ACCOUNT_FILE)

    credentials = service_account.Credentials.from_service_account_file(
        credential_dir, scopes=SCOPES)

    service = build('sheets', 'v4', credentials=credentials)

    return service


def readSheet(sid, range_name):
    service = get_spreadsheet_service()

    spreadsheet_id = sid
    range_name = range_name
    result = service.spreadsheets().values().get(
        spreadsheetId=spreadsheet_id, range=range_name).execute()

    values = result.get('values', [])

    return values


def sold_data():
    """
    
      Reads Sold Data from all the three cities and stores them in list
                  sold_list : contains all the mobile numbers (including alternate)
                  
    """

    sid = '1MORHRhAky2qIuEbBdP_djZpfeMjWn2_gh2Spl-t_hIo'
    rangeName = 'Mumbai!H:I'
    details = readSheet(sid, rangeName)

    global sold_list

    for i in range(1, len(details)):
        for j in details[i]:
            j = str(j)
            if j not in sold_list:
                sold_list.append(j)

    sid = '1MORHRhAky2qIuEbBdP_djZpfeMjWn2_gh2Spl-t_hIo'
    rangeName = 'Bangalore!G:H'
    details = readSheet(sid, rangeName)

    for i in range(1, len(details)):
        for j in details[i]:
            j = str(j)
            if j not in sold_list:
                sold_list.append(j)

    sid = '1MORHRhAky2qIuEbBdP_djZpfeMjWn2_gh2Spl-t_hIo'
    rangeName = 'Delhi!G:H'
    details = readSheet(sid, rangeName)

    for i in range(1, len(details)):
        for j in details[i]:
            j = str(j)
            if j not in sold_list:
                sold_list.append(j)

    print("\nSold Data has been read :)")


def market_place():
    global start_date
    global end_date
    db1 = pymysql.connect(host="db.truebil.njcrm.in", user="truebil_guest", passwd="Guest@321", db="fusion_truebilcrm")

    # Extracts data from the CRM database for visit booking

    df1 = pd.read_sql("""select a.id as enquiry, a.phone_mobile as mobile, r.name as city,date(a.`date_entered`) as LG, a.`birthdate` as VB, b.`visit_done_date_c` as VD, b.`visit_id_set_from_api_c` as WebVB
    from leads a left join leads_cstm b on a.id=b.id_c 
    left join c_city r on r.id=b.c_city_id_c 
    where a.deleted=0 and a.birthdate>0
    order by city,VB, mobile 
    """, db1)

    # vb_list = {{}}  [dictionary of dictionaries]
    # key : Mobile Number
    # Stores the latest entry if repeated

    vb_list = {}
    for i in range(len(df1)):
        m = (df1.mobile[i])
        vb = df1.VB[i]
        web = int(df1.WebVB[i])
        vd = df1.VD[i]
        vb_list[m] = {}
        vb_list[m]['vb'] = vb
        vb_list[m]['web'] = web
        vb_list[m]['vd'] = vd

    rec_dic = {}
    temp_poten_list = []
    temp_non_poten_list = []

    input_file = 'reco_score.csv'
    df = pd.read_csv(input_file)
    for i in range(len(df)):
        try:
            m = int(df.mobile[i])
            m = str(m)
            sc = df.avg[i]
            rec_dic[m] = sc
        except:
            pass

    global city_id_list
    db = pymysql.connect(host="db.truebil.com", user="truebil-read", passwd="newreadpassword", db="truebil")

    df2 = pd.read_sql("""
    select c.id, bu.mobile from (select distinct a.id from (select distinct b.id, l.is_inventory, lo.city_id, bl.created_at
    from buyer_listings bl, buyers b, listings l, localities lo
    where bl.created_at >= %s and bl.created_at <= %s and
    bl.buyer_id = b.id and bl.listing_id = l.id
    and l.locality_id = lo.id
    union
    select distinct b.id, l.is_inventory, lo.city_id, bl.created_at
    from buyer_listing_offers bl, buyers b, listings l, localities lo
    where bl.created_at >= %s and bl.created_at <= %s and
    bl.buyer_id = b.id and bl.listing_id = l.id
    and l.locality_id = lo.id)a where a.is_inventory=0 and a.id not in (select distinct b.id from (select distinct b.id, l.is_inventory,      lo.city_id, bl.created_at
    from buyer_listings bl, buyers b, listings l, localities lo
    where bl.created_at >= %s and bl.created_at <= %s and
    bl.buyer_id = b.id and bl.listing_id = l.id
    and l.locality_id = lo.id
    union
    select distinct b.id, l.is_inventory, lo.city_id, bl.created_at
    from buyer_listing_offers bl, buyers b, listings l, localities lo
    where bl.created_at >= %s and bl.created_at <= %s and
    bl.buyer_id = b.id and bl.listing_id = l.id
    and l.locality_id = lo.id)b where b.id in (select d.id from (select b.id, l.is_inventory, lo.city_id, bl.created_at
    from buyer_listings bl, buyers b, listings l, localities lo
    where bl.created_at >= %s and bl.created_at <= %s and
    bl.buyer_id = b.id and bl.listing_id = l.id
    and l.locality_id = lo.id
    union
    select b.id, l.is_inventory, lo.city_id, bl.created_at
    from buyer_listing_offers bl, buyers b, listings l, localities lo
    where bl.created_at >= %s and bl.created_at <= %s and
    bl.buyer_id = b.id and bl.listing_id = l.id
    and l.locality_id = lo.id)d where d.is_inventory=1)))c join buyers bu on c.id=bu.id  where city_id in (%s,%s,%s)""", db,
    params=[start_date, end_date, start_date, end_date, start_date, end_date, start_date, end_date, start_date_pre
                          , end_date, start_date_pre, end_date, city_id_list[0], city_id_list[1], city_id_list[2]])

    global mp_list
    mp_list = []

    for i in range(len(df2)):
        mp_list.append(df2.mobile[i])

    global poten_list
    poten_list = []

    global non_poten_list
    non_poten_list = []

    global rec_score
    for i in mp_list:
        try:
            if rec_dic[i] > rec_score:
                poten_list.append(i)
            else:
                non_poten_list.append(i)
        except:
            pass

    global poten_vb_list
    poten_vb_list = []
    for i in poten_list:
        if i in vb_list:
            poten_vb_list.append(i)

    global non_poten_vb_list
    non_poten_vb_list = []
    for i in non_poten_list:
        if i in vb_list:
            non_poten_vb_list.append(i)

    global web_count
    global web_visit_done_count
    global web_vd_sold_count
    global web_vb_sold_count
    global non_web_vd_sold_count
    global non_web_vb_sold_count
    global non_web_visit_done_count
    global non_web_count

    web_count = 0
    web_visit_done_count = 0
    web_vd_sold_count = 0
    web_vb_sold_count = 0
    non_web_vd_sold_count = 0
    non_web_vb_sold_count = 0
    non_web_visit_done_count = 0
    non_web_count = 0

    for i in poten_vb_list:
        if vb_list[i]['web'] == 1:
            web_count += 1
            if i in sold_list:
                web_vb_sold_count += 1
            if vb_list[i]['vd'] is not None:
                web_visit_done_count += 1
                if i in sold_list:
                    web_vd_sold_count += 1

        else:
            non_web_count += 1
            if i in sold_list:
                non_web_vb_sold_count += 1
            if vb_list[i]['vd'] is not None:
                non_web_visit_done_count += 1
                if i in sold_list:
                    non_web_vd_sold_count += 1

    global web_count_1
    global web_visit_done_count_1
    global web_vd_sold_count_1
    global web_vb_sold_count_1
    global non_web_vd_sold_count_1
    global non_web_vb_sold_count_1
    global non_web_visit_done_count_1
    global non_web_count_1

    web_count_1 = 0
    web_visit_done_count_1 = 0
    web_vd_sold_count_1 = 0
    web_vb_sold_count_1 = 0
    non_web_vd_sold_count_1 = 0
    non_web_vb_sold_count_1 = 0
    non_web_visit_done_count_1 = 0
    non_web_count_1 = 0

    for i in non_poten_vb_list:
        if vb_list[i]['web'] == 1:
            web_count_1 += 1
            if i in sold_list:
                web_vb_sold_count_1 += 1
            if vb_list[i]['vd'] is not None:
                web_visit_done_count_1 += 1
                if i in sold_list:
                    web_vd_sold_count_1 += 1

        else:
            non_web_count_1 += 1
            if i in sold_list:
                non_web_vb_sold_count_1 += 1
            if vb_list[i]['vd'] is not None:
                non_web_visit_done_count_1 += 1
                if i in sold_list:
                    non_web_vd_sold_count_1 += 1

    print("Market Place calculations are done :)")


def inventory():
    db1 = pymysql.connect(host="db.truebil.njcrm.in", user="truebil_guest", passwd="Guest@321", db="fusion_truebilcrm")

    # Extracts data from the CRM database for visit booking

    df_inv_vb = pd.read_sql("""select a.id as enquiry, a.phone_mobile as mobile, r.name as city,date(a.`date_entered`) as LG, a.`birthdate` as VB, b.`visit_done_date_c` as VD, b.`visit_id_set_from_api_c` as WebVB
    from leads a left join leads_cstm b on a.id=b.id_c 
    left join c_city r on r.id=b.c_city_id_c 
    where a.deleted=0 and a.birthdate>0
    order by city,VB, mobile 
    """, db1)

    # vb_list_inv = {{}}  [dictionary of dictionaries]
    # key : Mobile Number
    # Stores the latest entry if repeated

    vb_list_inv = {}
    for i in range(len(df_inv_vb)):
        m = (df_inv_vb.mobile[i])
        vb = df_inv_vb.VB[i]
        web = int(df_inv_vb.WebVB[i])
        vd = df_inv_vb.VD[i]
        vb_list_inv[m] = {}
        vb_list_inv[m]['vb'] = vb
        vb_list_inv[m]['web'] = web
        vb_list_inv[m]['vd'] = vd

    global city_id_list

    db = pymysql.connect(host="db.truebil.com", user="truebil-read", passwd="newreadpassword", db="truebil")

    df = pd.read_sql("""
         select c.id, bu.mobile  from (select distinct a.id from (select distinct b.id, l.is_inventory, lo.city_id, bl.created_at
        from buyer_listings bl, buyers b, listings l, localities lo
        where bl.created_at >= %s and bl.created_at <= %s  and
        bl.buyer_id = b.id and bl.listing_id = l.id
        and l.locality_id = lo.id
        union all
        select distinct b.id, l.is_inventory, lo.city_id, bl.created_at
        from buyer_listing_offers bl, buyers b, listings l, localities lo
        where bl.created_at >= %s and bl.created_at <= %s and
        bl.buyer_id = b.id and bl.listing_id = l.id
        and l.locality_id = lo.id)a where a.id in (select c.id from (select b.id, l.is_inventory, lo.city_id, bl.created_at
        from buyer_listings bl, buyers b, listings l, localities lo
        where bl.created_at >= %s and bl.created_at <= %s and
        bl.buyer_id = b.id and bl.listing_id = l.id
        and l.locality_id = lo.id
        union
        select b.id, l.is_inventory, lo.city_id, bl.created_at
        from buyer_listing_offers bl, buyers b, listings l, localities lo
        where bl.created_at >= %s and bl.created_at <= %s and
        bl.buyer_id = b.id and bl.listing_id = l.id
        and l.locality_id = lo.id)c where c.is_inventory=1) )c join buyers bu on c.id=bu.id where city_id in (%s,%s,%s)""", db,
         params=[start_date, end_date, start_date, end_date, start_date_pre, end_date, start_date_pre,end_date, city_id_list[0]
                         , city_id_list[1], city_id_list[2]])

    global l_inv
    l_inv = len(df)

    count = 0
    temp_list = []
    for i in range(len(df)):
        m = (df.mobile[i])
        if m in vb_list_inv:
            count += 1
            temp_list.append(m)

    global web_count_inv
    global web_visit_done_count_inv
    global web_vd_sold_count_inv
    global web_vb_sold_count_inv
    global non_web_vd_sold_count_inv
    global non_web_vb_sold_count_inv
    global non_web_visit_done_count_inv
    global non_web_count_inv
    global sold_list

    for i in temp_list:
        if vb_list_inv[i]['web'] == 1:
            web_count_inv += 1
            if i in sold_list:
                web_vb_sold_count_inv += 1
            if vb_list_inv[i]['vd'] is not None:
                web_visit_done_count_inv += 1
                if i in sold_list:
                    web_vd_sold_count_inv += 1

        else:
            non_web_count_inv += 1
            if i in sold_list:
                non_web_vb_sold_count_inv += 1
            if vb_list_inv[i]['vd'] is not None:
                non_web_visit_done_count_inv += 1
                if i in sold_list:
                    non_web_vd_sold_count_inv += 1

    print("Inventory calculations are done :)")

    market_place()


get_credentials()
print("\nSTART DATE\n")
year = int(input('Enter a year '))
print("\nDon't prefix zero infront of month Eg: For Aug enter 8 not 08 \n")
month = int(input('Enter a month '))
day = int(input('Enter a day '))

start_date = datetime.date(year, month, day)
start_date_pre = datetime.date(year, month - 1, day)

print("Start_date is : ", start_date)
print("Start_date_previous month is : ", start_date_pre)

print("\nEND DATE\n")
year = int(input('Enter a year '))
print("\nDon't prefix zero infront of month Eg: For Aug enter 8 not 08 \n")
month = int(input('Enter a month '))
day = int(input('Enter a day '))

end_date = datetime.date(year, month, day)

print("End_date is : ", end_date)

sheet_city_dic = {1: [1, 1, 1], 2: [2, 2, 2], 4: [1, 2, 5], 5: [5, 5, 5]}

city = int(input('\nEnter 1 - for mumbai  2 - for delhi  5 -for banglore  4 - for all cities '))

city_id_list = sheet_city_dic[city]

print("\nSelected city_id is:", city)
print("\nMake sure Recommendation scores are updated before continuing ")
rec_flag = int(input("Enter 1-To Continue 2-To Exit "))

if (rec_flag == 1):

    rec_score = float(input(
        "\nEnter the Score on which you wanted to differentiate Potential and Non-Potential Buyers (0.85 or 0.9) "))
    print("\nThe process has begun :)")	

    sold_list = []	
    l_inv = 0
    web_count_inv = 0
    web_visit_done_count_inv = 0
    web_vd_sold_count_inv = 0
    web_vb_sold_count_inv = 0
    non_web_vd_sold_count_inv = 0
    non_web_vb_sold_count_inv = 0
    non_web_visit_done_count_inv = 0
    non_web_count_inv = 0
    mp_list = []
    poten_list = []
    non_poten_list = []
    poten_vb_list = []
    non_poten_vb_list = []
    web_count = 0
    web_visit_done_count = 0
    web_vd_sold_count = 0
    web_vb_sold_count = 0
    non_web_vd_sold_count = 0
    non_web_vb_sold_count = 0
    non_web_visit_done_count = 0
    non_web_count = 0
    web_count_1 = 0
    web_visit_done_count_1 = 0
    web_vd_sold_count_1 = 0
    web_vb_sold_count_1 = 0
    non_web_vd_sold_count_1 = 0
    non_web_vb_sold_count_1 = 0
    non_web_visit_done_count_1 = 0
    non_web_count_1 = 0

    sold_data()
    inventory()

    print("\nBelow is the link to the Spreadsheet where you can check the results out")
    link = 'https://docs.google.com/spreadsheets/d/1tzJOG56SkZvbuoVZxUKO5qyN0b-88bHjs0Gz7NZgbHk'
    print(link)

    spread_sheet = 'Conversion_Funnels'
    sheet_data_dic = {1: 'mumbai_data', 2: 'delhi_data', 5: 'banglore_data', 4: 'all_data'}
    sheet_perc_dic = {1: 'mumbai', 2: 'delhi', 5: 'banglore', 4: 'all'}

    sheet_name = sheet_data_dic[city]
    sheet_abs_data()

    sheet_name = sheet_perc_dic[city]
    sheet_relative_data()

else:
    print("Update the Reco Score and continue..!\n")
