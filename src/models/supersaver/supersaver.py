import collections
import os

import pandas as pd
import pyarrow as pa
import redis
import sentry_sdk

from dp_z_score_generator.constants import ACTIVE_CARS_QUERY, PRICE_CHANGES_QUERY, CITY_NAME_QUERY
from dp_z_score_generator.constants import SUPER_SAVER_INSERT_QUERY

os.environ['SLACK_LOGGING'] = 'TRUE'
from dp_z_score_generator.dynamic_pricing_score import instantiate_multi_index_dataframe, get_leads_per_latest_dates, \
    get_price_changes, get_demand_model, get_slab_check
from dynamic_pricing_code.helpers import config_data, sql_result, PROD_CREDENTIALS, get_cursor, get_sql_connection, \
    add_log, write_in_sheet

CITIES = [1, 2, 5]


def cluster_lead_deficit_data(cluster_data):
    price_slabs = collections.OrderedDict([('(1, 3)', (1, 3)), ('(3, 7)', (3, 7)), ('(7, 50)', (7, 50))])

    demands = ['High_Demand', 'Mid_Demand', 'Low_Demand']
    for demand in demands:
        for price_slab in (list(price_slabs.keys())):
            mean_value_series = cluster_data[demand][price_slab]['mean']
            required_leads = sum(mean_value_series) / 19
            cluster_data[demand, price_slab, 'req_lead'] = required_leads
            cluster_data.sort_index(axis=1)
    return cluster_data


def get_cluster_benchmark_data():
    redis_connection = redis.Redis().from_url(url=config_data['redis']['recommendation'])
    context = pa.default_serialization_context()
    for city in CITIES:
        city_name = sql_result(CITY_NAME_QUERY.format(id=city)).iloc[0, 0].lower()
        CLUSTER_DATA[city] = context.deserialize(redis_connection.get(city_name + '_dp_cluster_benchmark'))
        CLUSTER_DATA[city] = cluster_lead_deficit_data(CLUSTER_DATA[city])


def get_leads_per_car(active_cars_leads_df):
    listing_leads_days_active = active_cars_leads_df['dates'].apply(
        lambda x: {len(x.keys()): sum(x.values())}).to_frame()
    return listing_leads_days_active


def set_super_saver_listings_in_db(super_saver_df, city_id):
    super_saver_listings = list(super_saver_df.index)
    rank = 1
    city_wise_data = []
    for listing_id in super_saver_listings:
        city_wise_data.append([city_id, listing_id, rank])
        rank += 1
    insert_query = SUPER_SAVER_INSERT_QUERY + """,""".join("""(%s, %s, %s)""" for number in city_wise_data)
    flattened_values = [item for sublist in city_wise_data for item in sublist]
    CURSOR.execute(insert_query, flattened_values)
    CONNECTION.commit()


def get_super_saver_cars(lead_deficit_df):
    top_percent = 20
    super_saver_df = lead_deficit_df.head(int(len(lead_deficit_df) * (top_percent / 100)))
    return super_saver_df


def get_lead_deficit(leads_sum, cluster_data, model_key, price_slab_key):
    leads_sum['lead_deficit'] = ''
    for i in range(0, len(leads_sum)):
        days_active = list(leads_sum['dates'].iloc[i].keys())[0]
        leads = list(leads_sum['dates'].iloc[i].values())[0]
        req_lead_avg = cluster_data[model_key][price_slab_key]['req_lead'].iloc[0]
        lead_deficit = (req_lead_avg * days_active) - leads
        leads_sum['lead_deficit'].iloc[i] = lead_deficit
    return leads_sum


def set_super_saver_to_sheets(super_saver_df, city_id):
    city_name = sql_result(CITY_NAME_QUERY.format(id=city_id)).iloc[0, 0].lower()
    super_saver_df['dates'] = super_saver_df['dates'].astype(str)
    super_saver_list = super_saver_df.reset_index().to_numpy().tolist()
    write_in_sheet(super_saver_list, config_data['sheet_keys']['factor_sheet']['key'], city_name + '_supersaver!A2:C')


def calculate_cluster_wise_lead_deficit(leads_sum_days_active, active_cars, price_slabs):
    for city_id in CITIES:
        cluster_data = CLUSTER_DATA[city_id]
        city_active_cars = active_cars.loc[active_cars['city_id'] == city_id]
        low_demand_price_slab = []
        final_lead_deficit_df = pd.DataFrame()
        for price_slab_key in list(price_slabs.keys()):
            price_slab = price_slabs[price_slab_key]
            temp_df_1 = city_active_cars.loc[
                (active_cars.price / 10 ** 5 >= price_slab[0]) & (active_cars.price / 10 ** 5 < price_slab[1])]
            models = get_demand_model(price_slab)
            [slab_check, temp_df_1] = get_slab_check(price_slab, low_demand_price_slab, temp_df_1)

            if slab_check == 1 or slab_check == 2:
                for model_key in list(models.keys()):
                    model = models[model_key]
                    k = temp_df_1.loc[temp_df_1.model_id.isin(model)]
                    temp_df_2 = leads_sum_days_active.loc[leads_sum_days_active.index.intersection(k['id'])]
                    if not len(temp_df_2.index):
                        continue
                    temp_df_3 = get_lead_deficit(temp_df_2, cluster_data, model_key, price_slab_key)
                    final_lead_deficit_df = pd.DataFrame.append(final_lead_deficit_df, temp_df_3)
        final_lead_deficit_df = final_lead_deficit_df.sort_values(by=['lead_deficit'], ascending=False)
        super_saver_cars = get_super_saver_cars(final_lead_deficit_df)
        set_super_saver_listings_in_db(super_saver_cars, city_id)
        set_super_saver_to_sheets(super_saver_cars, city_id)


if __name__ == "__main__":
    global SENTRY_URL, CLUSTER_DATA, CURSOR, CONNECTION
    SENTRY_URL = config_data['sentry']['url']
    sentry_sdk.init(SENTRY_URL)
    CONNECTION = get_sql_connection(PROD_CREDENTIALS)
    CURSOR = get_cursor(CONNECTION)
    CLUSTER_DATA = {}

    try:
        cols, price_slabs = instantiate_multi_index_dataframe()
        price_changes_done = get_price_changes(PRICE_CHANGES_QUERY.format(condition="l.source_update != 'cron'"))
        all_active_cars = sql_result(ACTIVE_CARS_QUERY.format(status_condition="('active','refurbishing')"))
        list_all_aDa_test = get_leads_per_latest_dates(active_cars_df=all_active_cars,
                                                       price_changes_done=price_changes_done)
        leads_days_active = get_leads_per_car(list_all_aDa_test)
        get_cluster_benchmark_data()
        calculate_cluster_wise_lead_deficit(leads_days_active, all_active_cars, price_slabs)
    except Exception as e:
        add_log(str(e), is_error=True)
    CONNECTION.close()
