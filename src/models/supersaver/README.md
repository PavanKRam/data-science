## Purpose

This script generates supersaver listings everyday.

### Execution

```cd ~/data-science/src/models && python -m supersaver.supersaver```

#### Note

This script is compatible with python 3.7.2