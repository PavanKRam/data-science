import requests
import os
import json

"""
This class takes the required credentials from the config file and sends the message or file. When the class is 
instantiated the token, channel_name, and current file path are instantiated. Then, when the send function is called, it
sends the required message or file.
"""


class Slack:
    def __init__(self):
        base_dir = os.path.dirname(os.path.realpath(__file__))
        configfile = open(os.path.join(base_dir, 'config.json'), 'r')
        config_data = json.loads(configfile.read())
        token = config_data['slack_bot']['token']
        self.base_url = config_data['slack_bot']['base_url']
        self.headers = {"Authorization": "Bearer " + token}
        self.config_data = config_data

    def send(self, action, data_payload=None, json_payload=None):
        action = self.config_data['slack_bot']['action'][action]
        response = requests.post(self.base_url + action, data=data_payload, json=json_payload, headers=self.headers)
        return response
