GET_LIVE_CARS_QUERY = """SELECT `date`,
       Sum(CASE
             WHEN (@t := first_active_date IS NULL OR @t := Datediff(`date`, Date(first_active_date)) < {days_slab}) THEN 1
             ELSE 0 end) AS 'cars_live' {cluster_sub_query}
FROM (SELECT DISTINCT Date(created_at) AS `date`, @token_count := 1 as token_count
      FROM buyer_listings
      WHERE Date(created_at) between '{lower_limit}' and '{upper_limit}') d
       LEFT JOIN (SELECT a.listing_id,
                         IF(first_active_date > start_time, first_active_date, start_time) AS start_time,
                         end_time,
                         first_active_date, body_type_id, price
                  FROM (SELECT listing_id, lifeline.`status`, start_time, end_time, listings.body_type_id, 
                  listings.price
                        FROM (SELECT l.listing_id,
                                     l.status_to                                                     AS `status`,
                                     l.changed_at                                                    AS start_time,
                                     CASE WHEN r.changed_at IS NULL THEN Now() ELSE r.changed_at end AS end_time
                              FROM (SELECT @row_no1 := @row_no1 + 1 AS row_no, listing_id, status_to, changed_at
                                    FROM listing_status_track,
                                         (SELECT @row_no1 := 0) t
                                    ORDER BY listing_id, changed_at) l
                                     LEFT JOIN (SELECT @row_no2 := @row_no2 + 1 AS row_no,
                                                       listing_id,
                                                       status_to,
                                                       changed_at
                                                FROM listing_status_track,
                                                     (SELECT @row_no2 := 0) p
                                                ORDER BY listing_id, changed_at) r
                                               ON l.row_no + 1 = r.row_no AND l.listing_id = r.listing_id) lifeline
                               LEFT JOIN listings ON listings.id = lifeline.listing_id AND
                                                     lifeline.end_time >= listings.inventory_added_date
                               LEFT JOIN localities ON localities.id = listings.locality_id
                        WHERE lifeline.`status` IN ('Active')
                          AND is_inventory = 1
                          AND city_id = {city_id}
                          AND inventory_type_id = 2) a
                         LEFT JOIN (SELECT listing_id,
                                           lifeline.`status`,
                                           IF(Min(start_time) < inventory_added_date, inventory_added_date,
                                              Min(start_time)) AS first_active_date
                                    FROM (SELECT l.listing_id, l.status_to AS `status`, l.changed_at AS start_time,
                                                 CASE WHEN r.changed_at IS NULL THEN Now() ELSE r.changed_at end AS end_time
                                          FROM (SELECT @row_no1 := @row_no1 + 1 AS row_no,
                                                       listing_id,
                                                       status_to,
                                                       changed_at
                                                FROM listing_status_track,
                                                     (SELECT @row_no1 := 0) t
                                                ORDER BY listing_id, changed_at) l
                                                 LEFT JOIN (SELECT @row_no2 := @row_no2 + 1 AS row_no,
                                                                   listing_id,
                                                                   status_to,
                                                                   changed_at
                                                            FROM listing_status_track,
                                                                 (SELECT @row_no2 := 0) p
                                                            ORDER BY listing_id, changed_at) r
                                                           ON l.row_no + 1 = r.row_no AND l.listing_id = r.listing_id) lifeline
                                           LEFT JOIN listings ON listings.id = lifeline.listing_id AND
                                                                 lifeline.end_time >= listings.inventory_added_date
                                           LEFT JOIN localities ON localities.id = listings.locality_id
                                    WHERE lifeline.`status` IN ('Active')
                                      AND is_inventory = 1
                                      AND city_id = {city_id}
                                      AND inventory_type_id = 2
                                    GROUP BY listing_id) b ON a.listing_id = b.listing_id) c
                 ON d.`date` BETWEEN IF(first_active_date > start_time, first_active_date, start_time) AND `end_time`
GROUP BY `date`
ORDER BY `date`
"""

GET_TOKENS_QUERY = """SELECT d.`date` AS token_date,
       Sum(CASE
             WHEN @t:=age_at_token >= 0 AND @t:=age_at_token < {days_slab} THEN IF(token_count IS NULL, 0, token_count)
             ELSE 0 end) AS 'cars_sold' {cluster_sub_query}
FROM (SELECT DISTINCT Date(created_at) AS `date`
      FROM buyer_listings
      WHERE Date(created_at) BETWEEN '{lower_limit}' AND '{upper_limit}') d
       LEFT JOIN (SELECT Date(token_time) AS token_date, c.price, c.body_type_id,first_active_date,
                         IF(first_active_date IS NULL OR Datediff(Date(token_time), Date(first_active_date)) < 0, 0,
                            IF(Datediff(Date(token_time), Date(first_active_date)) > 90, 90,
                               Datediff(Date(token_time), Date(first_active_date)))) AS age_at_token,
                         Count(DISTINCT listing_id) AS token_count
                  FROM (SELECT a.listing_id, token_time, first_active_date, a.price, a.body_type_id
                        FROM (SELECT listing_id, lifeline.`status`, token_time, listings.price, listings.body_type_id
                              FROM (SELECT l.listing_id, r.status_to AS `status`, r.changed_at AS token_time
                                    FROM (SELECT @row_no1 := @row_no1 + 1 AS row_no, listing_id, status_to, changed_at
                                          FROM listing_status_track,
                                               (SELECT @row_no1 := 0) t
                                          ORDER BY listing_id, changed_at) l
                                           LEFT JOIN (SELECT @row_no2 := @row_no2 + 1 AS row_no,
                                                             listing_id,
                                                             status_to,
                                                             changed_at
                                                      FROM listing_status_track,
                                                           (SELECT @row_no2 := 0) p
                                                      ORDER BY listing_id, changed_at) r
                                                     ON l.row_no + 1 = r.row_no AND l.listing_id = r.listing_id
                                    WHERE r.listing_id IS NOT NULL) lifeline
                                     LEFT JOIN listings ON listings.id = lifeline.listing_id
                                     LEFT JOIN localities ON localities.id = listings.locality_id
                              WHERE lifeline.`status` IN ('Truebiled')
                                AND is_inventory = 1
                                AND city_id = {city_id}
                                AND (inventory_type_id = 2 OR inventory_type_id IS NULL)
                                AND listings.id NOT IN (7566, 8834, 10104, 10678, 14023, 20338, 26100, 32318, 41550)) a
                               LEFT JOIN (SELECT listing_id,
                                                 lifeline.`status`,
                                                 IF(Min(start_time) < inventory_added_date, inventory_added_date,
                                                    Min(start_time)) AS first_active_date
                                          FROM (SELECT l.listing_id,
                                                       l.status_to  AS `status`,
                                                       l.changed_at AS start_time,
                                                       CASE WHEN r.changed_at IS NULL THEN Now() ELSE r.changed_at end AS end_time
                                                FROM (SELECT @row_no1 := @row_no1 + 1 AS row_no,
                                                             listing_id,
                                                             status_to,
                                                             changed_at
                                                      FROM listing_status_track,
                                                           (SELECT @row_no1 := 0) t
                                                      ORDER BY listing_id, changed_at) l
                                                       LEFT JOIN (SELECT @row_no2 := @row_no2 + 1 AS row_no,
                                                                         listing_id,
                                                                         status_to,
                                                                         changed_at
                                                                  FROM listing_status_track,
                                                                       (SELECT @row_no2 := 0) p
                                                                  ORDER BY listing_id, changed_at) r
                                                                 ON l.row_no + 1 = r.row_no AND l.listing_id = r.listing_id) lifeline
                                                 LEFT JOIN listings ON listings.id = lifeline.listing_id AND
                                                                       lifeline.end_time >= listings.inventory_added_date
                                                 LEFT JOIN localities ON localities.id = listings.locality_id
                                          WHERE lifeline.`status` IN ('Active')
                                            AND is_inventory = 1
                                            AND city_id = {city_id}
                                            AND (inventory_type_id = 2 OR inventory_type_id IS NULL)
                                            AND listings.id NOT IN
                                                (7566, 8834, 10104, 10678, 14023, 20338, 26100, 32318, 41550)
                                          GROUP BY listing_id) b ON a.listing_id = b.listing_id) c
                  GROUP BY Date(token_time), price, body_type_id,
                           IF(first_active_date IS NULL OR Datediff(Date(token_time), Date(first_active_date)) < 0, 0,
                              IF(Datediff(Date(token_time), Date(first_active_date)) > 90, 90,
                                 Floor(Datediff(Date(token_time), Date(first_active_date)) / 7) * 7))
                  ORDER BY token_time) f ON f.token_date = d.`date`
GROUP BY `date`
ORDER BY `date`
"""
GET_ALL_LIVE_CARS_QUERY = """SELECT `date`,
       Sum(CASE
             WHEN (first_active_date IS NULL OR Datediff(`date`, Date(first_active_date)) < 90) THEN 1
             ELSE 0 end) AS 'cars_live'
FROM (SELECT DISTINCT Date(created_at) AS `date`
      FROM buyer_listings
      WHERE Date(created_at) between '{lower_limit}' and '{upper_limit}') d
       LEFT JOIN (SELECT a.listing_id,
                         IF(first_active_date > start_time, first_active_date, start_time) AS start_time,
                         end_time,
                         first_active_date
                  FROM (SELECT listing_id, lifeline.`status`, start_time, end_time
                        FROM (SELECT l.listing_id,
                                     l.status_to                                                     AS `status`,
                                     l.changed_at                                                    AS start_time,
                                     CASE WHEN r.changed_at IS NULL THEN Now() ELSE r.changed_at end AS end_time
                              FROM (SELECT @row_no1 := @row_no1 + 1 AS row_no, listing_id, status_to, changed_at
                                    FROM listing_status_track,
                                         (SELECT @row_no1 := 0) t
                                    ORDER BY listing_id, changed_at) l
                                     LEFT JOIN (SELECT @row_no2 := @row_no2 + 1 AS row_no,
                                                       listing_id,
                                                       status_to,
                                                       changed_at
                                                FROM listing_status_track,
                                                     (SELECT @row_no2 := 0) p
                                                ORDER BY listing_id, changed_at) r
                                               ON l.row_no + 1 = r.row_no AND l.listing_id = r.listing_id) lifeline
                               LEFT JOIN listings ON listings.id = lifeline.listing_id AND
                                                     lifeline.end_time >= listings.inventory_added_date
                               LEFT JOIN localities ON localities.id = listings.locality_id
                        WHERE lifeline.`status` IN ('Active')
                          AND is_inventory = 1
                          AND city_id = {city_id}
                          AND inventory_type_id = 2 )a
                         LEFT JOIN (SELECT listing_id,
                                           lifeline.`status`,
                                           IF(Min(start_time) < inventory_added_date, inventory_added_date,
                                              Min(start_time)) AS first_active_date
                                    FROM (SELECT l.listing_id,
                                                 l.status_to                                                     AS `status`,
                                                 l.changed_at                                                    AS start_time,
                                                 CASE WHEN r.changed_at IS NULL THEN Now() ELSE r.changed_at end AS end_time
                                          FROM (SELECT @row_no1 := @row_no1 + 1 AS row_no,
                                                       listing_id,
                                                       status_to,
                                                       changed_at
                                                FROM listing_status_track,
                                                     (SELECT @row_no1 := 0) t
                                                ORDER BY listing_id, changed_at) l
                                                 LEFT JOIN (SELECT @row_no2 := @row_no2 + 1 AS row_no,
                                                                   listing_id,
                                                                   status_to,
                                                                   changed_at
                                                            FROM listing_status_track,
                                                                 (SELECT @row_no2 := 0) p
                                                            ORDER BY listing_id, changed_at) r
                                                           ON l.row_no + 1 = r.row_no AND l.listing_id = r.listing_id) lifeline
                                           LEFT JOIN listings ON listings.id = lifeline.listing_id AND
                                                                 lifeline.end_time >= listings.inventory_added_date
                                           LEFT JOIN localities ON localities.id = listings.locality_id
                                    WHERE lifeline.`status` IN ('Active')
                                      AND is_inventory = 1
                                      AND city_id = {city_id}
                                      AND inventory_type_id = 2
                                    GROUP BY listing_id) b ON a.listing_id = b.listing_id) c
                 ON d.`date` BETWEEN IF(first_active_date > start_time, first_active_date, start_time) AND `end_time`
GROUP BY `date`
ORDER BY `date`"""

GET_ALL_TOKENS_QUERY = """SELECT d.`date`          AS token_date,
       Sum(CASE
             WHEN age_at_token >= 0 THEN IF(tokencount IS NULL, 0, tokencount)
             ELSE 0 end) AS 'cars_sold'
FROM (SELECT DISTINCT Date(created_at) AS `date`
      FROM buyer_listings
      WHERE Date(created_at) BETWEEN '{lower_limit}' AND '{upper_limit}') d
       LEFT JOIN (SELECT Date(token_time)                                            AS token_date,
                         IF(first_active_date IS NULL OR Datediff(Date(token_time), Date(first_active_date)) < 0, 0,
                            IF(Datediff(Date(token_time), Date(first_active_date)) > 90, 90,
                               Datediff(Date(token_time), Date(first_active_date)))) AS age_at_token,
                         Count(DISTINCT listing_id)                                  AS tokencount
                  FROM (SELECT a.listing_id, token_time, first_active_date
                        FROM (SELECT listing_id, lifeline.`status`, token_time
                              FROM (SELECT l.listing_id, r.status_to AS `status`, r.changed_at AS token_time
                                    FROM (SELECT @row_no1 := @row_no1 + 1 AS row_no, listing_id, status_to, changed_at
                                          FROM listing_status_track,
                                               (SELECT @row_no1 := 0) t
                                          ORDER BY listing_id, changed_at) l
                                           LEFT JOIN (SELECT @row_no2 := @row_no2 + 1 AS row_no,
                                                             listing_id,
                                                             status_to,
                                                             changed_at
                                                      FROM listing_status_track,
                                                           (SELECT @row_no2 := 0) p
                                                      ORDER BY listing_id, changed_at) r
                                                     ON l.row_no + 1 = r.row_no AND l.listing_id = r.listing_id
                                    WHERE r.listing_id IS NOT NULL) lifeline
                                     LEFT JOIN listings ON listings.id = lifeline.listing_id
                                     LEFT JOIN localities ON localities.id = listings.locality_id
                              WHERE lifeline.`status` IN ('Truebiled')
                                AND is_inventory = 1
                                AND city_id = {city_id}
                                AND (inventory_type_id = 2 OR inventory_type_id IS NULL )
                                AND listings.id NOT IN (7566, 8834, 10104, 10678, 14023, 20338, 26100, 32318, 41550)) a
                               LEFT JOIN (SELECT listing_id,
                                                 lifeline.`status`,
                                                 IF(Min(start_time) < inventory_added_date, inventory_added_date,
                                                    Min(start_time)) AS first_active_date
                                          FROM (SELECT l.listing_id,
                                                       l.status_to                                                     AS `status`,
                                                       l.changed_at                                                    AS start_time,
                                                       CASE WHEN r.changed_at IS NULL THEN Now() ELSE r.changed_at end AS end_time
                                                FROM (SELECT @row_no1 := @row_no1 + 1 AS row_no,
                                                             listing_id,
                                                             status_to,
                                                             changed_at
                                                      FROM listing_status_track,
                                                           (SELECT @row_no1 := 0) t
                                                      ORDER BY listing_id, changed_at) l
                                                       LEFT JOIN (SELECT @row_no2 := @row_no2 + 1 AS row_no,
                                                                         listing_id,
                                                                         status_to,
                                                                         changed_at
                                                                  FROM listing_status_track,
                                                                       (SELECT @row_no2 := 0) p
                                                                  ORDER BY listing_id, changed_at) r
                                                                 ON l.row_no + 1 = r.row_no AND l.listing_id = r.listing_id) lifeline
                                                 LEFT JOIN listings ON listings.id = lifeline.listing_id AND
                                                                       lifeline.end_time >= listings.inventory_added_date
                                                 LEFT JOIN localities ON localities.id = listings.locality_id
                                          WHERE lifeline.`status` IN ('Active')
                                            AND is_inventory = 1
                                            AND city_id = {city_id}
                                            AND (inventory_type_id = 2 OR inventory_type_id IS NULL )
                                            AND listings.id NOT IN
                                                (7566, 8834, 10104, 10678, 14023, 20338, 26100, 32318, 41550)
                                          GROUP BY listing_id) b ON a.listing_id = b.listing_id) c
                  GROUP BY Date(token_time),
                           IF(first_active_date IS NULL OR Datediff(Date(token_time), Date(first_active_date)) < 0, 0,
                              IF(Datediff(Date(token_time), Date(first_active_date)) > 90, 90,
                                 Floor(Datediff(Date(token_time), Date(first_active_date)) ) ))
                  ORDER BY token_time) f ON f.token_date = d.`date`
GROUP BY `date`
ORDER BY `date`"""

GET_CLUSTER_LEADS_QUERY = """SELECT
   date {cluster_sub_query}
FROM
   (SELECT
       DATE(a.created_at) AS date,
           b.body_type_id,
           listings.price, @t:=1,
           COUNT(a.created_at) AS token_count
   FROM
       (SELECT DISTINCT
       buyer_id, listing_id, DATE(created_at) AS created_at
   FROM
       (SELECT
       buyer_id, listing_id, created_at
   FROM
       buyer_listings
   WHERE
       source_id NOT IN (2 , 15, 42) UNION SELECT
       buyer_id, listing_id, created_at
   FROM
       buyer_listing_offers) z) a
   LEFT JOIN listings ON listings.id = a.listing_id
   LEFT JOIN localities ON localities.id = listings.locality_id
   LEFT JOIN (SELECT
       listing_id,
           lifeline.status,
           IF(MIN(start_time) < inventory_added_date, inventory_added_date, MIN(start_time)) AS first_active_date,
           listings.body_type_id
   FROM
       (SELECT
       l.listing_id,
           l.status_to AS status,
           l.changed_at AS start_time,
           CASE
               WHEN r.changed_at IS NULL THEN NOW()
               ELSE r.changed_at
           END AS end_time
   FROM
       (SELECT
       @row_no1:=@row_no1 + 1 AS row_no,
           listing_id,
           status_to,
           changed_at
   FROM
       listing_status_track, (SELECT @row_no1:=0) t
   ORDER BY listing_id , changed_at) l
   LEFT JOIN (SELECT
       @row_no2:=@row_no2 + 1 AS row_no,
           listing_id,
           status_to,
           changed_at
   FROM
       listing_status_track, (SELECT @row_no2:=0) p
   ORDER BY listing_id , changed_at) r ON l.row_no + 1 = r.row_no
       AND l.listing_id = r.listing_id) lifeline
   LEFT JOIN listings ON listings.id = lifeline.listing_id
       AND lifeline.end_time >= listings.inventory_added_date
   LEFT JOIN localities ON localities.id = listings.locality_id
   WHERE
       lifeline.status IN ('Active',  'refurbishing')
           AND is_inventory = 1
           AND city_id = {city_id}
           AND (inventory_type_id = 2
           OR inventory_type_id IS NULL)
   GROUP BY listing_id) b ON b.listing_id = a.listing_id
   WHERE
       is_inventory = 1 AND city_id = {city_id}
           AND (inventory_type_id = 2
           OR inventory_type_id IS NULL)
           AND a.created_at BETWEEN '2017-12-01' AND NOW()
           AND a.created_at > first_active_date AND DATE(a.created_at) BETWEEN '{lower_limit}' AND '{upper_limit}'
   GROUP BY DATE(a.created_at) , b.body_type_id , listings.price) a
GROUP BY date"""

EXTRACT_MAXIMUM_PENALTY = """SELECT t1.action, t1.id
                              FROM q_tables t1
                              WHERE t1.city_id = {city}
                                AND CAST(t1.z_state AS DECIMAL(3, 2)) = CAST({listing_state} AS DECIMAL(3, 2))
                                AND t1.q_value = (SELECT MAX(t2.q_value)
                                                  FROM q_tables t2
                                                  WHERE t2.city_id = t1.city_id
                                                    AND t2.z_state = t1.z_state
                                                    AND q_value <> 0)"""

EXTRACT_SECOND_BEST_PENALTY = """SELECT t1.action, t1.id
                                  FROM q_tables t1
                                  WHERE t1.city_id = {city}
                                    AND CAST(t1.z_state AS DECIMAL(3, 2)) = CAST({listing_state} AS DECIMAL(3, 2))
                                    AND t1.q_value = (SELECT MAX(t2.q_value)
                                                      FROM q_tables t2
                                                      WHERE t2.city_id = t1.city_id
                                                        AND t2.z_state = t1.z_state
                                                        AND t2.action < {penalty}
                                                        AND q_value <> 0)"""

EXTRACT_STATE_FROM_RAW_STATE = """SELECT state FROM z_score_states WHERE min_value <= {z_score} AND max_value > {z_score}"""

EXTRACT_Q_TABLE_QUERY = """SELECT q_table_id
                          FROM listing_z_score_state_logs lzssl
                                JOIN target_selling_price_logs tspl ON lzssl.target_selling_price_log_id = tspl.id
                          WHERE tspl.listing_id = {id}
                          ORDER BY lzssl.created_at DESC
                          LIMIT 1"""

LATEST_LISTING_ID_QUERY = """SELECT ldp.listing_id FROM listing_dynamic_pricings ldp JOIN listings l ON ldp.listing_id=l.id JOIN localities lo ON l.locality_id=lo.id WHERE lo.city_id={city_id} AND l.is_inventory=1 AND l.inventory_type_id={inventory_type} ORDER BY ldp.id DESC LIMIT 1"""

INITIAL_PRICE_CHECK_QUERY = """SELECT id FROM listing_price_changes WHERE source_update = 'dp_initial' AND listing_id = {listing_id}"""

DP_PARAMS_QUERY = """SELECT value FROM global_constants where name='dp_price_params' """

DAYS_SINCE_ACTIVE_TD_QUERY = """SELECT lst.listing_id, SUM(DATEDIFF(IFNULL(lst.end_time, NOW()), IF(lst.start_time < 
                                l.inventory_added_date, l.inventory_added_date, lst.start_time))) AS days_since_active 
                                FROM listing_status_lifeline lst JOIN listings l ON l.id = lst.listing_id JOIN 
                                localities lo ON l.locality_id = lo.id WHERE lst.status = 'active' AND lo.city_id = 
                                {city_id} AND l.status IN ('active' , 'refurbishing') AND l.is_inventory = 1 AND 
                                IFNULL(lst.end_time, NOW()) >= l.inventory_added_date GROUP BY lst.listing_id"""

INITIAL_PRICE_PARAMS_QUERY = """SELECT name, value FROM global_constants WHERE name in 
("initial_price_margin_params","benchmark_stock_performance")"""

INITIAL_PRICE_CLUSTER_SUB_QUERY = """, SUM(CASE WHEN @t AND body_type_id = 1 AND price BETWEEN 0 AND 300000 THEN 
IF(token_count IS NULL, 0, token_count) ELSE 0 END) AS 'Hatchback_0_3', SUM(CASE WHEN @t AND body_type_id = 1 AND 
price BETWEEN 300001 AND 500000 THEN IF(token_count IS NULL, 0, token_count) ELSE 0 END) AS 'Hatchback_3_5',
SUM(CASE WHEN @t AND body_type_id = 2 AND price BETWEEN 0 AND 300000 THEN IF (token_count IS NULL,
0, token_count) ELSE 0 END) AS 'Sedan_0_3', SUM(CASE WHEN @t AND body_type_id = 2 AND price BETWEEN 300001 AND 500000 
THEN IF(token_count IS NULL, 0, token_count) ELSE 0 END) AS 'Sedan_3_5', SUM(CASE WHEN @t AND body_type_id = 2 AND 
price > 500000 THEN IF(token_count IS NULL, 0, token_count) ELSE 0 END) AS 'Sedan_a_5', 
SUM(CASE WHEN @t AND body_type_id = 3 THEN IF(token_count IS NULL, 0, token_count) ELSE 0 END) AS 'MPV', 
SUM(CASE WHEN @t AND body_type_id = 4 THEN IF(token_count IS NULL, 0, token_count) ELSE 0 END) AS 'SUV', 
SUM(CASE WHEN @t AND price BETWEEN 0 AND 300000 THEN IF(token_count IS NULL, 0, token_count) ELSE 0 END) AS 'price_0_3', 
SUM(CASE WHEN @t AND price BETWEEN 300001 AND 500000 THEN IF(token_count IS NULL, 0, token_count) ELSE 0 END)
 AS 'price_3_5', SUM(CASE WHEN @t AND price > 500000 THEN IF(token_count IS NULL, 0, token_count) ELSE 0 END) 
 AS 'price_a_5' """

GET_MAX_ACTION_CITY_AND_STATE_WISE = """SELECT qt.city_id, MAX(qt.action) AS max_action FROM q_tables qt JOIN 
(SELECT city_id, z_state, MAX(q_value) AS max_q FROM q_tables WHERE q_value <> 0 GROUP BY city_id , z_state) max_q_data 
ON qt.city_id = max_q_data.city_id AND qt.z_state = max_q_data.z_state AND qt.q_value = max_q_data.max_q 
GROUP BY city_id"""

LDP_INSERTION_QUERY = """INSERT INTO listing_dynamic_pricings (listing_id, source_id, minimum_selling_price, 
                        target_selling_price, gst_rate, refurbishing_cost, procurement_price, estimated_selling_price, 
                        actual_refurb_cost, payment_date, seller_type, seller_name) VALUES {data}"""

ACTIVE_LDP_DATA_QUERY = """SELECT  ldp.* FROM listing_dynamic_pricings ldp JOIN listings l ON ldp.listing_id = l.id 
                           JOIN localities lo ON l.locality_id = lo.id WHERE lo.city_id = {city_id} AND 
                           l.inventory_type_id = {inventory_type_id} AND l.status in ('active', 'refurbishing') 
                           ORDER BY l.id"""

DEPICTED_MP_QUERY = """SELECT depicted_market_price FROM listings WHERE id = {listing_id} """
DEPICTED_MP_UPDATE_QUERY = """UPDATE listings SET depicted_market_price = {depicted_price} WHERE id = {listing_id}"""

UPDATE_LISTING_DYNAMIC_PRICINGS_QUERY = """UPDATE listing_dynamic_pricings
                                SET source_id = {source_id}, minimum_selling_price = {msp},
                                    target_selling_price = {tsp}, gst_rate = {gst},
                                    refurbishing_cost = {refurb_cost}, procurement_price = {proc_price}
                          WHERE listing_id = {listing_id}"""

LISTING_DYNAMIC_PRICING_INSERT_QUERY = """INSERT INTO listing_dynamic_pricings (listing_id, source_id, 
                minimum_selling_price, target_selling_price, gst_rate, refurbishing_cost, procurement_price) VALUES 
                ({listing_id}, {source_id}, {msp}, {tsp}, {gst_rate}, {refurb_cost}, {proc_price})"""

PREVIOUS_PRICE_LOG_QUERY = """SELECT updated_price FROM {table_name} WHERE listing_id = {listing_id} ORDER BY 
                            created_at DESC LIMIT 1"""

LOG_INSERT_QUERY = """INSERT INTO {table_name} (listing_id, source_id, previous_price, updated_price) VALUES 
    ({listing_id}, {source_id}, {previous_price}, {updated_price})"""

LSP_INSERT_QUERY = """INSERT INTO listing_price_changes (listing_id, source_update, previous_price, updated_price) 
                    VALUES ({listing_id}, '{source_name}', {previous_price}, {updated_price})"""

UPDATE_LISTINGS_PRICE_QUERY = """UPDATE listings SET price = {lsp} WHERE id = {listing_id} """

CITY_ACTIVE_LISTINGS_QUERY = """SELECT l.id as listing_id, l.price as lsp, SUM(DATEDIFF(IFNULL(DATE(lsl.end_time), DATE (NOW())), DATE (lsl.start_time)))
AS days_since_active FROM listings l JOIN localities lo ON l.locality_id = lo.id JOIN listing_status_lifeline lsl ON l.id = lsl.listing_id
WHERE l.status IN ('active', 'refurbishing') AND lo.city_id = {city_id} AND l.inventory_type_id IN (1,2) AND l.is_inventory = 1
GROUP BY  l.id"""

FIXED_PRICE_QUERY = """SELECT listing_id, minimum_selling_price as msp, target_selling_price as tsp 
FROM listing_dynamic_pricings WHERE listing_id in ({listing_ids})"""

FIXED_PRICE_UPDATE_QUERY = """UPDATE listings SET price = {fixed_price} WHERE id = {listing_id}"""

UPDATE_LDP_QUERY = """UPDATE listing_dynamic_pricings SET minimum_selling_price = {msp}, target_selling_price = {tsp}
                      WHERE listing_id = {listing_id}"""

FIXED_PRICE_CHECK_QUERY = """SELECT t.listing_id, t.lsp, t.msp, t.tsp, t.days_since_active FROM (SELECT l.id AS listing_id, l.price AS lsp, 
ldp.minimum_selling_price AS msp, ldp.target_selling_price AS tsp, SUM(DATEDIFF(IFNULL(DATE(lsl.end_time), DATE(NOW())), DATE(lsl.start_time))) AS days_since_active
FROM listings l JOIN localities lo ON l.locality_id = lo.id JOIN listing_dynamic_pricings ldp ON l.id = ldp.listing_id JOIN listing_status_lifeline lsl
ON l.id = lsl.listing_id WHERE l.status IN ('active', 'refurbishing') AND lo.city_id = {city_id} AND l.inventory_type_id IN (1,2) AND l.is_inventory = 1
AND (l.price <> ldp.target_selling_price OR ldp.minimum_selling_price <> ldp.target_selling_price OR l.price <> ldp.minimum_selling_price)
GROUP BY  l.id) AS t WHERE days_since_active > 3"""
