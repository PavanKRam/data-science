import sentry_sdk
from dynamic_pricing_code.helpers import sql_result, sql_result_insert, config_data
from dynamic_pricing_code.constants import CITY_ACTIVE_LISTINGS_QUERY, FIXED_PRICE_QUERY, FIXED_PRICE_UPDATE_QUERY, \
    LOG_INSERT_QUERY, LSP_INSERT_QUERY, UPDATE_LDP_QUERY, PREVIOUS_PRICE_LOG_QUERY

SENTRY_URL = config_data['sentry']['url']


def insert_fixed_price(fixed_price_data, city_active_listing_ids, city_active_listing_data):
    price_factor_mapping = {
        1.04: {'min': 0, 'max': 300000},
        1.02: {'min': 300000, 'max': 500000},
        1.01: {'min': 500000, 'max': float('inf')},
    }
    for listing_id in city_active_listing_ids:
        fixed_price = fixed_price_data.loc[fixed_price_data['listing_id'] == listing_id, ['msp']]
        prev_msp = sql_result(
            PREVIOUS_PRICE_LOG_QUERY.format(listing_id=listing_id, table_name='minimum_selling_price_logs'))
        if prev_msp.empty:
            prev_msp = fixed_price
        prev_lsp = sql_result(
            PREVIOUS_PRICE_LOG_QUERY.format(listing_id=listing_id, table_name='listing_price_changes')).iloc[0, 0]
        prev_tsp = sql_result(
            PREVIOUS_PRICE_LOG_QUERY.format(listing_id=listing_id, table_name='target_selling_price_logs'))
        if prev_tsp.empty:
            prev_tsp = fixed_price_data.loc[fixed_price_data['listing_id'] == listing_id, ['tsp']]
        days_since_active = city_active_listing_data.loc[
            city_active_listing_data['listing_id'] == listing_id, ['days_since_active']].iloc[0, 0]
        if days_since_active < 45:
            lsp = city_active_listing_data.loc[
                city_active_listing_data['listing_id'] == listing_id, ['lsp']].iloc[0, 0]
            for price_factor, price_range in price_factor_mapping.items():
                if int(lsp) >= price_range['min'] and int(lsp) < price_range['max']:
                    fixed_price = fixed_price.iloc[0, 0] * price_factor
                    fixed_price = int(round(fixed_price, -3))
        else:
            fixed_price = fixed_price.iloc[0, 0]
        prev_msp = prev_msp.iloc[0, 0]
        prev_tsp = prev_tsp.iloc[0, 0]

        sql_result_insert(FIXED_PRICE_UPDATE_QUERY.format(listing_id=listing_id, fixed_price=fixed_price))
        sql_result_insert(
            LOG_INSERT_QUERY.format(listing_id=listing_id, previous_price=prev_msp, updated_price=fixed_price,
                                    source_id=7, table_name='minimum_selling_price_logs'))
        sql_result_insert(
            LOG_INSERT_QUERY.format(listing_id=listing_id, previous_price=prev_tsp, updated_price=fixed_price,
                                    source_id=7, table_name='target_selling_price_logs'))
        sql_result_insert(
            LSP_INSERT_QUERY.format(listing_id=listing_id, previous_price=prev_lsp, updated_price=fixed_price,
                                    source_name='cron'))
        sql_result_insert(UPDATE_LDP_QUERY.format(listing_id=listing_id, msp=fixed_price, tsp=fixed_price))


def get_fixed_price_data(city_active_listing_ids):
    city_active_listing_ids = ",".join([str(x) for x in city_active_listing_ids])
    fixed_price_data = sql_result(FIXED_PRICE_QUERY.format(listing_ids=city_active_listing_ids))

    return fixed_price_data


def get_city_active_listings_data(city_id):
    city_active_listing_data = sql_result(CITY_ACTIVE_LISTINGS_QUERY.format(city_id=city_id))
    city_active_listing_ids = city_active_listing_data['listing_id'].tolist()

    return city_active_listing_ids, city_active_listing_data


def update_fixed_price(CITIES):
    for city_id in CITIES:
        city_active_listing_ids, city_active_listing_data = get_city_active_listings_data(city_id)
        fixed_price_data = get_fixed_price_data(city_active_listing_ids)
        insert_fixed_price(fixed_price_data, city_active_listing_ids, city_active_listing_data)


if __name__ == "__main__":
    sentry_sdk.init(SENTRY_URL)
    CITIES = [5]
    update_fixed_price(CITIES)
