import datetime

import numpy as np
import pandas as pd

from dynamic_pricing_code.helpers import sql_result, sql_result_insert, read_sheet, write_in_sheet, config_data
from dynamic_pricing_code.train_model import dataframe_type_conversion, remove_duplicate_entries, DPConstants, get_tsp_wrt_esp

CITY_PROC_SHEETS = config_data['sheet_keys']['procurement_sheet']


def get_source_id(name):
    source_id = sql_result((""" SELECT id from sources where name = '{0}' """.format(name)))
    return source_id


def price_changes_update(changed_data):
    changed_data = dataframe_type_conversion(changed_data)
    changed_data = changed_data.loc[~changed_data.isna().any(axis=1)]
    changed_data[changed_data.columns] = changed_data[changed_data.columns].astype('int')
    source_id = int(get_source_id("cron").iloc[0, 0])

    for i in range(changed_data.shape[0]):
        try:
            changed_values_list = list(changed_data.iloc[i].values)
            listing_id = changed_values_list[0]
            changed_values_list.insert(1, source_id)
            msp = changed_values_list[2]
            tsp = changed_values_list[3]
            prev_msp = changed_values_list[8]
            prev_tsp = changed_values_list[9]

            msp_insert_query = """INSERT INTO minimum_selling_price_logs (listing_id,source_id, previous_price, updated_price) VALUES ({listing_id}, {source_id}, {previous_price}, {updated_price})"""
            tsp_insert_query = """INSERT INTO target_selling_price_logs (listing_id,source_id, previous_price, updated_price) VALUES ({listing_id}, {source_id}, {previous_price}, {updated_price})"""

            # Existing entry check
            existing_entry_check = sql_result(
                """ SELECT * FROM listing_dynamic_pricings WHERE listing_id = %s """ % (str(listing_id)))

            if existing_entry_check.empty:
                listing_dynamic_pricing_insert_query = "INSERT INTO listing_dynamic_pricings (listing_id, source_id, minimum_selling_price, target_selling_price, gst_rate, refurbishing_cost, procurement_price) VALUES (%s, %s, %s, %s, %s, %s, %s)"
                sql_result_insert(
                    listing_dynamic_pricing_insert_query % (changed_values_list[0], changed_values_list[1],
                                                            changed_values_list[2], changed_values_list[3],
                                                            changed_values_list[4], changed_values_list[5],
                                                            changed_values_list[6]))
            else:
                # ldp update
                update_listing_query = """UPDATE listing_dynamic_pricings SET source_id = %s, minimum_selling_price = %s, target_selling_price = %s, gst_rate = %s, refurbishing_cost = %s, procurement_price = %s WHERE listing_id = %s"""
                sql_result_insert(update_listing_query % (changed_values_list[1], changed_values_list[2],
                                                          changed_values_list[3], changed_values_list[4],
                                                          changed_values_list[5], changed_values_list[6],
                                                          changed_values_list[0]))

            # tsp log insert
            previous_tsp_log_query = """SELECT updated_price
                                                            FROM target_selling_price_logs
                                                            WHERE listing_id = {listing_id}
                                                            ORDER BY created_at DESC
                                                            LIMIT 1 """
            previous_tsp_price = sql_result(previous_tsp_log_query.format(listing_id=listing_id))

            if not previous_tsp_price.empty:
                previous_tsp_price = previous_tsp_price.iloc[0, 0]
                if previous_tsp_price != tsp:
                    target_selling_price_change_log_id = sql_result_insert(tsp_insert_query.format(
                        listing_id=listing_id, source_id=source_id, previous_price=previous_tsp_price,
                        updated_price=tsp))
            elif prev_tsp != tsp:
                target_selling_price_change_log_id = sql_result_insert(tsp_insert_query.format(
                    listing_id=listing_id, source_id=source_id, previous_price=prev_tsp, updated_price=tsp))

            previous_msp_log_query = """SELECT updated_price
                                                                    FROM minimum_selling_price_logs
                                                                    WHERE listing_id = {listing_id}
                                                                    ORDER BY created_at DESC
                                                                    LIMIT 1 """
            previous_msp_price = sql_result(previous_msp_log_query.format(listing_id=listing_id))

            if not previous_msp_price.empty:
                previous_msp_price = previous_msp_price.iloc[0, 0]
                if previous_msp_price != msp:
                    minimum_selling_price_change_log_id = sql_result_insert(msp_insert_query.format(
                        listing_id=listing_id, source_id=source_id, previous_price=previous_msp_price,
                        updated_price=msp))
            elif prev_msp != msp:
                minimum_selling_price_change_log_id = sql_result_insert(msp_insert_query.format(
                    listing_id=listing_id, source_id=source_id, previous_price=prev_msp, updated_price=msp))

        except Exception as e:
            continue


def sheet_update(changed_data, city, data_to_update):
    range_index_mapping = {"msp": {"df_index": 1, "column_range": "{0}!AA3:AA"},
                           "tsp": {"df_index": 2, "column_range": "{0}!Z3:Z"},
                           "lsp": {"df_index": 3, "column_range": "{0}!AE3:AE"}}
    city_key = CITY_PROC_SHEETS['cities'][city]
    proc_sheet_name = CITY_PROC_SHEETS['name']
    for key in data_to_update:
        df_index = range_index_mapping[key]["df_index"]
        col_range = range_index_mapping[key]["column_range"]
        write_in_sheet([[i] for i in changed_data.iloc[:, df_index].values.tolist()],
                       city_key,
                       col_range.format(proc_sheet_name))


def get_neg_margin(proc_price, city_id):
    neg_margin_dict = DPConstants.CITY_WISE_NEG_MARGIN[str(city_id)]
    margin_slab = pd.cut([proc_price], [0, 200000, 500000, float("inf")], include_lowest=True)[0]
    margin = neg_margin_dict.get(str(margin_slab), neg_margin_dict['(0.0, 200000.0]'])

    return margin


def price_change_calculation(city):
    procurement_sheet = pd.DataFrame(read_sheet(CITY_PROC_SHEETS['cities'][city], CITY_PROC_SHEETS['name']))
    [procurement_sheet, duplicate_data] = remove_duplicate_entries(procurement_sheet, True)
    listing_ages = sql_result("""select id  from listings where year(now())-year(registration_date)<=1 and status in 
    ('active', 'refurbishing') and is_inventory=1""")

    if city == 'mumbai':
        city_id = 1
    elif city == 'delhi':
        city_id = 2
    elif city == 'bangalore':
        city_id = 5

    if procurement_sheet.shape[0] > 0:
        ids = []
        procurement_last_row = int(procurement_sheet.iloc[0, 39])
        PS_data = procurement_sheet.iloc[2:procurement_last_row, :].copy()
        PS_listing_ids = PS_data.iloc[:, 2]
        MSP_data = PS_data.iloc[:, 26]
        TSP_data = PS_data.iloc[:, 25]
        LSP_data = PS_data.iloc[:, 30]
        ESP_data = PS_data.iloc[:, 49]
        range_data = PS_data.iloc[:, 51]
        Procurement_price_data = PS_data.iloc[:, 23]

        for i in range(procurement_last_row - 2):

            # checks if car is live
            try:
                days_since_active = (datetime.datetime.today() - pd.to_datetime(PS_data.iloc[i, 14])).days
            except Exception as e:
                continue

            # checks if the listing has a website listing_id
            if PS_data.iloc[i, 2] == '' or PS_data.iloc[i, 49] == '' or (PS_data.iloc[i, 2] in ('57969', '57815')):
                continue

            status_data = (PS_data.iloc[i, 29].upper() in (
                ['LIVE', 'SENT OUT FOR P&S', 'TO BE OFFLOADED', 'UNDER REFURBISHMENT']))

            if ((PS_data.iloc[i, 26] != '') and (
                    PS_data.iloc[i, 49] != '') and (PS_data.iloc[i, 19] != '') and (
                    PS_data.iloc[i, 14] != '') and status_data and (PS_data.iloc[i, 3] == 'Truebil')):

                lsp_curr = float(LSP_data.iloc[i])
                esp_curr = float(ESP_data.iloc[i])
                proc_price = int(Procurement_price_data.iloc[i])
                listing_id = int(PS_listing_ids.iloc[i])

                # check and fix negotiation margin
                msp_curr = float(MSP_data.iloc[i])
                margin_new = get_neg_margin(proc_price, city_id)
                if listing_id in listing_ages.id.values:
                    margin_new = max(margin_new - 0.02, 0.07)

                msp_new = round((lsp_curr - margin_new * esp_curr), -3)
                TSP_data.iloc[i] = get_tsp_wrt_esp(int(TSP_data.iloc[i]), city_id, msp_new, esp_curr,
                                                   input_params=np.array([lsp_curr]))
                if msp_new > (msp_curr):
                    MSP_data.iloc[i] = msp_new
                    ids.append(listing_id)
    data_to_update = ["msp", "tsp"]
    listing_ids = list(set(ids))
    final = pd.DataFrame()
    final['id'] = PS_data.iloc[:, 2]
    final['msp'] = MSP_data
    final['tsp'] = TSP_data
    final['lsp'] = LSP_data
    final['range'] = range_data
    final['gst_rate'] = PS_data.iloc[:, 43]
    final['refurb_cost'] = PS_data.iloc[:, 19]
    final['procurement_price'] = PS_data.iloc[:, 23]
    final['prev_msp'] = procurement_sheet.iloc[2:procurement_last_row, 26]
    final['prev_tsp'] = procurement_sheet.iloc[2:procurement_last_row, 25]
    final['prev_lsp'] = procurement_sheet.iloc[2:procurement_last_row, 30]

    final = dataframe_type_conversion(final)
    final['gst_rate'] = final['gst_rate'].fillna(18)
    final = final.fillna('')
    changed_data = final[final['id'].isin(listing_ids)][
        ['id', 'msp', 'tsp', 'gst_rate', 'refurb_cost', 'procurement_price', 'lsp', 'prev_msp', 'prev_tsp', 'prev_lsp']]

    duplicate_data = duplicate_data[[2, 26, 25, 30, 51]]
    duplicate_data.columns = ['id', 'msp', 'tsp', 'lsp', 'range']
    duplicate_data = dataframe_type_conversion(duplicate_data).fillna('')

    final = final.iloc[:, :5].append(duplicate_data).sort_index()
    price_changes_update(changed_data)
    sheet_update(final, city, data_to_update)


if __name__ == "__main__":
    # iterate over city ids
    for city_sheet_name in CITY_PROC_SHEETS['cities']:
        price_change_calculation(city_sheet_name)
