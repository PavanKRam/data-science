import ast
import datetime
import math
import os
import random

import numpy as np
import pandas as pd
import sentry_sdk

from dynamic_pricing_code.constants import INITIAL_PRICE_CHECK_QUERY, GET_TOKENS_QUERY, GET_LIVE_CARS_QUERY, \
    INITIAL_PRICE_PARAMS_QUERY, DEPICTED_MP_QUERY, DEPICTED_MP_UPDATE_QUERY, FIXED_PRICE_CHECK_QUERY

os.environ['SLACK_LOGGING'] = 'TRUE'
os.environ['LOG_FILENAME'] = 'initial_pricing.log'
from dynamic_pricing_code.fixed_price_setting import insert_fixed_price
from dynamic_pricing_code.helpers import sql_result, config_data, add_log, send_log_to_slack, read_sheet, \
    get_formatted_columns
from dynamic_pricing_code.train_model import remove_duplicate_entries, db_sheet_sync, get_tsp_wrt_esp, \
    new_listings_addition, get_neg_margin, psychological_price, get_dealer_neg_margin, \
    get_source_name_from_source_table, get_stock_performance, get_lower_bound_msp, DPConstants, final_data_preparation
from ds_helpers.sheet_db_sync import SheetDbSync
from dynamic_pricing_code.sheet_logging import SheetUpdate
from dynamic_pricing_code.utils.helpers import get_cluster_wise_z_scores

SENTRY_URL = config_data['sentry']['url']


class InitialPriceConstants:
    initial_price_params_df = sql_result(INITIAL_PRICE_PARAMS_QUERY).set_index('name')
    initial_price_params_df['value'] = initial_price_params_df['value'].apply(ast.literal_eval)
    INITIAL_PRICE_MARGIN_PARAMS = initial_price_params_df.loc['initial_price_margin_params', 'value']
    BENCHMARK_SP_DATA = initial_price_params_df.loc['benchmark_stock_performance', 'value']
    CLUSTER_WISE_Z_SCORE = get_cluster_wise_z_scores()
    CLUSTER_STOCK_PERFORMANCE = get_stock_performance(GET_TOKENS_QUERY, GET_LIVE_CARS_QUERY, BENCHMARK_SP_DATA,
                                                      days_slab=8, cluster_flag=True)


def update_db_data(proc_data):
    update_string = """UPDATE listing_dynamic_pricings SET estimated_selling_price = {estimated_selling_price},
    actual_refurb_cost = {actual_refurb_cost}, payment_date = {payment_date}, seller_type = {seller_type},
    seller_name = {seller_name} WHERE listing_id = {listing_id};"""
    for row in proc_data.itertuples():
        update_data = update_string.format(estimated_selling_price=row.estimated_selling_price,
                                           actual_refurb_cost=row.actual_refurb_cost, payment_date=row.payment_date,
                                           seller_type=row.seller_type, seller_name=row.seller_name,
                                           listing_id=row.listing_id)
        sql_result(update_data)


def sheet_db_sync(proc_sheet, inventory_type_id, city_id):
    fields_dict = {2: 'listing_id', 49: 'estimated_selling_price', 20: 'actual_refurb_cost', 10: 'payment_date',
                   5: 'seller_type', 4: 'seller_name'}
    fields_data_types = {2: 'int', 49: 'int', 20: 'int', 10: 'datetime', 5: 'str', 4: 'str'}
    capitalize_columns = [5, 4]

    sheet_db = SheetDbSync(proc_sheet, fields_dict, fields_data_types, capitalize_columns)
    proc_data = sheet_db.get_proc_sheet_data(inventory_type_id, city_id)
    proc_data = get_formatted_columns(proc_data, ['payment_date', 'seller_type', 'seller_name'])
    proc_data = proc_data.replace({'': 'NULL', "''": 'NULL', "'NaT'": 'NULL'})
    update_db_data(proc_data)


def get_listing_benchmark_data(city_id, esp, body_type, slab_cluster_data=None):
    """
    Identifies the cluster of given listing id and it's SP benchmark & Z score
    :param city_id: City Id
    :param esp: Estimated Selling Price of that listing
    :param body_type: Body Type
    :param slab_cluster_data: Use slab wise cluster data if given
    :return: listing z score, stock performance
    """
    city_wise_z_score = InitialPriceConstants.CLUSTER_WISE_Z_SCORE[city_id]
    if slab_cluster_data:
        city_cluster_data = slab_cluster_data[city_id]
    else:
        city_cluster_data = InitialPriceConstants.CLUSTER_STOCK_PERFORMANCE[city_id]

    price_cluster = pd.cut([esp], [0, 300000, 500000, float("inf")], labels=['0_3', '3_5', 'a_5'])[0]
    valid_body_types = ['Hatchback', 'Sedan', 'MPV', 'SUV']
    cluster_index = '{body_type}_{cluster}'.format(body_type=body_type, cluster=price_cluster)

    if body_type in valid_body_types[-2:]:
        default_cluster = body_type
    else:
        default_cluster = 'price_{cluster}'.format(cluster=price_cluster)

    listing_benchmark_score = city_wise_z_score.get(cluster_index, city_wise_z_score[default_cluster])
    listing_sp_data = city_cluster_data.get(cluster_index, city_cluster_data[default_cluster])

    return listing_benchmark_score, listing_sp_data


def get_margin_on_price_buckets(price, city_id):
    """
    Fetches markup values from database and calculates margin range cluster wise
    :param price: Price
    :param city_id: City Id
    :return: [max_margin, min_margin]
    """
    markup_data = InitialPriceConstants.INITIAL_PRICE_MARGIN_PARAMS['markup_limits']

    if (city_id is 1 and price < 200000) or (
            city_id in (2, 5) and price < 300000):
        return [markup_data['max'], markup_data['min']]
    elif (city_id is 1 and 200000 <= price < 400000) or (
            city_id is 2 and 300000 <= price < 500000) or (
            city_id is 5 and 300000 <= price < 600000):
        return [np.ceil(markup_data['max'] * 0.7), markup_data['min']]
    else:
        return [np.ceil(markup_data['max'] * 0.35), markup_data['min']]


def get_listing_initial_margin(z_score, sp_data, initial_margin_range_dict):
    scaling_factor = (np.exp(sp_data) - 1) / (np.exp(1) - 1)
    mean_initial_margin = np.floor(initial_margin_range_dict['min'] +
                                   0.50 * (initial_margin_range_dict['max'] - initial_margin_range_dict['min']))

    if z_score >= 0:
        initial_margin = (mean_initial_margin + (z_score / 5) *
                          (initial_margin_range_dict['max'] - mean_initial_margin)) * scaling_factor
    else:
        initial_margin = (mean_initial_margin - (z_score / 3) *
                          (initial_margin_range_dict['min'] - mean_initial_margin)) * scaling_factor

    initial_margin = max(min(initial_margin, initial_margin_range_dict['max']), initial_margin_range_dict['min'])

    return initial_margin


def get_initial_margin_age(listing_id, proc_price, city_id, esp, inventory_type=2, dealer_neg_margin=5):
    price_body_type_query = """SELECT price, body_type_id, registration_date FROM listings WHERE id = {listing_id}"""
    price_body_type_df = sql_result(price_body_type_query.format(listing_id=listing_id))
    body_type_id = price_body_type_df.iloc[0, 1]
    registration_date = price_body_type_df.iloc[0, 2]
    age = TODAY.year - registration_date.year

    z_score, sp_data = get_listing_benchmark_data(city_id, esp, body_type_id)
    initial_margin_range = get_margin_on_price_buckets(proc_price, city_id)
    initial_margin_range_dict = dict(zip(['max', 'min'], initial_margin_range))
    initial_margin = 0

    if inventory_type == 2:
        initial_margin = get_listing_initial_margin(z_score, sp_data, initial_margin_range_dict)

    if inventory_type == 1:
        dnm = min(dealer_neg_margin * 0.01 * proc_price, 30000)
        mmsp = proc_price - dnm
        price_breaks = max((float(esp - mmsp) / 16), 0)  # to ensure msp never falls below mmsp

        if z_score >= -4:
            margin_fac = int((z_score + 4) / 0.25)
            msp = mmsp + margin_fac * price_breaks
            initial_margin = (float(msp - esp) / esp) * 100
        else:
            initial_margin = (float(mmsp - esp) / esp) * 100

    return np.ceil(initial_margin), age


def get_initial_prices(city, neg_margin, initial_margin, age, esp, lower_bound_msp, tsp):
    if age <= 1:
        initial_margin = math.ceil(min(0.6 * initial_margin, 6))
        neg_margin = max(neg_margin - 0.02, 0.07)
    initial_margin = float(1 + float(initial_margin / 100))
    msp = float(round(initial_margin * esp, -3))
    if msp < round(lower_bound_msp, -3):
        msp = round(lower_bound_msp, -3)
        initial_margin = round(float(msp / esp), 2)
    tsp = get_tsp_wrt_esp(tsp=tsp, city_id=city, msp=msp, esp=esp, input_params=np.array([initial_margin, neg_margin]),
                          initial_check=True)
    lsp = float(round((initial_margin + neg_margin) * esp, -3))
    lsp = float(round(psychological_price(lsp, msp, esp), -3))
    source_change = int(SOURCE_NAME_TO_ID_DICT['dp_initial'])

    return lsp, tsp, msp, source_change


def set_depicted_price(listing_id, lsp_data):
    depicted_mp = int(sql_result(DEPICTED_MP_QUERY.format(listing_id=listing_id)).iloc[0, 0])

    if depicted_mp < lsp_data:
        depicted_mp = lsp_data + random.randint(5, 10) * 1000
        sql_result(DEPICTED_MP_UPDATE_QUERY.format(listing_id=listing_id, depicted_price=depicted_mp))


def set_initial_prices(city):
    add_log("INITIAL PRICES FUNCTION STARTED : " + city)
    sheet_key = CITY_PROC_SHEETS['cities'][city]
    procurement_sheet = pd.DataFrame(read_sheet(sheet_key, CITY_PROC_SHEETS['name']))
    [procurement_sheet, duplicate_data] = remove_duplicate_entries(procurement_sheet, True)

    city_id = DPConstants.CITY_NAME_TO_ID_DICT[city]

    try:
        new_listings_addition(procurement_sheet, city_id)
    except Exception as err:
        add_log(str(err), True)

    try:
        db_sheet_sync(procurement_sheet, duplicate_data, sheet_key, city_id)
        sheet_db_sync(procurement_sheet, 2, city_id)
    except Exception as err:
        add_log(str(err), True)

    procurement_last_row = int(procurement_sheet.iloc[0, 39])
    ps_data = procurement_sheet.iloc[2:procurement_last_row, :].copy()
    new_gst = float(procurement_sheet.iloc[0, 41]) * 100
    msp_data = ps_data.iloc[:, 26]
    tsp_data = ps_data.iloc[:, 25]
    lsp_data = ps_data.iloc[:, 30]
    proc_price_data = ps_data.iloc[:, 23]
    actual_rfc_data = ps_data.iloc[:, 20]
    estimated_rfc_data = ps_data.iloc[:, 19]
    range_data = ps_data.iloc[:, 51]
    listing_ids = []
    source_change = pd.Series().reindex_like(ps_data.iloc[:, 30]).fillna('')

    for i in range(procurement_last_row - 2):

        try:
            days_since_active = (TODAY - pd.to_datetime(ps_data.iloc[i, 14])).days
        except Exception:
            continue

        if (ps_data.iloc[i, 2] == '') or (days_since_active > 3) or (int(ps_data.iloc[i, 2]) == 10):
            continue

        initial_price_check = sql_result(INITIAL_PRICE_CHECK_QUERY.format(listing_id=ps_data.iloc[i, 2]))
        if not initial_price_check.empty:
            continue

        if not (ps_data.iloc[i, 43] != "#N/A" and ps_data.iloc[i, 43] != ''):
            ps_data.iloc[i, 43] = new_gst

        status_data = (ps_data.iloc[i, 29].upper() in (
            ['LIVE', 'SENT OUT FOR P&S', 'TO BE OFFLOADED', 'UNDER REFURBISHMENT']))

        if ((ps_data.iloc[i, 26] != '') and (ps_data.iloc[i, 19] != '') and (
                ps_data.iloc[i, 14] != '') and status_data and (
                ps_data.iloc[i, 3] != 'Park and Sell') and (ps_data.iloc[i, 2] != '') and ps_data.iloc[i, 23] != ''):

            listing_id = ps_data.iloc[i, 2]
            proc_price = int(proc_price_data.iloc[i])

            try:
                cost_price = int(actual_rfc_data.iloc[i]) + proc_price
            except ValueError:
                cost_price = int('0' + estimated_rfc_data.iloc[i]) + proc_price

            if ps_data.iloc[i, 49] != '':
                esp = int(ps_data.iloc[i, 49])
            else:
                esp = round(cost_price * 1.15, -3)

            initial_margin, age = get_initial_margin_age(listing_id, proc_price, city_id, esp)

            lower_bound_msp = get_lower_bound_msp(days_since_active, esp, cost_price, inventory_type=2)
            neg_margin = get_neg_margin(proc_price, city_id)
            lsp_data.iloc[i], tsp_data.iloc[i], msp_data.iloc[i], source_change.iloc[i] = get_initial_prices(
                city_id, neg_margin, initial_margin, age, esp, lower_bound_msp, int(float(tsp_data.iloc[i]))
            )
            listing_ids.append(int(listing_id))

            try:
                set_depicted_price(listing_id, lsp_data.iloc[i])
            except Exception:
                continue

    column_names = filter(lambda x: x not in ['q_table_id'], DPConstants.COLUMN_DATA_DICTIONARY)

    data_frame_values = [ps_data.iloc[:, 2], msp_data, tsp_data, lsp_data, range_data,
                         ps_data.iloc[:, 43], ps_data.iloc[:, 19],
                         ps_data.iloc[:, 23], procurement_sheet.iloc[2:procurement_last_row, 26],
                         procurement_sheet.iloc[2:procurement_last_row, 25],
                         procurement_sheet.iloc[2:procurement_last_row, 30], source_change]

    data_frame_values = [column.values.tolist() for column in data_frame_values]
    final = pd.DataFrame(dict(zip(column_names, data_frame_values)))

    final_data_preparation(final, listing_ids, city_id)
    add_log("INITIAL PRICES FUNCTION COMPLETED : " + city)


def park_sell_set_initial_prices(city):
    sheet_key = PARK_SELL_DICT[city]
    add_log("INITIAL PRICES (PARK N SELL) FUNCTION STARTED : " + city)
    park_n_sell_sheet = pd.DataFrame(read_sheet(sheet_key, CITY_PROC_SHEETS['name']))
    [park_n_sell_sheet, duplicate_data] = remove_duplicate_entries(park_n_sell_sheet, True)

    dealer_wise_neg_margin_dict = get_dealer_neg_margin(sheet_key)
    city_id = DPConstants.CITY_NAME_TO_ID_DICT[city]

    try:
        new_listings_addition(park_n_sell_sheet, city_id, inventory_type=1)
    except Exception as err:
        add_log(str(err), True)

    try:
        db_sheet_sync(park_n_sell_sheet, duplicate_data, sheet_key, city_id, inventory_type=1)
        sheet_db_sync(park_n_sell_sheet, 1, city_id)
    except Exception as err:
        add_log(str(err), True)

    last_row = int(park_n_sell_sheet.iloc[0, 39])
    ps_data = park_n_sell_sheet.iloc[2:last_row, :].copy()
    listing_id_data = ps_data.iloc[:, 2]
    dealer_name = ps_data.iloc[:, 4]
    msp_data = ps_data.iloc[:, 26]
    lsp_data = ps_data.iloc[:, 30]
    cp_data = ps_data.iloc[:, 19]
    tsp_data = ps_data.iloc[:, 25]
    current_status = ps_data.iloc[:, 29]
    esp_data = ps_data.iloc[:, 49]
    car_live_date = ps_data.iloc[:, 14]
    gst_data = ps_data.iloc[:, 43]
    new_gst = float(park_n_sell_sheet.iloc[0, 41]) * 100
    listing_ids = []
    source_change = pd.Series().reindex_like(ps_data.iloc[:, 30]).fillna('')

    for i in range(last_row - 2):

        try:
            days_since_active = (TODAY - pd.to_datetime(car_live_date.iloc[i])).days
        except IndexError:
            continue

        try:
            listing_id = int(listing_id_data.iloc[i])
        except ValueError:
            continue

        if current_status.iloc[i] not in ('Live', 'Under Refurbishment') or (days_since_active > 3):
            continue

        initial_price_check = sql_result(INITIAL_PRICE_CHECK_QUERY.format(listing_id=listing_id))
        if not initial_price_check.empty:
            continue

        try:
            cp_data.iloc[i] = int(cp_data.iloc[i])
            proc_price = int(cp_data.iloc[i])
        except (ValueError, TypeError):
            continue
        try:
            esp = int(esp_data.iloc[i])
        except (ValueError, TypeError):
            esp = cp_data.iloc[i]
        if esp == 0:
            continue

        if gst_data.iloc[i] in ("#N/A", ""):
            gst_data.iloc[i] = new_gst

        dealer_neg_margin = dealer_wise_neg_margin_dict.get(dealer_name.iloc[i], 2.5)
        if dealer_neg_margin < 0:
            dealer_neg_margin = 0

        initial_margin, age = get_initial_margin_age(listing_id, proc_price, city_id, esp, inventory_type=1,
                                                     dealer_neg_margin=dealer_neg_margin)
        neg_margin = get_neg_margin(proc_price, city_id)
        lower_bound_msp = get_lower_bound_msp(days_since_active, esp, proc_price, inventory_type=1,
                                              dealer_neg_margin=dealer_neg_margin)
        lsp_data.iloc[i], tsp_data.iloc[i], msp_data.iloc[i], source_change.iloc[i] = get_initial_prices(
            city_id, neg_margin, initial_margin, age, esp, lower_bound_msp, int(float(tsp_data.iloc[i]))
        )
        listing_ids.append(int(listing_id))

        try:
            set_depicted_price(listing_id, lsp_data.iloc[i])
        except Exception:
            continue

    column_names = filter(lambda x: x not in ['range', 'q_table_id'], DPConstants.COLUMN_DATA_DICTIONARY)

    data_frame_values = [ps_data.iloc[:, 2], msp_data, tsp_data, lsp_data, gst_data,
                         ps_data.iloc[:, 20].replace('', 0), cp_data,
                         park_n_sell_sheet.iloc[2:last_row, 26], park_n_sell_sheet.iloc[2:last_row, 25],
                         park_n_sell_sheet.iloc[2:last_row, 30], source_change]

    data_frame_values = [column.values.tolist() for column in data_frame_values]
    final = pd.DataFrame(dict(zip(column_names, data_frame_values)))
    final_data_preparation(final, listing_ids, city_id)
    add_log("INITIAL PRICES (PARK N SELL) FUNCTION COMPLETED : " + city)


def get_proc_price_wise_cluster(esp, body_type, city_id, slab_cluster_data, slab_data):
    proc_price_clusters = [100001, 300001, 600001]
    esp_index = np.floor(esp / 100000)
    slab_data[city_id][body_type][esp_index] = {}
    cluster_z_score, cluster_sp_data = get_listing_benchmark_data(city_id, esp, body_type,
                                                                  slab_cluster_data)
    cluster_dict = {'z_score': cluster_z_score, 'sp_data': cluster_sp_data,
                    'scaling_factor': (np.exp(cluster_sp_data) - 1) / (np.exp(1) - 1)}
    for proc_price in proc_price_clusters:
        proc_price_index = np.floor(proc_price / 100000)
        slab_data[city_id][body_type][esp_index][proc_price_index] = cluster_dict
        initial_margin_range = get_margin_on_price_buckets(proc_price, city_id)
        initial_margin_range_dict = dict(zip(['max', 'min'], initial_margin_range))
        initial_margin = get_listing_initial_margin(cluster_z_score, cluster_sp_data,
                                                    initial_margin_range_dict)
        slab_data[city_id][body_type][esp_index][proc_price_index][
            'initial_margin'] = np.ceil(initial_margin)

    return slab_data


def log_stock_performance():
    """
    Logs slab and city wise cluster z score, stock performance and initial margin data into spreadsheet
    """
    all_slabs_sp = get_stock_performance(GET_TOKENS_QUERY, GET_LIVE_CARS_QUERY, InitialPriceConstants.BENCHMARK_SP_DATA,
                                         days_slab=8, cluster_flag=True, data_logging=True)
    esp_clusters = [100001, 300001, 500001]
    body_types = ['Hatchback', 'Sedan', 'MPV', 'SUV', 'Others']
    all_slab_data = []
    for slab_cluster_data in all_slabs_sp.values():
        slab_data = dict()
        for city_id in [1, 2, 5]:
            slab_data[city_id] = {}
            for body_type in body_types:
                slab_data[city_id][body_type] = {}
                for esp in esp_clusters:
                    slab_data = get_proc_price_wise_cluster(esp, body_type, city_id, slab_cluster_data, slab_data)

        all_slab_data.append(slab_data)
    sheet_data_dict = dict(zip(all_slabs_sp.keys(), all_slab_data))
    sheet_instance = SheetUpdate("stock_performance_sheet")
    sheet_instance.log_cluster_stock_performance(sheet_data_dict)


def check_if_fixed_price_is_set(city_id):
    not_set_listing_data = sql_result(FIXED_PRICE_CHECK_QUERY.format(city_id=city_id))
    if not_set_listing_data.empty:
        return
    not_set_listing_ids = not_set_listing_data['listing_id'].tolist()
    insert_fixed_price(not_set_listing_data, not_set_listing_ids, not_set_listing_data)


if __name__ == "__main__":
    CITY_PROC_SHEETS = config_data['sheet_keys']['procurement_sheet']
    PARK_SELL_DICT = config_data['sheet_keys']['park_n_sell']['cities']
    SOURCE_NAME_TO_ID_DICT, SOURCE_ID_TO_NAME_DICT = get_source_name_from_source_table()
    TODAY = datetime.datetime.today()
    add_log("INITIAL PRICE SETTING STARTED")
    sentry_sdk.init(SENTRY_URL)
    for city_name in DPConstants.CITY_NAME_TO_ID_DICT:
        try:
            set_initial_prices(city_name)
        except Exception as e:
            add_log(str(e), True)
        if city_name in list(PARK_SELL_DICT.keys()):
            try:
                park_sell_set_initial_prices(city_name)
            except Exception as e:
                add_log(str(e), True)
        if city_name == 'bangalore':
            city_id = DPConstants.CITY_NAME_TO_ID_DICT[city_name]
            check_if_fixed_price_is_set(city_id)
    add_log("INITIAL PRICE SETTING COMPLETED")
    current_time = datetime.datetime.now()
    if current_time.hour == 11 and 0 <= current_time.minute <= 13:
        log_stock_performance()
    if current_time.hour == 23:
        send_log_to_slack(str(datetime.date.today()))
