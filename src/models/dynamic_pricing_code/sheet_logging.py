import pandas as pd

from dynamic_pricing_code.helpers import config_data, write_in_sheet


class SheetUpdate:
    """
    Updates data into specified spreadsheet
    """

    def __init__(self, sheet_name):
        self.spread_sheet = config_data["sheet_keys"][sheet_name]

    @staticmethod
    def reform_4_level_dict(nested_dict):
        """
        Converts a 4 level nested dict into multi_indexed dataframe
        :param nested_dict: dictionary
        :return: Dataframe
        """
        reform = {(level1_key, level2_key, level3_key, level4_key): values
                  for level1_key, level2_dict in nested_dict.items()
                  for level2_key, level3_dict in level2_dict.items()
                  for level3_key, level4_dict in level3_dict.items()
                  for level4_key, values in level4_dict.items()
                  }
        return pd.DataFrame(reform)

    def log_cluster_stock_performance(self, slab_data_dict):
        """
        Logs cluster wise z_score, stock_performance, initial margin data
        :param slab_data_dict: slab data dictionary
        """
        slab_location_data = {
            '7_days_sp': 'B6:EF9',
            '14_days_sp': 'B17:EF20',
            '7_14_days_sp': 'B28:EF31'
        }
        for slab, location in slab_location_data.items():
            sp_dataframe = self.reform_4_level_dict(slab_data_dict[slab])
            write_in_sheet(sp_dataframe.values.tolist(), self.spread_sheet["sheet_key"], "{sheet_name}!{location}"
                           .format(sheet_name=self.spread_sheet["cluster_sheet_name"], location=location))

    def log_overall_stock_performance(self, sp_data):
        """
        Logs overall city wise stock performance data
        """
        sp_list = list(sp_data.items())
        write_in_sheet(sp_list, self.spread_sheet["sheet_key"], "{sheet_name}!A2:B4"
                       .format(sheet_name=self.spread_sheet["overall_sheet_name"]))
