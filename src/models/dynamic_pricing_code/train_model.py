import datetime
import json
import math
import os
from sys import exit

import numpy as np
import pandas as pd
import sentry_sdk

import calendar
from dynamic_pricing_code import constants

os.environ['SLACK_LOGGING'] = 'TRUE'
os.environ['LOG_FILENAME'] = 'dynamic_pricing.log'
from dynamic_pricing_code.sheet_logging import SheetUpdate
from dynamic_pricing_code.utils.helpers import get_days_range, get_city_benchmark_sp
from dynamic_pricing_code.helpers import sql_result, sql_result_insert, read_sheet, write_in_sheet, add_log, \
    config_data, get_reward_data, send_log_to_slack, convert_column_data_types, get_formatted_columns, \
    get_formatted_list, dataframe_type_conversion, get_city_data_mapping_dict, get_source_name_from_source_table

SENTRY_URL = config_data['sentry']['url']


class DPConstants:
    CITY_WISE_MAX_ACTION_DICT = sql_result(
        constants.GET_MAX_ACTION_CITY_AND_STATE_WISE).set_index('city_id').to_dict()['max_action']
    CITY_WISE_NEG_MARGIN = json.loads(
        sql_result("""SELECT value FROM global_constants WHERE name = 'city_neg_margin'""").values.flatten()[0])
    FACTOR_SHEET_DATA = config_data['sheet_keys']['factor_sheet']
    CITY_PROC_SHEETS = config_data['sheet_keys']['procurement_sheet']
    PARK_SELL_DICT = config_data['sheet_keys']['park_n_sell']['cities']
    TODAY = datetime.date.today()
    SOURCE_NAME_TO_ID_DICT, SOURCE_ID_TO_NAME_DICT = get_source_name_from_source_table()
    CITY_ID_TO_NAME_DICT, CITY_NAME_TO_ID_DICT = get_city_data_mapping_dict()
    LIVE_STATUS_DATA = ['LIVE', 'OUT FOR P&S', 'TO BE OFFLOADED', 'UNDER REFURBISHMENT']
    COLUMN_DATA_DICTIONARY = ['id', 'msp', 'tsp', 'lsp', 'range', 'gst_rate', 'refurb_cost', 'procurement_price',
                              'prev_msp', 'prev_tsp', 'prev_lsp', 'source_change', 'q_table_id']


def remove_duplicate_entries(procurement_sheet, duplicate_check=False):
    duplicate_entries = procurement_sheet.groupby([2])[0].size()
    duplicate_entries_index = list(duplicate_entries[duplicate_entries > 1].index)
    if '' in duplicate_entries_index:
        duplicate_entries_index.remove('')
    duplicate_entries_count = len(duplicate_entries_index)
    duplicate_rows = []

    if duplicate_entries_count != 0:
        for i in duplicate_entries_index:
            duplicate_row_index = procurement_sheet[procurement_sheet[2] == i].index[:-1]
            duplicate_rows.extend(duplicate_row_index)

    duplicate_entries_dataframe = procurement_sheet.iloc[duplicate_rows]
    procurement_sheet = procurement_sheet.drop(duplicate_rows)

    if duplicate_check:
        return procurement_sheet, duplicate_entries_dataframe

    return procurement_sheet


def get_active_listings_from_sheet(proc_sheet, column_list):
    active_listings_data = proc_sheet[(proc_sheet[29].str.upper().isin(ACTIVE_STATUS_LIST))][column_list]
    active_listings_data = active_listings_data[active_listings_data.iloc[:, 0].str.isdigit()].sort_values([2])
    active_listings_data = active_listings_data.reset_index(drop=True).apply(pd.to_numeric)

    return active_listings_data


# Database To Sheet : LDP Changes Update
def db_sheet_sync(proc_sheet, duplicate_rows, sheet_key, city_id, inventory_type=2):
    """
    Synchronizes any changed data in database into procurement sheet
    :param proc_sheet: Procurement Sheet Data
    :param duplicate_rows: Duplicate date previously removed, to be appended before updating
    :param sheet_key: Sheet Key
    :param city_id: City Id
    :param inventory_type: Inventory Type
    """
    add_log("DB SYNC STARTED FOR CITY_ID " + str(city_id) + str(inventory_type))

    procurement_last_row = int(proc_sheet.iloc[0, 39])
    column_list = [2, 26, 25, 30]  # [id, msp, tsp, lsp]
    active_listings_data = get_active_listings_from_sheet(proc_sheet, column_list)
    previous_ldp_data_query = """SELECT l.id, ldp.minimum_selling_price, ldp.target_selling_price, l.price FROM 
    listing_dynamic_pricings ldp JOIN listings l ON ldp.listing_id = l.id JOIN localities lo ON l.locality_id = lo.id 
    WHERE ldp.listing_id IN {listing_ids} AND lo.city_id = {city_id} AND l.inventory_type_id = {inventory_type} 
    ORDER BY l.id"""

    try:

        listings_data_in_db = sql_result(previous_ldp_data_query
                                         .format(listing_ids=tuple(active_listings_data.iloc[:, 0].tolist()),
                                                 city_id=city_id, inventory_type=inventory_type))
        active_listings_data = active_listings_data[active_listings_data.iloc[:, 0].isin(listings_data_in_db['id'])]
        changed_data_bool = np.subtract(active_listings_data.values, listings_data_in_db.values).any(1)
        changed_indices = np.where(changed_data_bool)[0]
        changed_data = listings_data_in_db.loc[changed_indices, :]
        duplicate_dataframe = duplicate_rows[column_list]

        if changed_data.shape[0] > 0:
            # MSP, TSP, LSP
            columns_range = ['AA3:AA', 'Z3:Z', 'AE3:AE']
            columns_to_update = [col + str(procurement_last_row) for col in columns_range]
            listing_ids = changed_data.iloc[:, 0].values.astype(str)
            sheet_data = proc_sheet[proc_sheet.iloc[:, 2].isin(listing_ids)][column_list].apply(pd.to_numeric)
            changed_sheet_indices = sheet_data.sort_values([2]).index
            proc_sheet.loc[changed_sheet_indices, column_list] = changed_data.values.astype('str')
            update_data = proc_sheet[column_list].append(duplicate_dataframe).sort_index()
            update_data[26] = pd.to_numeric(update_data[26], errors='coerce')
            update_data[25] = pd.to_numeric(update_data[25], errors='coerce')
            update_data[30] = pd.to_numeric(update_data[30], errors='coerce')
            update_data = update_data.fillna('')

            for row in range(3):
                write_in_sheet([[price] for price in update_data.iloc[2:procurement_last_row, row + 1].values.tolist()],
                               sheet_key, "{proc_sheet_name}!{proc_sheet_range}"
                               .format(proc_sheet_name=CITY_PROC_SHEETS['name'],
                                       proc_sheet_range=columns_to_update[row]))

    except Exception as e:
        add_log(str(e), True)

    add_log("DB SHEET SYNC COMPLETED FOR CITY_ID " + str(city_id) + str(inventory_type))


def insert_new_data_in_ldp(newly_added_listings):
    source_id = str(DPConstants.SOURCE_NAME_TO_ID_DICT['sheet'])
    listing_id_string = get_formatted_list(newly_added_listings['listing_ids'].tolist())
    existing_data = sql_result("""SELECT listing_id FROM listing_dynamic_pricings where listing_id in ({listing_ids})"""
                               .format(listing_ids=listing_id_string))
    to_be_inserted_data = newly_added_listings[~newly_added_listings['listing_ids'].isin(existing_data['listing_id'])]
    to_be_inserted_data.loc[:, 'payment_date'] = pd.to_datetime(to_be_inserted_data.loc[:, 'payment_date'])
    to_be_inserted_data.insert(1, 'source_id', source_id)
    if len(to_be_inserted_data):
        to_be_inserted_data = get_formatted_columns(to_be_inserted_data, ['seller_type', 'seller_name', 'payment_date'])
        insertion_data_list = get_formatted_list(
            ["({data})".format(data=get_formatted_list(row)) for row in to_be_inserted_data.values.tolist()])
        sql_result(constants.LDP_INSERTION_QUERY.format(data=insertion_data_list))


# Per listing addition
def new_listings_addition(proc_sheet, city_id, inventory_type=2):
    add_log("NEW LISTINGS ADDITION STARTED FOR CITY_ID " + str(city_id) + ' AND INVENTORY TYPE ' + str(inventory_type))
    proc_sheet = proc_sheet.copy()
    default_gst_rate = float(proc_sheet.iloc[0, 41]) * 100
    proc_sheet = proc_sheet.iloc[2:, :]
    proc_sheet[2] = proc_sheet[2].fillna('').map(lambda x: x.strip())
    column_names = ['listing_ids', 'msp', 'tsp', 'gst', 'estimated_refurb_cost',
                    'procurement_price', 'esp', 'actual_refurb_cost', 'payment_date', 'seller_type', 'seller_name']
    if inventory_type is 2:
        payment_date_index = 10
        column_list = [2, 26, 25, 43, 19, 23, 49, 20, 10, 5, 4]
    else:
        payment_date_index = 15
        column_list = [2, 26, 25, 43, 20, 19, 49, 20, 15, 5, 4]
    latest_listing_id = sql_result(constants.LATEST_LISTING_ID_QUERY.format(city_id=city_id,
                                                                            inventory_type=inventory_type)).iloc[0, 0]
    if (not latest_listing_id) or (str(latest_listing_id) not in proc_sheet[2]):
        latest_listing_id = proc_sheet.iloc[2, 2]
    listing_payment_date = pd.to_datetime(proc_sheet[proc_sheet[2] == str(latest_listing_id)]
                                          [payment_date_index].values[0])
    active_listings = proc_sheet[(proc_sheet[29].str.upper().isin(ACTIVE_STATUS_LIST))]
    active_listings[43].replace('#N/A', default_gst_rate, inplace=True)
    active_listings = active_listings[(pd.to_datetime(active_listings[10]) >= listing_payment_date) &
                                      (active_listings[2] != '')][column_list]
    active_listings.columns = column_names
    newly_added_listings = active_listings.replace('', 'NULL')
    for col in ['seller_type', 'seller_name']:
        newly_added_listings[col] = newly_added_listings[col].str.upper()
    convert_column_data_types(newly_added_listings, column_names[:-3], 'int')
    if len(newly_added_listings):
        insert_new_data_in_ldp(newly_added_listings)

    add_log("NEW LISTINGS ADDITION COMPLETED FOR CITY_ID " + str(city_id) + str(inventory_type))


# Gets the ratio of projected stock performance(for 15 days) by benchmark stock performance
def get_stock_performance(token_query, live_cars_query, benchmark_stock_performance, days_slab=None,
                          cluster_flag=False, data_logging=False):
    current_stock_performance = {}
    if data_logging:
        current_stock_performance = {'7_days_sp': {}, '14_days_sp': {}, '7_14_days_sp': {}}

    days_in_month = calendar.monthrange(TODAY.year, TODAY.month)[1]
    if not days_slab:
        days_slab = """DATEDIFF(NOW(), Date(first_active_date))+1"""  # Condition to get all cars irrespective of age

    for city_id in [1, 2, 5]:
        city_wise_day_performance = []
        benchmark_data, cluster_sub_query = get_city_benchmark_sp(cluster_flag, benchmark_stock_performance, city_id)

        for lower_limit_days in [7, 14]:
            lower_limit, upper_limit = get_days_range(lower_limit_days, 1)

            tokens = sql_result(token_query.format(city_id=city_id, lower_limit=lower_limit, upper_limit=upper_limit,
                                                   days_slab=days_slab, cluster_sub_query=cluster_sub_query))
            cars_live = sql_result(live_cars_query.format(city_id=city_id, lower_limit=lower_limit,
                                                          upper_limit=upper_limit, days_slab=days_slab,
                                                          cluster_sub_query=cluster_sub_query))

            cars_sold = tokens.iloc[:, 1:].sum() * (days_in_month / lower_limit_days)
            avg_cars = cars_live.iloc[:, 1:].mean()
            stock_performance = cars_sold.divide(avg_cars.values).replace([np.inf, np.nan], [0, 0]).round(2)

            city_stock_performance = stock_performance.divide(benchmark_data)
            city_wise_day_performance.append(city_stock_performance.values)

        days_min_data = dict(zip(city_stock_performance.keys(), np.amin(city_wise_day_performance, axis=0)))
        if data_logging:
            seven_day_sp = dict(zip(city_stock_performance.keys(), city_wise_day_performance[0]))
            fourteen_day_sp = dict(zip(city_stock_performance.keys(), city_wise_day_performance[1]))
            current_stock_performance['7_days_sp'][city_id] = seven_day_sp
            current_stock_performance['14_days_sp'][city_id] = fourteen_day_sp
            current_stock_performance['7_14_days_sp'][city_id] = days_min_data
        else:
            current_stock_performance[city_id] = days_min_data if cluster_flag else days_min_data['cars_sold']

    return current_stock_performance


def get_price_change_log_id(listing_id, source_id, prev_lsp, curr_lsp, prev_msp, curr_msp, prev_tsp, curr_tsp):
    source_name = DPConstants.SOURCE_ID_TO_NAME_DICT[source_id]
    listing_price_change_id = sql_result_insert(
        constants.LSP_INSERT_QUERY.format(listing_id=listing_id, source_name=source_name,
                                          previous_price=prev_lsp, updated_price=curr_lsp))

    minimum_selling_price_change_log_id = sql_result_insert(
        constants.LOG_INSERT_QUERY.format(table_name='minimum_selling_price_logs', listing_id=listing_id,
                                          source_id=source_id, previous_price=prev_msp, updated_price=curr_msp))

    target_selling_price_change_log_id = sql_result_insert(
        constants.LOG_INSERT_QUERY.format(table_name='target_selling_price_logs', listing_id=listing_id,
                                          source_id=source_id, previous_price=prev_tsp, updated_price=curr_tsp))

    return listing_price_change_id, minimum_selling_price_change_log_id, target_selling_price_change_log_id


def price_changes_update(changed_data, city_id):
    add_log("PRICE_CHANGES_UPDATE FUNCTION CALLED")
    changed_data = dataframe_type_conversion(changed_data)
    changed_data = changed_data.loc[~changed_data.isna().any(axis=1)]
    changed_data[changed_data.columns] = changed_data[changed_data.columns].astype('int')

    for i in range(changed_data.shape[0]):
        try:
            changed_values_list = list(changed_data.iloc[i].values)
            listing_id = changed_values_list[0]
            msp = changed_values_list[1]
            tsp = changed_values_list[2]
            gst_rate = changed_values_list[4]
            refurb_cost = changed_values_list[5]
            proc_price = changed_values_list[6]
            lsp = changed_values_list[3]
            prev_msp = changed_values_list[7]
            prev_tsp = changed_values_list[8]
            prev_lsp = changed_values_list[9]
            source_id = changed_values_list[10]
            if city_id == 5: # For Bangalore, LSP = TSP = MSP
                lsp = msp
                tsp = msp
            # Existing entry check
            existing_entry_check = sql_result(
                """ SELECT * FROM listing_dynamic_pricings WHERE listing_id = {listing_id} """.format(
                    listing_id=listing_id))

            # listings table update
            sql_result(constants.UPDATE_LISTINGS_PRICE_QUERY.format(lsp=lsp, listing_id=listing_id))

            if existing_entry_check.empty:
                sql_result(
                    constants.LISTING_DYNAMIC_PRICING_INSERT_QUERY.format(listing_id=listing_id, source_id=source_id,
                                                                          msp=msp, tsp=tsp, gst_rate=gst_rate,
                                                                          refurb_cost=refurb_cost, proc_price=proc_price))
            else:
                # ldp update
                sql_result(
                    constants.UPDATE_LISTING_DYNAMIC_PRICINGS_QUERY.format(listing_id=listing_id, source_id=source_id,
                                                                           msp=msp, tsp=tsp, gst=gst_rate,
                                                                           refurb_cost=refurb_cost, proc_price=proc_price))

            previous_lsp_updated_price = sql_result(
                constants.PREVIOUS_PRICE_LOG_QUERY.format(listing_id=listing_id,
                                                          table_name='listing_price_changes'))
            previous_msp_updated_price = sql_result(
                constants.PREVIOUS_PRICE_LOG_QUERY.format(listing_id=listing_id,
                                                          table_name='minimum_selling_price_logs'))
            previous_tsp_updated_price = sql_result(
                constants.PREVIOUS_PRICE_LOG_QUERY.format(listing_id=listing_id,
                                                          table_name='target_selling_price_logs'))
            if previous_lsp_updated_price.empty and previous_msp_updated_price.empty and previous_tsp_updated_price.empty:

                listing_price_change_id, minimum_selling_price_change_log_id, target_selling_price_change_log_id = get_price_change_log_id(
                    listing_id, source_id, prev_lsp, lsp, prev_msp, msp, prev_tsp, tsp)

            else:
                previous_lsp_updated_price = prev_lsp if previous_lsp_updated_price.empty else previous_lsp_updated_price.iloc[0, 0]
                previous_msp_updated_price = prev_msp if previous_msp_updated_price.empty else previous_msp_updated_price.iloc[0, 0]
                previous_tsp_updated_price = prev_tsp if previous_tsp_updated_price.empty else previous_tsp_updated_price.iloc[0, 0]
                listing_price_change_id, minimum_selling_price_change_log_id, target_selling_price_change_log_id = get_price_change_log_id(
                    listing_id, source_id, previous_lsp_updated_price, lsp, previous_msp_updated_price, msp,
                    previous_tsp_updated_price, tsp)

            if changed_data.shape[1] == 12:
                q_table_id = int(changed_values_list[11])
                if q_table_id != 0:
                    insert_z_score_state_logs_query = """INSERT INTO listing_z_score_state_logs 
                    (q_table_id, listing_price_change_id, minimum_selling_price_log_id, target_selling_price_log_id) 
                    VALUES ({q_table_id}, {listing_price_change_id}, {msp_log_id}, {tsp_log_id})"""
                    if None not in [listing_price_change_id, minimum_selling_price_change_log_id,
                                    target_selling_price_change_log_id]:
                        sql_result(insert_z_score_state_logs_query
                                   .format(q_table_id=q_table_id, listing_price_change_id=listing_price_change_id,
                                           msp_log_id=minimum_selling_price_change_log_id,
                                           tsp_log_id=target_selling_price_change_log_id))

        except Exception as e:
            add_log(str(e), True)
            continue

    add_log("PRICE_CHANGES_UPDATE FUNCTION COMPLETED")


def psychological_price(lsp, msp, esp):
    value = max(float(float(msp) + 0.07 * float(esp)), float(lsp))
    flt_value = float(round(value / 100000, 2))
    int_value = math.floor(flt_value)
    dec_value = round(flt_value - int_value, 2)
    val = int(flt_value * 100)
    reduc_fac = 0.01 * (int_value + 1)
    if val % 10 == 0:
        new_value = flt_value - 0.02
    elif (2 <= int_value <= 5 and dec_value <= reduc_fac) or (int_value >= 6 and dec_value <= 0.10):
        new_value = int_value - 0.02
    elif (2 <= int_value <= 5 and 0.50 <= dec_value <= 0.50 + reduc_fac) or (
            int_value >= 6 and 0.50 <= dec_value <= 0.56):
        new_value = int_value + 0.48
    elif val % 5 == 0:
        new_value = (val - 1) / 100
    else:
        new_value = flt_value

    new_lsp = round(new_value, 2)
    new_lsp = new_lsp * 100000

    if new_lsp < lsp:
        return new_lsp
    else:
        return lsp


def get_tsp_wrt_esp(tsp, city_id, msp, esp, input_params=np.array([]), initial_check=False):
    tsp_city_ids = np.array(list(TSP_PRICE_DATA["city_id"].keys())).astype('int')

    if city_id in tsp_city_ids:
        price_data = TSP_PRICE_DATA["city_id"][str(city_id)]
        price_slab = np.array(list(price_data.keys())).astype('int')
        rounded_value = int(esp) / 100000
        floored_value = np.floor(rounded_value)
        esp_floored_value = int(floored_value * 100000)

        if esp_floored_value not in price_slab:
            if esp_floored_value < min(price_slab):
                slab = min(price_slab)
            else:
                while esp_floored_value not in price_slab:
                    esp_floored_value -= 100000
                slab = esp_floored_value
        else:
            if (rounded_value - floored_value) == 0.0 and esp_floored_value != min(price_slab):
                slab_index = list(price_slab).index(esp_floored_value) - 1
                esp_floored_value = price_slab[slab_index]

            slab = esp_floored_value
        tsp = int(msp) + int(price_data[str(slab)])

    else:
        if len(input_params):
            if initial_check:
                initial_margin, neg_margin = input_params
                tsp = float(round((initial_margin + np.ceil(neg_margin * 100 / 2) / 100) * esp, -3))
            else:
                lsp = input_params
                lsp = float(lsp)
                if lsp <= (int(tsp) + 0.02 * esp):
                    tsp = (int(msp) + lsp) / 2
                    tsp = (int(round(tsp) / 1000)) * 1000

    return tsp


def get_lower_bound_msp(days_since_active, esp, cp, inventory_type, dealer_neg_margin=2.5, msp_data=None):
    lower_bound_msp_amount = int(1.02 * int(cp))
    if inventory_type is 2:
        esp = int(esp)
        if (days_since_active <= 35 and esp < 600000) or (days_since_active <= 42 and esp >= 600000):
            lower_limit = 1.02
        elif (days_since_active <= 49 and esp < 600000) or (days_since_active <= 56 and esp >= 600000):
            lower_limit = 0.97
        else:
            lower_limit = 0.9
        lower_bound_msp_amount = int(lower_limit * int(cp))

    if inventory_type is 1:
        dnm = min(dealer_neg_margin * 0.01 * cp, 30000)
        mmsp = cp - dnm
        lower_bound_msp_amount = mmsp

    if msp_data:
        msp, city_id = msp_data
        max_action = DPConstants.CITY_WISE_MAX_ACTION_DICT[city_id]
        msp_abs_reduction_bound = int(msp) - max_action * 0.01 * 1100000
        lower_bound_msp_amount = max(lower_bound_msp_amount, msp_abs_reduction_bound)

    return lower_bound_msp_amount


def get_dealer_neg_margin(sheet_key):
    sheet_name = config_data["sheet_keys"]["park_n_sell"]["dealers"]
    dealer_margin_df = pd.DataFrame(read_sheet(sheet_key, sheet_name + "!B3:G"))
    dealer_margin_df.drop(dealer_margin_df.columns[1:5], axis=1, inplace=True)
    dealer_margin_df.dropna(inplace=True)
    dealer_margin_df.rename(columns={0: 'dealer', 5: 'margin'}, inplace=True)
    dealer_margin_df['margin'] = pd.to_numeric(dealer_margin_df['margin'])
    dealer_neg_margin_dict = dict(zip(dealer_margin_df.dealer, dealer_margin_df.margin))
    return dealer_neg_margin_dict


def get_action(listing_state, days_since_active, msp, cp, city_id, esp, inventory_type=2, dealer_neg_margin=2.5):

    # finding the max penalty
    action_df = sql_result(constants.EXTRACT_MAXIMUM_PENALTY.format(city=city_id, listing_state=listing_state))
    source_type = 'dynamic_pricing'
    # Add logic for 7/8 days and lower limit check to get the max action
    if inventory_type == 2:
        if 12 <= days_since_active <= 18 and int(action_df.iloc[0, 0]) > 1 and \
                STOCK_PERFORMANCE_DICT_21_DAYS[city_id] >= 1 and listing_state >= -0.99:
            action_df = sql_result(
                constants.EXTRACT_SECOND_BEST_PENALTY.format(city=city_id, listing_state=listing_state,
                                                             penalty=action_df.iloc[0, 0]))
            source_type = 'dp_second_best'

    # extracting the max q_value for the current state
    action = int(action_df.iloc[0, 0])
    q_table_id = int(action_df.iloc[0, 1])
    new_msp = (100 - action) * int(msp) / 100

    lower_bound_msp_amount = get_lower_bound_msp(days_since_active, esp, cp, inventory_type, dealer_neg_margin,
                                                 msp_data=[msp, city_id])
    boolean_check = new_msp < lower_bound_msp_amount

    if boolean_check:
        action = int(100 * (1 - (lower_bound_msp_amount / int(msp))))
        source_type = 'dp_limited'
        days_limit = np.array([[35, 49], [42, 56]])

        if inventory_type == 2:
            if days_since_active < days_limit[0][1] and esp < 600000:
                days_upper_limit = days_limit[0][days_limit[0] > days_since_active].min()
            elif days_since_active < days_limit[1][1] and esp >= 600000:
                days_upper_limit = days_limit[1][days_limit[1] > days_since_active].min()
            else:
                return action, q_table_id, source_type
            weeks_left = math.ceil((days_upper_limit - days_since_active) / 7)
            action = math.ceil(action / weeks_left)

    return action, q_table_id, source_type


def update_q_table(q_table_id, current_state):
    alpha = float(0.01)
    gamma = float(0.05)
    if current_state == 0.0:
        current_state = int(current_state)

    # sql query to extract action, prev state ,city id and old_q_value
    extract_action_state_and_city_id_old_q_value = """  SELECT t1.action,t1.z_state,t1.city_id,t1.q_value
                                                 FROM q_tables t1
                                                 WHERE t1.id = %s """

    result_df = sql_result(extract_action_state_and_city_id_old_q_value % q_table_id)
    action = int(result_df.iloc[0, 0])
    prev_state = float(result_df.iloc[0, 1])
    city_id = int(result_df.iloc[0, 2])
    old_q_value = float(result_df.iloc[0, 3])

    reward_data = get_reward_data()

    # extracting the max q_value for the current state
    if current_state != 0:
        extract_current_state_max_q_value = """SELECT t1.q_value FROM q_tables t1 WHERE t1.city_id = %s and 
        cast(t1.z_state as decimal(3,2)) = cast(%s as decimal(3,2)) and t1.q_value = (SELECT MAX(t2.q_value) 
        FROM q_tables t2 WHERE t2.city_id = t1.city_id and t1.z_state = t2.z_state and t2.q_value <> 0)"""

        result_df1 = sql_result(extract_current_state_max_q_value % (city_id, current_state))
        current_state_max_q_value = float(result_df1.iloc[0, 0])
    else:
        current_state_max_q_value = 0.0

    reward = int(reward_data[str(prev_state)][str(current_state)])
    reward = reward - 50 * np.exp(0.05 * action)

    new_value = (1 - alpha) * old_q_value + alpha * (reward + gamma * current_state_max_q_value)

    # update the q_value in q_tables
    update_q_value = """UPDATE q_tables SET q_value = %s WHERE id = %s"""
    sql_result(update_q_value % (new_value, q_table_id))

    # insert into q_table_logs
    insert_q_table_logs_query = """ INSERT INTO q_table_logs (q_table_id, previous_q_value, updated_q_value) 
                                    VALUES (%s, %s, %s)"""
    sql_result(insert_q_table_logs_query % (q_table_id, old_q_value, new_value))


def final_data_preparation(final, listing_ids, city_id):
    final = dataframe_type_conversion(final)
    final = final.fillna('')
    changed_columns = ['id', 'msp', 'tsp', 'lsp', 'gst_rate', 'refurb_cost', 'procurement_price', 'prev_msp',
                       'prev_tsp', 'prev_lsp', 'source_change']
    if 'q_table_id' in final.columns:
        changed_columns.append('q_table_id')

    changed_data = final[final['id'].isin(listing_ids)][changed_columns]
    price_changes_update(changed_data, city_id)


def listing_wise_table_update(listing_id, listing_state):
    q_table_df = sql_result(constants.EXTRACT_Q_TABLE_QUERY.format(id=listing_id))

    if not q_table_df.empty:
        prev_q_table_id = int(q_table_df.iloc[0, 0])
        update_q_table(prev_q_table_id, listing_state)


def get_sheet_columns(park_n_sell_sheet, last_row):
    procurement_sheet_data = park_n_sell_sheet.iloc[2:last_row, :].copy()
    sheet_columns = [pd.Series(col) for col in procurement_sheet_data.T.values[[2, 26, 30, 19, 25, 29, 49, 14, 43]]]
    new_gst = float(park_n_sell_sheet.iloc[0, 41]) * 100

    return sheet_columns, new_gst, procurement_sheet_data


def get_esp(procurement_sheet_data, i):
    if procurement_sheet_data.iloc[i, 49] != '':
        esp = int(procurement_sheet_data.iloc[i, 49])
    else:
        esp = round(int(procurement_sheet_data.iloc[i, 24]) * 1.15, -3)

    return esp


def get_revised_prices(red_fac, msp_data, tsp_data, lsp_data, esp, city_id):
    msp_data = round(red_fac * float(msp_data), -3)
    if city_id == 5:
        tsp_data = get_tsp_wrt_esp(int(tsp_data), city_id, msp_data, esp)
    else:
        tsp_data = round(red_fac * float(tsp_data), -3)
    lsp_data = round(red_fac * float(lsp_data), -3)
    lsp_data = float(round(psychological_price(lsp_data, msp_data, esp), -3))

    return msp_data, tsp_data, lsp_data


def set_lsp_change(lsp, esp, tsp, city_id, msp):
    lsp = round((lsp - 0.015 * esp), -3)
    lsp = float(round(psychological_price(lsp, msp, esp), -3))
    tsp = get_tsp_wrt_esp(tsp, city_id, msp, esp, input_params=np.array([lsp]))
    source_change = int(DPConstants.SOURCE_NAME_TO_ID_DICT['dp_lsp'])
    return lsp, tsp, source_change


def price_change_using_qtable(factor_sheet_data, city):
    add_log("PRICE_CHANGE_CALCULATION FUNCTION CALLED : " + city)
    sheet_key = DPConstants.CITY_PROC_SHEETS['cities'][city]
    procurement_sheet = pd.DataFrame(read_sheet(sheet_key, DPConstants.CITY_PROC_SHEETS['name']))

    procurement_sheet = remove_duplicate_entries(procurement_sheet)
    city_id = DPConstants.CITY_NAME_TO_ID_DICT[city]

    city_wise_active_count = sql_result(constants.DAYS_SINCE_ACTIVE_TD_QUERY.format(city_id=city_id))

    if factor_sheet_data.shape[0] > 0:
        listing_ids = []
        procurement_last_row = int(procurement_sheet.iloc[0, 39])
        procurement_sheet_data = procurement_sheet.iloc[2:procurement_last_row, :].copy()
        column_indices = [2, 23, 26, 25, 30, 20, 19, 49, 51]
        series_data_list = []
        for col in procurement_sheet_data.T.values[column_indices]:
            series_data_list.append(pd.Series(col))
        sheet_listing_ids, procurement_price_data, msp_data, tsp_data, lsp_data = series_data_list[:5]
        actual_rfc_data, estimated_rfc_data, esp_data, range_data = series_data_list[5:]
        new_gst = float(procurement_sheet.iloc[0, 41]) * 100
        last_column = len(procurement_sheet_data.columns)
        procurement_sheet_data.insert(last_column, last_column, 0)
        q_table_id_data = procurement_sheet_data.iloc[:, last_column]
        source_change = pd.Series().reindex_like(procurement_sheet_data.iloc[:, 30]).fillna('')

        for fs in range(int(factor_sheet_data.shape[0])):

            listing_id, listing_raw_state = int(factor_sheet_data.iloc[fs, 0]), float(factor_sheet_data.iloc[fs, 1])

            listing_state = sql_result(constants.EXTRACT_STATE_FROM_RAW_STATE.format(z_score=listing_raw_state)).iloc[
                0, 0]

            # check for prev_price change and update q_table update_q_table(listing_id, listing_state)
            # extract the  qtable_id from listing_z_score_state_logs for a given listing id if it exists. then
            # from that listing id extract state and action from the q_table.
            try:
                i = pd.Index(sheet_listing_ids).get_loc(str(listing_id))
            except KeyError:
                continue

            listing_wise_table_update(listing_id, listing_state)
            proc_price = int(float(procurement_price_data.iloc[i]))

            try:
                cost_price = int(float(actual_rfc_data.iloc[i]) + proc_price)
            except ValueError:
                cost_price = int(float('0' + actual_rfc_data.iloc[i]) + proc_price)

            if procurement_sheet_data.iloc[i, 49] != '':
                esp = int(procurement_sheet_data.iloc[i, 49])
            else:
                esp = round(cost_price * 1.15, -3)

            status_data = (procurement_sheet_data.iloc[i, 29].upper() in ACTIVE_STATUS_LIST)
            if ((procurement_sheet_data.iloc[i, 26] != '') and (procurement_sheet_data.iloc[i, 49] != '') and
                    (procurement_sheet_data.iloc[i, 19] != '') and (procurement_sheet_data.iloc[i, 14] != '') and
                    status_data and (procurement_sheet_data.iloc[i, 3] != 'Park and Sell')):

                if not ((procurement_sheet_data.iloc[i, 43] != "#N/A") and (procurement_sheet_data.iloc[i, 43] != '')):
                    procurement_sheet_data.iloc[i, 43] = new_gst

                try:
                    days_since_active = int(city_wise_active_count[
                        city_wise_active_count['listing_id'] == listing_id][['days_since_active']].iloc[0, 0])
                except IndexError:
                    continue

                neg_margin = get_neg_margin(proc_price, city_id)

                try:
                    cost_price = int(float(actual_rfc_data.iloc[i]) + proc_price)
                except ValueError:
                    cost_price = int('0' + estimated_rfc_data.iloc[i]) + proc_price

                if listing_state == 0:
                    if days_since_active > 22 and float(lsp_data.iloc[i]) >= (float(msp_data.iloc[i]) + (
                            neg_margin - 0.02) * esp) and city_id != 2:
                        lsp_data.iloc[i], tsp_data.iloc[i], source_change.iloc[i] = set_lsp_change(
                            float(lsp_data.iloc[i]), esp, int(tsp_data.iloc[i]), city_id, msp_data.iloc[i])

                    elif days_since_active >= 35 and math.ceil(
                            ((float(lsp_data.iloc[i]) - float(msp_data.iloc[i])) / esp) * 100) < neg_margin * 100:
                        new_msp = float(msp_data.iloc[i]) - 0.015 * esp
                        lower_bound_msp = get_lower_bound_msp(days_since_active, esp, cost_price, inventory_type=2,
                                                              msp_data=[msp_data.iloc[i], city_id])
                        if new_msp >= lower_bound_msp and float(msp_data.iloc[i]) > round(lower_bound_msp, -3):
                            msp_data.iloc[i] = round(new_msp, -3)
                            tsp_data.iloc[i] = get_tsp_wrt_esp(int(tsp_data.iloc[i]), city_id, msp_data.iloc[i], esp,
                                                               input_params=np.array([lsp_data.iloc[i]]))
                            source_change.iloc[i] = int(DPConstants.SOURCE_NAME_TO_ID_DICT['dp_lsp'])
                        else:
                            source_change.iloc[i] = int(DPConstants.SOURCE_NAME_TO_ID_DICT['dp_limited'])
                            listing_ids.append(listing_id)
                            continue
                    else:
                        source_change.iloc[i] = int(DPConstants.SOURCE_NAME_TO_ID_DICT['dp_positive'])
                        listing_ids.append(listing_id)
                        continue

                range_data.iloc[i] = int(days_since_active)

                if listing_state != 0:
                    action, q_table_id, source_type = get_action(listing_state, days_since_active, msp_data.iloc[i],
                                                                 cost_price, city_id, esp=esp)
                    if action == 0:
                        if days_since_active > 22 and float(lsp_data.iloc[i]) >= (
                                float(msp_data.iloc[i]) + (neg_margin - 0.02) * esp) and city_id != 2:
                            lsp_data.iloc[i], tsp_data.iloc[i], source_change.iloc[i] = set_lsp_change(
                                float(lsp_data.iloc[i]), esp, int(tsp_data.iloc[i]), city_id, msp_data.iloc[i])
                        else:
                            source_change.iloc[i] = int(DPConstants.SOURCE_NAME_TO_ID_DICT[source_type])
                            listing_ids.append(listing_id)
                            continue
                    q_table_id_data.iloc[i] = int(q_table_id)
                    red_fac = float(1 - action / 100)
                    msp_data.iloc[i], tsp_data.iloc[i], lsp_data.iloc[i] = \
                        get_revised_prices(red_fac, msp_data.iloc[i], tsp_data.iloc[i], lsp_data.iloc[i], esp, city_id)
                    source_change.iloc[i] = int(DPConstants.SOURCE_NAME_TO_ID_DICT[source_type])

                listing_ids.append(listing_id)

        column_names = DPConstants.COLUMN_DATA_DICTIONARY

        data_frame_values = [procurement_sheet_data.iloc[:, 2], msp_data, tsp_data, lsp_data, range_data,
                             procurement_sheet_data.iloc[:, 43], procurement_sheet_data.iloc[:, 19],
                             procurement_sheet_data.iloc[:, 23], procurement_sheet.iloc[2:procurement_last_row, 26],
                             procurement_sheet.iloc[2:procurement_last_row, 25],
                             procurement_sheet.iloc[2:procurement_last_row, 30], source_change, q_table_id_data]
        data_frame_values = [column.values.tolist() for column in data_frame_values]
        final = pd.DataFrame(dict(zip(column_names, data_frame_values)))

        final_data_preparation(final, listing_ids, city_id)

    add_log("PRICE_CHANGE_CALCULATION FUNCTION COMPLETED : " + city)


def price_change_of_park_sell(factor_sheet_data, city):
    sheet_key = DPConstants.PARK_SELL_DICT[city]
    add_log("PRICE_CHANGE_CALCULATION PARK AND SELL FUNCTION STARTED : " + city)

    park_n_sell_sheet = pd.DataFrame(read_sheet(sheet_key, DPConstants.CITY_PROC_SHEETS['name']))
    park_n_sell_sheet = remove_duplicate_entries(park_n_sell_sheet)

    dealer_wise_neg_margin_dict = get_dealer_neg_margin(sheet_key)

    city_id = DPConstants.CITY_NAME_TO_ID_DICT[city]

    city_wise_active_count = sql_result(constants.DAYS_SINCE_ACTIVE_TD_QUERY.format(city_id=city_id))

    if factor_sheet_data.shape[0] > 0:
        last_row = int(park_n_sell_sheet.iloc[0, 39])
        park_sell_sheet_data = park_n_sell_sheet.iloc[2:last_row, :].copy()
        dealer_name = park_sell_sheet_data.iloc[:, 4]
        column_list, new_gst, procurement_sheet_data = get_sheet_columns(park_n_sell_sheet, last_row)
        sheet_listing_ids, msp_data, lsp_data, cp_data, tsp_data, current_status, esp_data, car_live_date, gst_data = \
            column_list

        listing_ids = []
        last_column = len(procurement_sheet_data.columns)
        procurement_sheet_data.insert(last_column, last_column, 0)
        q_table_id_data = procurement_sheet_data.iloc[:, last_column]
        source_change = pd.Series().reindex_like(procurement_sheet_data.iloc[:, 30]).fillna('')

        for fs in range(int(factor_sheet_data.shape[0])):
            listing_id, listing_raw_state = int(factor_sheet_data.iloc[fs, 0]), float(factor_sheet_data.iloc[fs, 1])

            listing_state = sql_result(constants.EXTRACT_STATE_FROM_RAW_STATE.format(z_score=listing_raw_state)).iloc[
                0, 0]

            try:
                i = pd.Index(sheet_listing_ids).get_loc(str(listing_id))
            except KeyError:
                continue

            if listing_state == 0:
                source_change.iloc[i] = int(DPConstants.SOURCE_NAME_TO_ID_DICT['dp_positive'])
                listing_ids.append(listing_id)
                continue

            listing_wise_table_update(listing_id, listing_state)

            try:
                days_since_active = int(city_wise_active_count[
                                            city_wise_active_count['listing_id'] == listing_id][
                                            ['days_since_active']].iloc[0, 0])
            except IndexError:
                continue

            if current_status.iloc[i] not in 'Live':
                continue

            if gst_data.iloc[i] in ("#N/A", ""):
                gst_data.iloc[i] = new_gst

            try:
                cp_data.iloc[i] = int(cp_data.iloc[i])
            except (ValueError, TypeError):
                cp_data.iloc[i] = 0
            try:
                esp = int(esp_data.iloc[i])
            except (ValueError, TypeError):
                esp = cp_data.iloc[i]

            if esp == 0:
                continue

            dealer_neg_margin = dealer_wise_neg_margin_dict.get(dealer_name.iloc[i], 2.5)

            action, q_table_id, source_type = get_action(listing_state, days_since_active, msp_data.iloc[i],
                                                         cp_data.iloc[i], city_id, esp=esp, inventory_type=1,
                                                         dealer_neg_margin=dealer_neg_margin)

            if action <= 0:
                source_change.iloc[i] = int(DPConstants.SOURCE_NAME_TO_ID_DICT[source_type])
                listing_ids.append(listing_id)
                continue
            q_table_id_data.iloc[i] = int(q_table_id)
            red_fac = float(1 - action / 100)
            lsp = round(red_fac * float(lsp_data.iloc[i]), -3)
            lsp = float(round(psychological_price(float(lsp), float(msp_data.iloc[i]), esp), -3))
            if lsp < cp_data.iloc[i]:
                red_fac = float(cp_data.iloc[i]) / float(lsp_data.iloc[i])

            msp_data.iloc[i], tsp_data.iloc[i], lsp_data.iloc[i] = \
                get_revised_prices(red_fac, msp_data.iloc[i], tsp_data.iloc[i], lsp_data.iloc[i], esp, city_id)
            source_change.iloc[i] = int(DPConstants.SOURCE_NAME_TO_ID_DICT[source_type])

            listing_ids.append(listing_id)

        column_names = filter(lambda x: x not in ['range', 'q_table_id'], DPConstants.COLUMN_DATA_DICTIONARY)

        data_frame_values = [procurement_sheet_data.iloc[:, 2], msp_data, tsp_data, lsp_data, gst_data,
                             procurement_sheet_data.iloc[:, 20].replace('', 0), cp_data,
                             park_n_sell_sheet.iloc[2:last_row, 26], park_n_sell_sheet.iloc[2:last_row, 25],
                             park_n_sell_sheet.iloc[2:last_row, 30], source_change, q_table_id_data]

        data_frame_values = [column.values.tolist() for column in data_frame_values]
        final = pd.DataFrame(dict(zip(column_names, data_frame_values)))
        final_data_preparation(final, listing_ids, city_id)
    add_log("PRICE_CHANGE_CALCULATION PARK AND SELL FUNCTION COMPLETED : " + city)


def get_neg_margin(proc_price, city_id):
    """
    Fetches city wise negotiation margin from global_constants and calculates margin on basis of proc price slab range
    """
    neg_margin_dict = DPConstants.CITY_WISE_NEG_MARGIN[str(city_id)]
    margin_slab = pd.cut([proc_price], [0, 200000, 500000, float("inf")], include_lowest=True)[0]
    margin = neg_margin_dict.get(str(margin_slab), neg_margin_dict['(0.0, 200000.0]'])

    return margin


def summarize_set(city_sheet_name):
    add_log("SUMMARIZE SET FUNCTION CALLED : " + city_sheet_name)
    city_name = city_sheet_name.split(' ')[0]

    try:
        city_result_sheet_qtable = pd.DataFrame(read_sheet(FACTOR_SHEET_DATA['key'], city_sheet_name + "!A7:C"))
        if city_result_sheet_qtable.empty:
            return
        truebil_direct = city_result_sheet_qtable[city_result_sheet_qtable[2] == '2']
        park_n_sell = city_result_sheet_qtable[city_result_sheet_qtable[2] == '1']

        if TODAY.weekday() not in (4, 5):
            price_change_using_qtable(truebil_direct, city_name)
            if city_name in list(PARK_SELL_DICT.keys()):
                price_change_of_park_sell(park_n_sell, city_name)

    except Exception as e:
        add_log(str(e), True)

    add_log("SUMMARIZE SET FUNCTION COMPLETED : " + city_sheet_name)


def check_latest_z_score(city_sheet_name):
    date = pd.DataFrame(read_sheet(DPConstants.FACTOR_SHEET_DATA['key'], city_sheet_name + "!B3")).iloc[0, 0]
    date = datetime.datetime.strptime(date, '%Y-%m-%d').date()
    if date != DPConstants.TODAY:
        exit(138)


def initialise_global_variables():
    global FACTOR_SHEET_DATA, PARK_SELL_DICT

    FACTOR_SHEET_DATA = config_data['sheet_keys']['factor_sheet']
    PARK_SELL_DICT = config_data['sheet_keys']['park_n_sell']['cities']


TODAY = datetime.date.today()
TSP_PRICE_DATA = json.loads(sql_result(constants.DP_PARAMS_QUERY).values.flatten()[0])["tsp_wrt_msp"]
CITY_PROC_SHEETS = config_data['sheet_keys']['procurement_sheet']
STOCK_PERFORMANCE_DICT_21_DAYS = get_stock_performance(constants.GET_TOKENS_QUERY, constants.GET_LIVE_CARS_QUERY,
                                                       {1: 1.3, 2: 1.3, 5: 1}, 22)
ACTIVE_STATUS_LIST = ['LIVE', 'SENT OUT FOR P&S', 'TO BE OFFLOADED', 'UNDER REFURBISHMENT']


if __name__ == "__main__":
    add_log("EXECUTION STARTED")
    sentry_sdk.init(SENTRY_URL)
    initialise_global_variables()
    # iterate over city ids
    for sheet_city_name in DPConstants.FACTOR_SHEET_DATA['cities']:
        check_latest_z_score(sheet_city_name)
        summarize_set(sheet_city_name)
    SheetUpdate("stock_performance_sheet").log_overall_stock_performance(STOCK_PERFORMANCE_DICT_21_DAYS)
    add_log("EXECUTION COMPLETED")
    send_log_to_slack(str(DPConstants.TODAY))