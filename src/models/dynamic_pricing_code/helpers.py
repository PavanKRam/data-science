import datetime
import json
import logging
import os
import subprocess
import time
import traceback

import MySQLdb
import pandas as pd
from google.oauth2 import service_account
from googleapiclient.discovery import build

from dynamic_pricing_code import slackapi

BASE_DIR = os.path.dirname(os.path.realpath(__file__))

LOG_DIR_PATH = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(BASE_DIR))), 'logs')
LOG_FILE_PATH = os.path.join(LOG_DIR_PATH, os.environ.get('LOG_FILENAME', 'data_science.log'))
configfile = open(os.path.join(BASE_DIR, 'config.json'), 'r')
config_data = json.loads(configfile.read())
ENABLE_SLACK_LOGGING = os.environ.get('SLACK_LOGGING', 'FALSE')
PROD_CREDENTIALS = config_data['mysql']['production-write']
CRM_CREDENTIALS = config_data['mysql']['crm']
SLACK_CONFIG = config_data['slack_bot']

# Constants
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
SERVICE_ACCOUNT_FILE = 'credentials.json'
REWARD_FILE_PATH = os.path.join(BASE_DIR, 'reward.json')


def send_log_to_slack(date):
    slack_api = slackapi.Slack()
    file = subprocess.check_output(['grep', date, LOG_FILE_PATH])
    channel_name = SLACK_CONFIG['channel_log']
    file_name = 'LOG of ' + LOG_FILE_PATH.split('/')[-1].split('.')[0] + ' ' + date
    params_error_log = {
                            'channels': channel_name,
                            'content': file,
                            'filename': file_name,
                            'filetype': 'python',
                            'title': file_name
    }

    slack_api.send('file', data_payload=params_error_log)


def create_slack_alert():
    """
    This method sends slack alerts on encountering errors.
    It uploads the error encountered as a file, containing the traceback of the error and it sends the error type as
    message.
    It also uploads the full log generated on that day, upon successful excecution of the sript.
    Both are sent via the slack api.
    Messages require additional headers, whereas file uploads don't require that.

    """
    slack_api = slackapi.Slack()
    today = str(datetime.datetime.utcnow())
    # the following statement give the full traceback stack to file variable
    file = traceback.format_exc()
    channel_name = SLACK_CONFIG['channel_error_log']
    error_name = file.splitlines()[-1]
    logging.error(error_name, exc_info=True)
    color = '#ff0000'
    file_name = 'Error ' + today
    params_message = {
                         "channel": channel_name,
                         "text": "<!channel>",
                         "icon_emoji": ":fearful:",
                         "attachments": [
                                            {
                                                "fallback": "Error notification for the Dynamic Pricing Cron Job!",
                                                "color": color,
                                                "title": "ERROR IN CRON JOB : " + os.environ.get('LOG_FILENAME').split
                                                ('.')[0],
                                                "fields": [
                                                        {
                                                            "title": error_name
                                                        }
                                                ]
                                            }
                          ]
    }
    slack_api.send('message', json_payload=params_message)

    params_error_log = {
                            'channels': channel_name,
                            'content': file,
                            'filename': file_name,
                            'filetype': 'python',
                            'title': file_name
    }
    slack_api.send('file', data_payload=params_error_log)


def get_spreadsheet_service():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    credential_dir = os.path.join(BASE_DIR, SERVICE_ACCOUNT_FILE)

    credentials = service_account.Credentials.from_service_account_file(
        credential_dir, scopes=SCOPES)

    service = build('sheets', 'v4', credentials=credentials)

    return service


def write_in_sheet(values, sid, range_name, duration=1):
    try:
        service = get_spreadsheet_service()

        spreadsheet_id = sid

        body = {
            'values': values
        }
        bodydel = {

        }
        range_name = range_name
        service.spreadsheets().values().clear(spreadsheetId=spreadsheet_id, range=range_name, body=bodydel).execute()
        service.spreadsheets().values().update(spreadsheetId=spreadsheet_id, range=range_name, valueInputOption="RAW",
                                               body=body).execute()
    except Exception as e:
        if duration < 40:
            time.sleep(duration)
            write_in_sheet(values, sid, range_name, duration * 2)
        else:
            raise e from None


def update_sheet(values, sid, range_name, duration=1):
    try:
        service = get_spreadsheet_service()

        spreadsheet_id = sid
        range_name = range_name
        old_values = read_sheet(sid, range_name)

        new_values = []
        for v in values:
            new_values.append(v)
        for o in old_values:
            new_values.append(o)
        body = {
            'values': new_values
        }
        bodydel = {

        }
        service.spreadsheets().values().clear(spreadsheetId=spreadsheet_id, range=range_name, body=bodydel).execute()
        service.spreadsheets().values().update(spreadsheetId=spreadsheet_id, range=range_name, valueInputOption="RAW",
                                               body=body).execute()
    except Exception as e:
        if duration < 40:
            time.sleep(duration)
            update_sheet(values, sid, range_name, duration * 2)
        else:
            raise e from None


def read_sheet(sid, range_name, duration=1):
    try:
        service = get_spreadsheet_service()

        spreadsheet_id = sid
        range_name = range_name
        result = service.spreadsheets().values().get(
            spreadsheetId=spreadsheet_id, range=range_name).execute()

        values = result.get('values', [])
    except Exception as e:
        if duration < 40:
            time.sleep(duration)
            return read_sheet(sid, range_name, duration * 2)
        else:
            raise e from None

    return values


# run a given query and return a Df
def sql_result(query):
    add_log(query)
    database = MySQLdb.connect(host=PROD_CREDENTIALS['host'], user=PROD_CREDENTIALS['user'],
                               passwd=PROD_CREDENTIALS['passwd'], db=PROD_CREDENTIALS['db'])
    cursor = database.cursor()
    cursor.execute(query)
    database.commit()
    query_result_list = [row for row in cursor.fetchall()]
    try:
        field_names = [i[0] for i in cursor.description]
        query_result_dataframe = pd.DataFrame(data=query_result_list, columns=field_names)
    except Exception:
        query_result_dataframe = None
    return query_result_dataframe


def sql_result_insert(query):
    add_log(query)
    database = MySQLdb.connect(host=PROD_CREDENTIALS['host'], user=PROD_CREDENTIALS['user'],
                               passwd=PROD_CREDENTIALS['passwd'], db=PROD_CREDENTIALS['db'])
    cursor = database.cursor()
    cursor.execute(query)
    database.commit()

    return cursor.lastrowid


def sql_result_crm(query):
    database = MySQLdb.connect(host=CRM_CREDENTIALS['host'], user=CRM_CREDENTIALS['user'],
                               passwd=CRM_CREDENTIALS['passwd'], db=CRM_CREDENTIALS['db'])
    cursor = database.cursor()
    cursor.execute(query)
    query_result_list = [row for row in cursor.fetchall()]
    field_names = [i[0] for i in cursor.description]
    query_result_dataframe = pd.DataFrame(data=query_result_list, columns=field_names)
    return query_result_dataframe


def add_log(line, is_error=False):
    file = open(LOG_FILE_PATH, 'a')
    log_line = str(datetime.datetime.utcnow()) + ' : '
    if is_error:
        if ENABLE_SLACK_LOGGING == 'TRUE':
            create_slack_alert()
        log_line += 'Error : '
    log_line += line + '\n'
    file.write(log_line)


def get_reward_data():
    with open(REWARD_FILE_PATH) as data_file:
        reward_data = json.load(data_file)
        return reward_data


def dataframe_type_conversion(df, data_type='float'):
    for col in df.columns:
        df[col] = pd.to_numeric(df[col], errors='coerce', downcast=data_type)
    return df


def get_source_name_from_source_table():
    source = sql_result("""SELECT * FROM sources""")
    source_name = list(source.iloc[:, 1].values)
    source_id = list(source.iloc[:, 0].values)
    source_name_to_id_dict = dict(zip(source_name, source_id))
    source_id_to_name_dict = dict(zip(source_id, source_name))
    return source_name_to_id_dict, source_id_to_name_dict


def get_city_data_mapping_dict():
    city_data = sql_result("""SELECT id,name FROM cities WHERE id IN (1,2,5)""")
    city_data.name = city_data.name.str.lower()
    city_id_to_name_dict = city_data.set_index('id').to_dict()['name']
    city_name_to_id_dict = city_data.set_index('name').to_dict()['id']

    return city_id_to_name_dict, city_name_to_id_dict


def get_sql_connection(db_configuration):
    connection = MySQLdb.connect(host=db_configuration['host'], user=db_configuration['user'],
                                 passwd=db_configuration['passwd'], db=db_configuration['db'])
    return connection


def get_cursor(connection):
    cursor = connection.cursor()
    return cursor


def sql_insert_execution(cursor, query):
    cursor.execute(query)


def convert_column_data_types(data_frame, column_names, data_type):
    if data_type == 'int':
        data_frame[column_names] = data_frame[column_names].apply(pd.to_numeric, errors='ignore', downcast='integer')
    elif data_type == 'datetime':
        data_frame[column_names] = pd.to_datetime(data_frame[column_names])
    else:
        data_frame[column_names] = data_frame[column_names].astype(data_type)


def get_formatted_list(to_convert_list):
    return ",".join(map(str, to_convert_list))


def get_formatted_columns(data_frame, columns):
    """
    Returns string column to formatted 'columns'
    """
    for col in columns:
        data_frame[col] = data_frame[col].apply('"{}"'.format)
    return data_frame
