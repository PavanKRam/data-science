import datetime

from dynamic_pricing_code.constants import GET_LIVE_CARS_QUERY, GET_CLUSTER_LEADS_QUERY, INITIAL_PRICE_CLUSTER_SUB_QUERY
from dynamic_pricing_code.helpers import sql_result


def get_days_range(lower_limit, upper_limit):
    """
    Get date range for specified day limits
    :param lower_limit: Starting day
    :param upper_limit: End day
    :return: date, date
    """
    lower_limit_date = datetime.date.today() - datetime.timedelta(days=lower_limit)
    upper_limit_date = datetime.date.today() - datetime.timedelta(days=upper_limit)

    return lower_limit_date, upper_limit_date


def get_cluster_lps_data(city_id):
    """
    Get 60 days cluster data for all listings irrespective of their age
    :param city_id: City Id
    :return: Dataframe of cluster stock, leads
    """

    # days_slab : Get data for whole age of listing
    days_slab, lower_limit_days = ["DATEDIFF(NOW(), Date(first_active_date))+1", 60]
    lower_limit_cluster, upper_limit_cluster = get_days_range(lower_limit_days, 1)

    cluster_live_stock = sql_result(GET_LIVE_CARS_QUERY.format(city_id=city_id, lower_limit=lower_limit_cluster,
                                                               upper_limit=upper_limit_cluster, days_slab=days_slab,
                                                               cluster_sub_query=INITIAL_PRICE_CLUSTER_SUB_QUERY
                                                               )).set_index('date').drop(['cars_live'], 1)

    cluster_wise_leads = sql_result(GET_CLUSTER_LEADS_QUERY.format(city_id=city_id, lower_limit=lower_limit_cluster,
                                                                   upper_limit=upper_limit_cluster, days_slab=days_slab,
                                                                   cluster_sub_query=INITIAL_PRICE_CLUSTER_SUB_QUERY
                                                                   )).set_index('date')

    return cluster_live_stock, cluster_wise_leads


def get_cluster_wise_z_scores():
    """
    Returns cluster wise z scores computed using lead per stock data for specific days ranges, city-wise
    """
    city_wise_z_score = {}

    for city_id in [1, 2, 5]:
        cluster_live_stock, cluster_wise_leads = get_cluster_lps_data(city_id)
        days_range_mean_data, days_range_std_data = [], []

        for lower_limit, upper_limit in zip([7, 60], [1, 8]):
            lps_lower_limit, lps_upper_limit = get_days_range(lower_limit, upper_limit)

            day_wise_leads = cluster_wise_leads[
                (cluster_wise_leads.index >= lps_lower_limit) & (cluster_wise_leads.index <= lps_upper_limit)]
            day_wise_live_stock = cluster_live_stock[
                (cluster_live_stock.index >= lps_lower_limit) & (cluster_live_stock.index <= lps_upper_limit)]

            day_wise_lps = day_wise_leads.divide(day_wise_live_stock.replace(0, 1)).astype('float')
            cluster_mean, cluster_std = day_wise_lps.mean().to_frame(), day_wise_lps.std().to_frame()
            days_range_mean_data.append(cluster_mean)
            days_range_std_data.append(cluster_std)

        # z_score = (7_day_lead_per_stock_mean - 7_60_day_lead_per_stock_mean) / 7_60_day_lead_per_stock_std
        # Cap z scores between [3, 5]
        city_wise_z_score[city_id] = ((days_range_mean_data[0] - days_range_mean_data[1]) /
                                      days_range_std_data[1]).apply(lambda series_score: [max(min(score, 5), -3)
                                                                                          for score in
                                                                                          series_score]).to_dict()[0]

    return city_wise_z_score


def get_city_benchmark_sp(cluster_flag, benchmark_stock_performance, city_id):
    """
    Gets Target stock performance city wise for every cluster
    :param cluster_flag: Cluster data check. If needed will return cluster data, otherwise whole
    :param benchmark_stock_performance: Benchmark stock performance data for every cluster from database
    :param city_id: City Id
    :return: dict of benchmark, cluster sub query string
    """
    cluster_sub_query = """"""
    if cluster_flag:
        cluster_sub_query = INITIAL_PRICE_CLUSTER_SUB_QUERY
        city_benchmark = benchmark_stock_performance[city_id].copy()
        # Multiplies overall benchmark to all cluster factors
        target_benchmark_multiplied = dict(
            (cluster_key, cluster_factor * city_benchmark['Overall']) for cluster_key, cluster_factor in
            city_benchmark['Cluster_SP_Factors'].items())
        benchmark_data = [city_benchmark['Overall']] + list(target_benchmark_multiplied.values())
    else:
        benchmark_data = benchmark_stock_performance[city_id]

    return benchmark_data, cluster_sub_query

