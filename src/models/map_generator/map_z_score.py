import datetime
import math
import os

import dateutil
import numpy as np
import pandas as pd
import sentry_sdk

os.environ['SLACK_LOGGING'] = 'TRUE'
os.environ['LOG_FILENAME'] = 'map_z_score.log'

from dynamic_pricing_code.helpers import sql_result, write_in_sheet, config_data, read_sheet, add_log
from dp_z_score_generator.constants import GET_LDP_ENTRY_QUERY, EXTRACT_STATE, UPDATE_LDP_MAP_QUERY, ACTIVE_CARS_QUERY, \
    MAP_LOG_INSERT_QUERY_FLOAT, CITY_NAME_QUERY, SOLD_DATA_QUERY, UPDATE_LDP_MAP_QUERY_NULL, PRICE_CHANGES_QUERY
from dp_z_score_generator.dynamic_pricing_score import get_demand_model, get_slab_check, get_seven_days_leads, \
    instantiate_multi_index_dataframe, get_sold_cars_active_dates, get_leads_per_latest_dates, get_price_changes, \
    set_cluster_data_dict, CLUSTER_DATA, get_z_scores, dataframe_check, get_car_quality_issues_listings
from dynamic_pricing_code.train_model import get_source_name_from_source_table, get_action
from dynamic_pricing_code.constants import DAYS_SINCE_ACTIVE_TD_QUERY


CITIES = [1, 2, 5]

"""
-This cron generates Z Scores of all the listings.
1. For listings with price changes older than seven days, the script uses the latest 7 days activity to generate the score.
2. For listings with price changes done less than seven days, the latest days activity is used to generate the score.
-The cron then calculates MAP of all listings, based on MSP and logic mentioned in the trello board.
-Listings with score -999 is changed to #NA and those with active days >= 50 have MAP changed from -9999 to "Business Call"
via the function get_map
"""


# gets vb_vd factors from the sheet
def get_vb_vd_factors():
    vb_vd_factors = pd.DataFrame(read_sheet(VB_VD_FACTOR_SHEET_KEY, "vb_vd_factors!F5:F31")).values[:, 0].astype(
        np.float)
    return vb_vd_factors * 1.05


# helper function to round down to nearest 5k
def round_down(n):
    n = round((n / 100000), 2)
    val = math.floor(n * 10)
    flt_val = n * 10
    dec_val = round((flt_val - val), 1)
    if dec_val < 0.5:
        result = val
    elif dec_val <= 0.9:
        result = val + 0.5
    result = result / 10
    result = round(((n - result) * 100000), 0)
    return result


def get_weekend_check(today, price_revision_in):

    if today in [0, 1, 2, 3, 4] and (today + price_revision_in) in [4, 5]:
        price_revision_in += 2 if today + price_revision_in == 4 else 1

    elif today in [5, 6] and (today + price_revision_in) % 7 in [4, 5]:
        price_revision_in = 1 if today == 5 else 0

    return price_revision_in


# finds map of all listings whose score was generated
def get_map(df, city_id):
    today_weekday = datetime.date.today().weekday()
    for i in df.index:
        # listing_id = df.iloc[i, 0]
        score = df.iloc[i, 1]
        inventory_type_id = df.iloc[i, 2]
        days_since_last_price = int(df.iloc[i, 3])
        msp = df.iloc[i, 4]
        cp = df.iloc[i, 5]
        esp = df.iloc[i, 6]
        state = df.iloc[i, 7]
        days_active = int(df.iloc[i, 8])  # Converts decimal into int
        # map = df.iloc[i, 9]
        # price_revision_in = df.iloc[i, 10]
        mod = days_since_last_price % 7
        price_revision_in = 0 if mod == 0 else 7 - mod

        price_revision_in = get_weekend_check(today_weekday, price_revision_in)

        if score == -999:
            df.iloc[i, 1] = '#NA'

        if days_active >= 50:
            df.iloc[i, 9] = 'Business Call'
            df.iloc[i, 10] = price_revision_in
        else:
            if price_revision_in > 3 and msp != '':
                day = datetime.datetime.today().weekday()
                if (day == 6 or day == 5) and state < 0:
                    df.iloc[i, 9] = round((msp - max(0.01 * msp, round_down(msp))), -3)
                    df.iloc[i, 10] = price_revision_in
                else:
                    df.iloc[i, 9] = round((msp - min(0.01 * msp, round_down(msp))), -3)
                    df.iloc[i, 10] = price_revision_in
            elif msp != '':
                if price_revision_in == 3:
                    k = 0.5
                elif price_revision_in == 2:
                    k = 0.7
                elif price_revision_in == 1:
                    k = 0.9
                else:
                    k = 1
                if state == 0:
                    df.iloc[i, 9] = round((msp - min(0.01 * msp, round_down(msp))), -3)
                    df.iloc[i, 10] = price_revision_in
                    continue
                action, q_table_id, source_type = get_action(state, days_active, msp, cp, city_id, esp,
                                                             inventory_type=2)
                if action <= 0:
                    df.iloc[i, 9] = int(msp)
                    df.iloc[i, 10] = price_revision_in
                    continue
                red_fac = float(action / 100)
                estim_price = round(red_fac * float(msp), -3)
                if estim_price:
                    reduction_price = k * estim_price
                else:
                    reduction_price = min(0.01 * msp, round_down(msp))

                df.iloc[i, 9] = round((msp - reduction_price), -3)
                df.iloc[i, 10] = price_revision_in

        if msp == '':
            if inventory_type_id == 1:
                df.iloc[i, 9] = 'P & S'
                df.iloc[i, 10] = 'P & S'
                df.iloc[i, 4] = 'P & S'
            else:
                df.iloc[i, 9] = ''
                df.iloc[i, 10] = price_revision_in
            continue
    return df


# finds out the state of a listing using its score and z_score_states table
def get_listing_state(df):
    raw_state = df['score'].values
    listing_state = []
    for listing_raw_state in raw_state:
        if listing_raw_state != -999:
            listing_state_df = (sql_result(EXTRACT_STATE.format(listing_raw_state=listing_raw_state)))
            listing_state.append(listing_state_df.iloc[0, 0])
        else:
            listing_state.append(0)
    listing_state = np.array(listing_state)
    listing_state = pd.DataFrame(data=listing_state[:]).rename(columns={0: 'state'})
    df_1 = df.merge(listing_state, left_index=True, right_index=True)
    return df_1


def set_esp(esp, cp):
    temp_df = esp.merge(cp, left_index=True, right_index=True)
    temp_df['esp_data'] = ''
    for index, row in temp_df.iterrows():
        # 19 is the name of estimated_rfc column, 20 is the name of actual_rfc, 23 is the name of proc_price
        try:
            temp_df.loc[index, 'esp_data'] = int(temp_df.loc[index, 49])
        except ValueError:
            temp_df.loc[index, 'esp_data'] = round(int(temp_df.loc[index, 'cp_data']) * 1.15, -3)
    return temp_df['esp_data'].to_frame()


def set_cost_price(actual_rfc, estimated_rfc, proc_price):
    temp_df = estimated_rfc.merge(actual_rfc, left_index=True, right_index=True)
    temp_df = temp_df.merge(proc_price, left_index=True, right_index=True)
    temp_df['cp_data'] = ''
    for index, row in temp_df.iterrows():
        # 19 is the name of estimated_rfc column, 20 is the name of actual_rfc, 23 is the name of proc_price
        try:
            rfc = float(temp_df.loc[index, 20])
        except ValueError:
            rfc = float('0' + temp_df.loc[index, 19])
        temp_df.loc[index, 'cp_data'] = rfc + int('0' + temp_df.loc[index, 23])
    return temp_df['cp_data'].to_frame()


# converts data from proc sheets into a float dataframe
def convert_df(id_data, msp_data, proc_data, estimated_rfc, actual_rfc, esp_data):
    df = id_data.merge(msp_data, left_index=True, right_index=True)
    cp_data = set_cost_price(actual_rfc, estimated_rfc, proc_data)
    df = df.merge(cp_data, left_index=True, right_index=True)
    esp_data = set_esp(esp_data, cp_data)
    df = df.merge(esp_data, left_index=True, right_index=True)
    df = df.rename(columns={2: 'list_id', 26: 'msp_data'})
    df['list_id'] = pd.to_numeric(df['list_id'], errors='coerce')
    df['msp_data'] = pd.to_numeric(df['msp_data'], errors='coerce')
    df['cp_data'] = pd.to_numeric(df['cp_data'], errors='coerce')
    df['esp_data'] = pd.to_numeric(df['esp_data'], errors='coerce')
    return df


def update_map_in_db(df):
    # check for datatype of entries
    for row in df.itertuples():
        existing_entry_check = sql_result(GET_LDP_ENTRY_QUERY.format(listing_id=row.list_id))
        update_dict = {'minimum_approval_price': row.map, 'z_score': row.score}
        if not existing_entry_check.empty:
            for key in update_dict:
                try:
                    float(update_dict[key])
                    sql_result(
                        UPDATE_LDP_MAP_QUERY.format(table=key, update_param=update_dict[key], listing_id=row.list_id))
                except ValueError:
                    sql_result(UPDATE_LDP_MAP_QUERY_NULL.format(table=key, listing_id=row.list_id))


def insert_map_log(df):
    for row in df.itertuples():
        try:
            float(row.map)  # for float values
            source_id = int(SOURCE_NAME_TO_ID_DICT['cron'])
            try:
                float(row.score)
                sql_result(MAP_LOG_INSERT_QUERY_FLOAT.format(listing_id=row.list_id, source_id=source_id, map=row.map,
                                                             z_score=row.score))
            except ValueError:
                sql_result(MAP_LOG_INSERT_QUERY_FLOAT.format(listing_id=row.list_id, source_id=source_id, map=row.map,
                                                             z_score='Null'))
        except ValueError:  # for strings
            continue


def get_days_since_active(df, city_id):
    active_days_df = sql_result(DAYS_SINCE_ACTIVE_TD_QUERY.format(city_id=city_id))
    df = df.merge(active_days_df, left_on='list_id', right_on='listing_id').drop(columns='listing_id')
    df['days_since_active'] = pd.to_numeric(df['days_since_active'], errors='coerce')
    return df


# gets data from procurement sheet and merges with the listing ids whose score has been generated
def procurement_data(cityname, temp_df, city_id):
    procurement_sheet = pd.DataFrame(read_sheet(CITY_PROC_SHEETS['cities'][cityname], CITY_PROC_SHEETS['name']))
    last_row = int(procurement_sheet.iloc[0, 39])
    ps_data = procurement_sheet.iloc[2:last_row, :].copy()
    id_data = ps_data.iloc[:, 2].to_frame()
    estimated_rfc = ps_data.iloc[:, 19].to_frame()
    actual_rfc = ps_data.iloc[:, 20].to_frame()
    msp_data = ps_data.iloc[:, 26].to_frame()
    proc_data = ps_data.iloc[:, 23].to_frame()
    esp_data = ps_data.iloc[:, 49].to_frame()
    proc_df = convert_df(id_data, msp_data, proc_data, estimated_rfc, actual_rfc, esp_data)
    df = temp_df.merge(proc_df, on='list_id', how='left').fillna('')
    df = get_listing_state(df)
    df = get_days_since_active(df, city_id)
    df['map'] = ''
    df['days_next_rev'] = ''
    df = get_map(df, city_id)
    df = df.drop_duplicates(subset='list_id', keep='last').sort_values(by=['list_id'], ascending=True)
    try:
        # adding new MAP row in listing_dynamic_pricings table
        update_map_in_db(df)
        # updating MAP in minimum_approval_price_logs table
        insert_map_log(df)
    except Exception as e:
        add_log(str(e), True)
    final_list = []
    for row1 in df.itertuples():
        final_list.append(
            [row1.list_id, row1.score, row1.days_since_active, row1.days_since_price_rev, row1.days_next_rev,
             row1.msp_data, row1.map])
    return final_list


# sets vb_vd factors in a dictionary
def set_vb_vd_factors(vb_vd):
    corresponding_demand_cluster = 0
    for city_id in CITIES:
        price_slab_list = ['(1, 3)', '(3, 7)', '(7, 50)']
        demand = ['High_Demand', 'Mid_Demand', 'Low_Demand']
        for slab in price_slab_list:
            for model_key in demand:
                VB_VD_DICT[city_id][slab][model_key] = vb_vd[corresponding_demand_cluster]
                corresponding_demand_cluster += 1


def calculate_z_score(list_all_aDa_test, all_active_cars, price_slabs, car_quality_listings):
    for city_id in CITIES:

        cluster_data = CLUSTER_DATA[city_id]

        # testing logic
        list_all_q_test_sub = all_active_cars.loc[all_active_cars['city_id'] == city_id]
        final_scores = []
        latest_dates = []
        low_demand_price_slab = []
        for price_slab_key in list(price_slabs.keys()):
            price_slab = price_slabs[price_slab_key]
            temp_df_1 = list_all_q_test_sub.loc[
                (all_active_cars.price / 10 ** 5 >= price_slab[0]) & (
                        all_active_cars.price / 10 ** 5 < price_slab[1])]

            models = get_demand_model(price_slab)

            [slab_check, temp_df_1] = get_slab_check(price_slab, low_demand_price_slab, temp_df_1)

            if slab_check == 1 or slab_check == 2:
                for model_key in list(models.keys()):
                    # temp_slab = price_slab_key
                    # if price_slab_key == '(0, 1)': temp_slab = '(7, 50)'
                    # final_vb_vd = VB_VD_DICT[city_id][temp_slab][model_key]
                    final_vb_vd = 1

                    model = models[model_key]
                    model_df = temp_df_1.loc[temp_df_1.model_id.isin(model)]

                    temp_df_2 = list_all_aDa_test.loc[list_all_aDa_test.index.intersection(model_df['id'])]

                    if not len(temp_df_2.index):
                        continue

                    temp_df_3 = temp_df_2.apply(
                        lambda x: get_seven_days_leads(x.latest_dates) if len(x.latest_dates) > 0 else False, axis=1)
                    temp_df_4 = dataframe_check(temp_df_3)

                    cluster_final_score, dates = get_z_scores(final_vb_vd, temp_df_4, cluster_data, model_key,
                                                              price_slab_key, car_quality_listings)

                    final_scores += cluster_final_score
                    latest_dates += dates
        final_scores = np.where(np.isnan(final_scores), -999, final_scores)
        latest_dates_df = pd.DataFrame.from_records(latest_dates, columns=['list_id', 'days_since_price_rev'])
        city_name = sql_result(CITY_NAME_QUERY.format(id=city_id)).iloc[0, 0].lower()

        final_df = pd.DataFrame.from_records(final_scores, columns=['list_id', 'score', 'inventory_type'])
        final_df = final_df.merge(latest_dates_df, on='list_id')
        final_scores = procurement_data(city_name, final_df, city_id)
        write_in_sheet(final_scores, Z_SCORE_OUTPUT_CITY_KEYS[city_name], city_name + "!A5:G")


if __name__ == "__main__":

    global CITY_PROC_SHEETS, Z_SCORE_OUTPUT_CITY_KEYS, VB_VD_FACTOR_SHEET_KEY, VB_VD_DICT, SOURCE_NAME_TO_ID_DICT, SENTRY_URL

    SENTRY_URL = config_data['sentry']['url']
    sentry_sdk.init(SENTRY_URL)

    try:
        CITY_PROC_SHEETS = config_data['sheet_keys']['procurement_sheet']
        Z_SCORE_OUTPUT_CITY_KEYS = config_data['sheet_keys']['z_scores']['cities']
        VB_VD_FACTOR_SHEET_KEY = config_data['sheet_keys']['vb_vd_factors_sheet']['cities']
    except Exception as e:
        add_log(str(e), True)

    try:
        cols, price_slabs = instantiate_multi_index_dataframe()
        # VB_VD_DICT = nested_dict()
        # set_vb_vd_factors(get_vb_vd_factors())
        price_changes = get_price_changes(PRICE_CHANGES_QUERY.format(condition="l.source_update != 'cron'"))
        SOURCE_NAME_TO_ID_DICT = get_source_name_from_source_table()[0]

        all_active_cars = sql_result(ACTIVE_CARS_QUERY.format(status_condition="('active')"))
        list_all_aDa_test = get_leads_per_latest_dates(active_cars_df=all_active_cars, price_changes_done=price_changes)

        sold_data = sql_result(SOLD_DATA_QUERY.format(
            start=datetime.date.today() - dateutil.relativedelta.relativedelta(years=1)))
        price_changes = get_price_changes(PRICE_CHANGES_QUERY.format(
            condition=" DATE(l.created_at) NOT IN ('2019-11-18', '2019-10-11', '2019-06-19') "))
        sold_data_aDa = get_sold_cars_active_dates(sold_cars_df=sold_data, price_changes_done=price_changes)

        set_cluster_data_dict(sold_data_aDa=sold_data_aDa, sold_data=sold_data, cols=cols, price_slabs=price_slabs)

        car_quality_issue_listings = get_car_quality_issues_listings()
        calculate_z_score(list_all_aDa_test=list_all_aDa_test, all_active_cars=all_active_cars,
                          price_slabs=price_slabs, car_quality_listings=car_quality_issue_listings)
    except Exception as e:
        add_log(str(e), True)
