## Purpose

This script generates Z Scores and Minimum Approval Prices for all cars everyday.

### Execution

```cd ~/data-science/src/models && python -m map_generator.map_zscore```

#### Note

This script is compatible with python 3.7.2