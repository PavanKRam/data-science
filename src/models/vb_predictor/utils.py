import redis
from dynamic_pricing_code.helpers import config_data
from pickle import loads

REDIS_CONNECTION_URL = config_data['redis']['recommendation']
REDIS_CONNECTION = redis.Redis.from_url(url=REDIS_CONNECTION_URL)


class ModelData:
    user_features = 'user_features'
    item_features = 'item_features'
    trained_model = 'model'
    popular_listings = 'popular_listings'
    max_buyer_id = 'max_buyer_id'
    active_listings = 'active_listings'
    indices_mapping = 'indices_mapping'


def get_redis_data(key):
    return loads(REDIS_CONNECTION.get(key), fix_imports=True, encoding="latin1")


def get_resized_features(model, item_f, user_f):
    """
    Resize item and user features according to model embedding size
    """
    # Existing embedding shape in model
    model_item_features_shape = model.item_biases.shape[0]
    model_user_features_shape = model.user_biases.shape[0]
    item_row_size = item_f.shape[0]
    user_row_size = user_f.shape[0]
    item_f.resize(item_row_size, model_item_features_shape)
    user_f.resize(user_row_size, model_user_features_shape)

    return item_f, user_f


def get_trained_model():
    trained_model = get_redis_data('model')
    user_features = get_redis_data('user_features')
    item_features = get_redis_data('item_features')
    item_features, user_features = get_resized_features(trained_model,
                                                        item_features,
                                                        user_features)
    ModelData.trained_model = trained_model
    ModelData.user_features = user_features
    ModelData.item_features = item_features
    ModelData.max_buyer_id = REDIS_CONNECTION.get('max_buyer_id')
    ModelData.popular_listings = get_redis_data('popular_listings')
    ModelData.active_listings = get_redis_data('active_listings')
    ModelData.indices_mapping = get_redis_data('reco:indices_mapping')
