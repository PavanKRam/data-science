BUYERS_QUERY = """SELECT uniq_buyer.buyer_id, buyers.mobile
FROM (SELECT bl.buyer_id
      FROM buyer_listings bl
      WHERE DATE(bl.created_at) BETWEEN DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND CURDATE()
      UNION
      SELECT blo.buyer_id
      FROM buyer_listing_offers blo
      WHERE DATE(blo.created_at) BETWEEN DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND CURDATE()) AS uniq_buyer
       JOIN buyers ON buyers.id = uniq_buyer.buyer_id
WHERE buyers.city_id = {city_id}"""

CITY_NAME_QUERY = """SELECT name FROM cities WHERE id = {city}"""

VB_UPDATE_QUERY = """UPDATE vb_predictor_logs SET end_time="{end_time}" WHERE id in ({row_ids})"""

VB_INSERT_QUERY = """INSERT INTO vb_predictor_logs(buyer_id, status, start_time) VALUES {data}"""

EXISTING_ENTRY_CHECK_QUERY = """SELECT t1.id, t1.buyer_id,t1.status , t1.end_time FROM vb_predictor_logs t1 
                                JOIN (SELECT buyer_id, MAX(id) id FROM vb_predictor_logs GROUP BY buyer_id) t2 ON 
                                t1.id = t2.id AND t1.buyer_id = t2.buyer_id WHERE t1.buyer_id in ({all_buyers})"""