## Purpose

This script does lead bifurcation, and divides them into Potential or Non-Potential. Scores are generated only for those buyers who have filled crf within last month. It runs four time everyday.

### Execution

```cd ~/data-science/src/models && python -m vb_predictor.vb_predictor```

#### Note

This script is compatible with python 3.7.2