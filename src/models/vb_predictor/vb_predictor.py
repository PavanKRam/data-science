import datetime
import json
import os

import numpy as np
import pandas as pd
import sentry_sdk
from kafka import KafkaProducer

from vb_predictor.constants import EXISTING_ENTRY_CHECK_QUERY, VB_UPDATE_QUERY, VB_INSERT_QUERY, \
    BUYERS_QUERY, CITY_NAME_QUERY

os.environ['SLACK_LOGGING'] = 'TRUE'
os.environ['LOG_FILENAME'] = 'vb_predictor.log'
from vb_predictor.utils import get_trained_model, ModelData
from dynamic_pricing_code.helpers import config_data, write_in_sheet, add_log, sql_result

CITY_IDS = [1, 2, 5]
CITY_POTENTIAL_SCORE = {1: 0.7, 2: 0.7, 5: 0.7}


def get_formatted_list(to_convert_list):
    return ",".join(map(str, to_convert_list))


def get_current_timestamp():
    return str(datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%d %H:%M:%S"))


# Gets list of all active cars in a city
def get_all_active_cars(city_id):
    city_active_inventory_listings = ModelData.active_listings[city_id]['inventory']
    city_active_marketplace_listings = ModelData.active_listings[city_id]['marketplace']
    all_listings = np.append(city_active_inventory_listings, city_active_marketplace_listings)
    return all_listings, city_active_inventory_listings


# Checks that the maximum buyer_id has not null user_features
def check_max_buyer_id(buyers, max_buyer_id):
    buyers.sort(reverse=True)
    index = -1
    for buyer in buyers:
        if buyer > max_buyer_id:
            index = buyers.index(buyer)
    buyers = buyers[index + 1:]
    return buyers


# Returns an array of scores allotted to all cars, according to the prediction of the model
def get_final_scores(all_active_listings_scores):
    all_active_cars_sorted_scores = np.flip(np.sort(all_active_listings_scores))
    final_scores = 1 / (1 + np.exp(-0.7 * all_active_cars_sorted_scores))
    final_scores = np.around(final_scores, decimals=4)
    return final_scores


# Returns the top three inventory car_ids and and their rank in the preference list
def get_top_three_inventory_cars(city_active_inventory_listings, all_active_cars_rank):
    inventory_cars_position = []
    for cars_id in city_active_inventory_listings:
        inventory_cars_position.append(np.where(all_active_cars_rank == cars_id)[0][0])
    inventory_listings_list = city_active_inventory_listings.tolist()

    inventory_listing_rank = [(listing_id, position) for position, listing_id in
                              sorted(zip(inventory_cars_position, inventory_listings_list))]
    top_three_inv = inventory_listing_rank[:3]
    return top_three_inv


'''Divides a lead into potential or non-potential according to the score of topmost inventory listing in its 
preference order '''


def get_potential_of_lead(score, city):
    if score > CITY_POTENTIAL_SCORE[city]:
        potential = 'Potential'
    else:
        potential = 'Not Potential'
    return potential


def get_formatted_columns(data_frame):
    data_frame.current_status = data_frame.current_status.apply("'{}'".format)
    data_frame.current_timestamp = data_frame.current_timestamp.apply("'{}'".format)

    return data_frame


def vb_predictor_logging(buyers_data):
    buyers_data = pd.DataFrame(buyers_data, columns=['buyer_id', 'current_status'])
    buyers_data.current_status = buyers_data.current_status.str.lower().map({'not potential': 'not_potential',
                                                                             'potential': 'potential'})
    current_time = get_current_timestamp()
    buyers_data['current_timestamp'] = current_time
    all_buyers = get_formatted_list(buyers_data['buyer_id'].tolist())

    if buyers_data.shape[0] > 0:
        existing_data = sql_result(EXISTING_ENTRY_CHECK_QUERY.format(all_buyers=all_buyers))
        merged_data = pd.merge(existing_data, buyers_data, on="buyer_id", how="left")

        to_be_updated_ids = merged_data[merged_data['status'] != merged_data['current_status']]
        try:
            if len(to_be_updated_ids) > 0:
                to_be_updated_ids = get_formatted_columns(to_be_updated_ids)
                to_be_inserted_data = get_formatted_list(["({data})".format(data=get_formatted_list(row)) for row in
                                                          to_be_updated_ids[['buyer_id', 'current_status',
                                                                             'current_timestamp']].values.tolist()])
                sql_result(VB_UPDATE_QUERY.format(row_ids=get_formatted_list(to_be_updated_ids.id.values),
                                                  end_time=current_time))
                sql_result(VB_INSERT_QUERY.format(data=to_be_inserted_data, start_time=current_time))

            new_data = buyers_data[~buyers_data['buyer_id'].isin(to_be_updated_ids['buyer_id'])]
            new_data = new_data[~new_data['buyer_id'].isin(existing_data['buyer_id'])]

            if new_data.shape[0] > 0:
                new_data = get_formatted_columns(new_data)
                new_data_to_be_inserted = get_formatted_list(
                    ["({data})".format(data=get_formatted_list(row)) for row in new_data.values.tolist()])
                sql_result(VB_INSERT_QUERY.format(data=new_data_to_be_inserted, start_time=current_time))
        except Exception as e:
            add_log(str(e), True)


def vb_predictor():
    trained_model = ModelData.trained_model
    item_features = ModelData.item_features
    user_features = ModelData.user_features
    max_buyer_id = int(ModelData.max_buyer_id.decode('UTF-8'))
    log_list = []
    stream_data = []
    item_mapping = ModelData.indices_mapping['item_ids_map']
    reverse_mapping = {matrix_index: feature for feature, matrix_index in item_mapping.items()}

    for city in CITY_IDS:
        # Gets a data_frame of buyers via a SQL Query
        df_buyers = sql_result(BUYERS_QUERY.format(city_id=city))

        buyers_list = df_buyers['buyer_id'].tolist()
        buyers_list = check_max_buyer_id(buyers_list, max_buyer_id)
        all_active_listings, city_active_inventory_listings = get_all_active_cars(city)
        active_listing_indices = np.array([item_mapping[listing] for listing in all_active_listings])
        final_list = []

        for buyer in buyers_list:
            matrix_id = ModelData.indices_mapping['user_ids_map'][buyer]
            all_active_listings_scores = trained_model.predict(matrix_id, active_listing_indices,
                                                               user_features=user_features,
                                                               item_features=item_features)
            # Gets rank of all cars according to their scores
            sorted_indices = active_listing_indices[np.argsort(-all_active_listings_scores)]
            all_active_cars_rank = [reverse_mapping[index] for index in sorted_indices]
            final_scores = get_final_scores(all_active_listings_scores)
            top_three_inv = get_top_three_inventory_cars(city_active_inventory_listings, all_active_cars_rank)

            score = final_scores[top_three_inv[0][1]]
            df_mobile = df_buyers.loc[df_buyers.index[df_buyers['buyer_id'] == buyer]]
            mobile_no = int(df_mobile.iloc[0, 1])

            potential_or_not = get_potential_of_lead(score, city)

            final_list.append(
                [mobile_no, score, potential_or_not, int(top_three_inv[0][0]), int(top_three_inv[1][0]),
                 int(top_three_inv[2][0])])
            log_list.append([buyer, potential_or_not])
            stream_data.append([mobile_no, score])

        city_name = sql_result(CITY_NAME_QUERY.format(city=city)).iloc[0, 0].lower()

        write_in_sheet([[str(datetime.date.today())]], VB_PREDICTOR_CITY_KEYS, city_name + "!B1:B1")
        write_in_sheet(final_list, VB_PREDICTOR_CITY_KEYS, city_name + "!A3:F")

    return log_list, stream_data


def dict_to_byte_string(data):
    return bytes(json.dumps(data).encode('utf8'))


def kafka_stream_producer(stream_data):
    producer = KafkaProducer(bootstrap_servers=config_data['kafka']['bootstrap_servers'])
    for mobile, score in stream_data:
        json_data = {
            "mobile": mobile,
            "score": score
        }
        producer.send(topic='vb_predictor', value=dict_to_byte_string(json_data))


if __name__ == "__main__":

    global VB_PREDICTOR_CITY_KEYS, SENTRY_URL
    SENTRY_URL = config_data['sentry']['url']
    sentry_sdk.init(SENTRY_URL)
    VB_PREDICTOR_CITY_KEYS = config_data['sheet_keys']['vb_predictor']['cities']

    try:
        get_trained_model()
        vb_data, vb_stream_data = vb_predictor()
        vb_predictor_logging(vb_data)
        kafka_stream_producer(vb_stream_data)

    except Exception as e:
        add_log(str(e), True)
