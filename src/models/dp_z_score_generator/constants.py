ACTIVE_DURATION_QUERY = """SELECT
                   l.listing_id,
                   CAST(l.changed_at AS DATE) start_date,
                   l.status_to,
                   CAST(CASE
                       WHEN r.changed_at IS NULL THEN NOW()
                       ELSE r.changed_at
                   END AS DATE) AS end_date
               FROM
                   (SELECT
                   @row_no1:=@row_no1 + 1 AS row_no,
                       listing_id,
                       status_to,
                       changed_at
               FROM
                   listing_status_track, (SELECT @row_no1:=0) t
               WHERE
                   listing_id IN {id_list}
               ORDER BY listing_id , changed_at) l
               LEFT JOIN (SELECT
                   @row_no2:=@row_no2 + 1 AS row_no,
                       listing_id,
                       status_to,
                       changed_at
               FROM
                   listing_status_track, (SELECT @row_no2:=0) p
               WHERE
                   listing_id IN {id_list} 
               ORDER BY listing_id , changed_at) r ON l.row_no + 1 = r.row_no
                   AND l.listing_id = r.listing_id"""

EXTRACT_MAXIMUM_PENALTY = """  SELECT t1.action, t1.id 
                                             FROM q_tables t1
                                             WHERE t1.city_id = {city_id} AND cast(t1.z_state 
                                             AS DECIMAL(3,2)) = cast({listing_state} AS DECIMAL(3,2)) 
                                             AND t1.q_value = (SELECT MAX(t2.q_value)
                                                 FROM q_tables t2
                                                 WHERE t2.city_id = t1.city_id AND t2.z_state = t1.z_state 
                                                 AND q_value <> 0)"""

EXTRACT_STATE = """  SELECT state FROM z_score_states 
                                WHERE min_value <= {listing_raw_state} AND max_value > {listing_raw_state} """

LISTING_LEAD_GENERATED_QUERY = """SELECT 
                    DISTINCT
                    TB1.id AS buyer_id,
                    listings.id AS listing_id,
                    CAST(buyer_listings.created_at AS DATE) AS created_at
                FROM
                    (SELECT 
                        id, CAST(created_at AS DATE) AS created_at, mobile
                    FROM
                        buyers
                    WHERE
                        mobile NOT IN (SELECT 
                                mobile
                            FROM
                                dnd_numbers)
                            AND mobile NOT IN (SELECT 
                                mobile
                            FROM
                                invalid_numbers)
                            AND mobile NOT IN (SELECT
                                mobile
                            FROM
                                sellers
                            WHERE
                                type_id NOT IN ('Dealer','Subscriber_Dealer'))) AS TB1,
                    buyer_listings,
                    listings
                WHERE
                    buyer_listings.buyer_id = TB1.id
                        AND buyer_listings.listing_id = listings.id
                        AND listings.id IN {id_list} """

OFFER_LEAD_GENERATED_QUERY = """SELECT 
                    DISTINCT
                    TB1.id AS buyer_id,
                    listings.id AS listing_id,
                    CAST(buyer_listing_offers.created_at AS DATE) AS created_at
                FROM
                    (SELECT 
                        id, CAST(created_at AS DATE) AS created_at, mobile
                    FROM
                        buyers
                    WHERE
                        mobile NOT IN (SELECT 
                                mobile
                            FROM
                                dnd_numbers)
                            AND mobile NOT IN (SELECT 
                                mobile
                            FROM
                                invalid_numbers)
                            AND mobile NOT IN (SELECT
                                mobile
                            FROM
                                sellers
                            WHERE
                                type_id NOT IN ('Dealer','Subscriber_Dealer'))) AS TB1,
                    buyer_listing_offers,
                    listings
                WHERE
                    buyer_listing_offers.buyer_id = TB1.id
                        AND buyer_listing_offers.listing_id = listings.id
                        AND buyer_listing_offers.is_verified = 1
                        AND listings.id IN {id_list}  """

SOLD_DATA_QUERY = """SELECT l.id, 
                                  l.price, 
                                  lo.city_id, 
                                  v.model_id 
                            FROM   listings l 
                                  JOIN variants v 
                                        ON l.variant_id = v.id 
                                  JOIN localities lo 
                                        ON l.locality_id = lo.id 
                            WHERE  l.is_inventory = 1 
                                  AND l.created_at >= '{start}' 
                                  AND l.status IN ( 'truebiled' ) 
                                  AND l.price > 10000"""

ACTIVE_CARS_QUERY = """SELECT listings.id, listings.price, localities.city_id, listings.status, variants.model_id
                           FROM listings, localities, variants
                           WHERE is_inventory = 1
                               AND locality_id = localities.id
                               AND listings.inventory_type_id IN (1, 2)
                               AND listings.variant_id=variants.id
                               AND status in {status_condition}"""

CRM_BUYER_QUERY = """SELECT DISTINCT leads.phone_mobile           AS mobile,
                                      city.name                    AS city,
                                      enj_cstm.visit_donedate_c    AS VD,
                                      enj_cstm.visit_bookingdate_c AS VB
                      FROM leads
                            LEFT JOIN leads_cstm l_c ON leads.id = l_c.id_c
                            LEFT JOIN leads_enj_visit_details_1_c enj_1_c ON leads.id = enj_1_c.`leads_enj_visit_details_1leads_ida`
                            LEFT JOIN enj_visit_details_cstm enj_cstm
                                      ON enj_1_c.`leads_enj_visit_details_1enj_visit_details_idb` = enj_cstm.`id_c`
                            LEFT JOIN enj_visit_details enj ON enj.id = enj_cstm.id_c
                            LEFT JOIN c_city city ON city.id = l_c.c_city_id_c
    
                      WHERE leads.deleted = 0
                            AND enj.deleted = 0
                            AND enj_cstm.visit_bookingdate_c > SUBDATE(CURDATE(), INTERVAL 6 MONTH)
                      ORDER BY leads.phone_mobile, enj_cstm.visit_bookingdate_c, enj_cstm.visit_donedate_c"""

MARKETPLACE_VB_VD_QUERY = """SELECT c.id, 
                        bu.mobile, 
                        {type_of_vb_vd} 
                  FROM   (SELECT DISTINCT a.id, 
                                          a.price, 
                                          a.model_id 
                        FROM   (SELECT DISTINCT b.id, 
                                                l.is_inventory, 
                                                lo.city_id, 
                                                bl.created_at, 
                                                l.price, 
                                                v.model_id 
                        FROM    buyer_listings bl, 
                                buyers b, 
                                listings l, 
                                localities lo, 
                                variants v 
                        WHERE  bl.created_at >= '{start_date}' 
                            AND bl.created_at <= '{end_date}' 
                            AND bl.buyer_id = b.id 
                            AND bl.listing_id = l.id 
                            AND l.locality_id = lo.id 
                            AND v.id = l.variant_id 
                  UNION 
                        SELECT DISTINCT b.id, 
                                        l.is_inventory, 
                                        lo.city_id, 
                                        bl.created_at, 
                                        l.price, 
                                        v.model_id 
                  FROM   buyer_listing_offers bl, 
                          buyers b, 
                          listings l, 
                          localities lo, 
                          variants v 
                  WHERE  bl.created_at >= '{start_date}' 
                       AND bl.created_at <= '{end_date}' 
                       AND bl.buyer_id = b.id 
                       AND bl.listing_id = l.id 
                       AND l.locality_id = lo.id 
                       AND v.id = l.variant_id) a 
              WHERE  a.is_inventory = 0 
                    AND a.id NOT IN (SELECT DISTINCT b.id 
                        FROM   (SELECT DISTINCT b.id, 
                                                l.is_inventory, 
                                                lo.city_id, 
                                                bl.created_at, 
                                                l.price, 
                                                v.model_id 
                              FROM   buyer_listings bl, 
                                      buyers b, 
                                      listings l, 
                                      localities lo, 
                                      variants v 
                                  WHERE  bl.created_at >= '{start_date}' 
                                        AND bl.created_at <= '{end_date}' 
                                        AND bl.buyer_id = b.id 
                                        AND bl.listing_id = l.id 
                                        AND l.locality_id = lo.id 
                                        AND v.id = l.variant_id 
                                  UNION 
                                        SELECT DISTINCT b.id, 
                                                        l.is_inventory, 
                                                        lo.city_id, 
                                                        bl.created_at, 
                                                        l.price, 
                                                        v.model_id 
                                        FROM   buyer_listing_offers bl, 
                                               buyers b, 
                                               listings l, 
                                               localities lo, 
                                               variants v 
                                        WHERE  bl.created_at >= '{start_date}' 
                                               AND bl.created_at <= '{end_date}' 
                                               AND bl.buyer_id = b.id 
                                               AND bl.listing_id = l.id 
                                               AND l.locality_id = lo.id 
                                               AND v.id = l.variant_id) b 
                                WHERE  b.id IN (SELECT d.id 
                                                FROM   (SELECT b.id, 
                                                               l.is_inventory, 
                                                               lo.city_id, 
                                                               bl.created_at, 
                                                               l.price, 
                                                               v.model_id 
                                                        FROM   buyer_listings bl, 
                                                                buyers b, 
                                                                listings l, 
                                                                localities lo, 
                                                                variants v 
                                                        WHERE  bl.created_at >= '{start_date_pre}' 
                                                                AND bl.created_at <= '{end_date}' 
                                                                AND bl.buyer_id = b.id 
                                                                AND bl.listing_id = l.id 
                                                                AND l.locality_id = lo.id 
                                                                AND v.id = l.variant_id 
                                                        UNION 
                                                SELECT b.id, 
                                                        l.is_inventory, 
                                                        lo.city_id, 
                                                        bl.created_at, 
                                                        l.price, 
                                                        v.model_id 
                                                        FROM   buyer_listing_offers bl, 
                                                                buyers b, 
                                                                listings l, 
                                                                localities lo, 
                                                                variants v 
                                                        WHERE  bl.created_at >= '{start_date_pre}' 
                                                                AND bl.created_at <= '{end_date}' 
                                                                AND bl.buyer_id = b.id 
                                                                AND bl.listing_id = l.id 
                                                                AND l.locality_id = lo.id 
                                                                AND v.id = l.variant_id) 
                                                        d 
                                                WHERE  d.is_inventory = 1))) c 
                                JOIN buyers bu 
                                     ON c.id = bu.id 
                                WHERE  city_id = {city} 
"""

INVENTORY_VB_VD_QUERY = """SELECT c.id, 
       bu.mobile,
       {type_of_vb_vd} 
    FROM   (SELECT DISTINCT a.id, 
                            a.price, 
                            a.model_id 
        FROM   (SELECT DISTINCT b.id, 
                                l.is_inventory, 
                                lo.city_id, 
                                bl.created_at, 
                                l.price, 
                                v.model_id 
                FROM   buyer_listings bl, 
                       buyers b, 
                       listings l, 
                       localities lo, 
                       variants v 
                WHERE  bl.created_at >= '{start_date}' 
                       AND bl.created_at <= '{end_date}' 
                       AND bl.buyer_id = b.id 
                       AND bl.listing_id = l.id 
                       AND l.locality_id = lo.id 
                       AND v.id = l.variant_id 
                UNION 
                SELECT DISTINCT b.id, 
                                l.is_inventory, 
                                lo.city_id, 
                                blo.created_at, 
                                l.price, 
                                v.model_id 
                FROM   buyer_listing_offers blo, 
                       buyers b, 
                       listings l, 
                       localities lo, 
                       variants v 
                WHERE  blo.created_at >= '{start_date}' 
                       AND blo.created_at <= '{end_date}' 
                       AND blo.buyer_id = b.id 
                       AND blo.listing_id = l.id 
                       AND l.locality_id = lo.id 
                       AND v.id = l.variant_id) a 
        WHERE  a.id IN (SELECT c.id 
                        FROM   (SELECT b.id, 
                                       l.is_inventory, 
                                       lo.city_id, 
                                       bl.created_at, 
                                       l.price, 
                                       v.model_id 
                                FROM   buyer_listings bl, 
                                       buyers b, 
                                       listings l, 
                                       localities lo, 
                                       variants v 
                                WHERE  bl.created_at >= '{start_date_pre}' 
                                       AND bl.created_at <= '{end_date}' 
                                       AND bl.buyer_id = b.id 
                                       AND bl.listing_id = l.id 
                                       AND l.locality_id = lo.id 
                                       AND v.id = l.variant_id 
                                UNION 
                                SELECT b.id, 
                                       l.is_inventory, 
                                       lo.city_id, 
                                       blo.created_at, 
                                       l.price, 
                                       v.model_id 
                                FROM   buyer_listing_offers blo, 
                                       buyers b, 
                                       listings l, 
                                       localities lo, 
                                       variants v 
                                WHERE  blo.created_at >= '{start_date_pre}' 
                                       AND blo.created_at <= '{end_date}' 
                                       AND blo.buyer_id = b.id 
                                       AND blo.listing_id = l.id 
                                       AND l.locality_id = lo.id 
                                       AND v.id = l.variant_id) c 
                        WHERE  c.is_inventory = 1)) c 
       JOIN buyers bu 
         ON c.id = bu.id 
WHERE  city_id = {city} 
"""

CLUSTER_SUB_QUERY = """ ( CASE 
                                WHEN ( c.price >= 100000 
                                    AND c.price < 300000 ) THEN '(1, 3)' 
                                WHEN ( c.price >= 300000 
                                    AND c.price < 700000 ) THEN '(3, 7)' 
                                ELSE '(7, 50)' 
                            END ) AS price_slab, 
                            ( CASE 
                                WHEN ( c.price >= 100000 
                                    AND c.price < 300000 
                                    AND c.model_id IN ( 546, 513, 480, 471, 464, 248, 217, 212, 210, 209, 184, 177, 
                                                    174, 196 ) ) THEN 'high_demand' 
                                WHEN ( c.price >= 100000 
                                    AND c.price < 300000 
                                    AND c.model_id IN ( 502, 497, 478, 469, 468, 463, 330, 314, 306, 260, 242, 202, 
                                      179, 148, 123, 76, 49, 376 ) ) THEN 'mid_demand' 
                                WHEN ( c.price >= 300000 
                                      AND c.price < 700000 
                                      AND c.model_id IN ( 524, 440, 241, 217, 212, 209, 199, 5, 381 ) ) 
                                      THEN 'high_demand' 
                                WHEN ( c.price >= 300000 
                                      AND c.price < 700000 
                                      AND c.model_id IN ( 521, 515, 513, 480, 471, 468, 461, 374, 363, 310, 249, 205, 
                                      196, 182, 179, 177, 174, 121, 76, 64, 380, 386, 517, 317, 545 ) ) 
                                      THEN 'mid_demand' 
                                WHEN ( c.price >= 700000 
                                      OR c.price < 100000 ) 
                                      AND c.model_id IN ( 496, 379, 314, 205, 167, 137, 132, 477, 554 ) THEN 'high_demand' 
                                WHEN ( c.price >= 700000 
                                      OR c.price < 100000 ) 
                                      AND c.model_id IN ( 517, 440, 301, 249, 241, 182, 152, 104, 
                                    76, 29, 5, 494, 516, 523, 543, 87, 253, 386, 392, 470, 39, 111, 349 ) THEN 'mid_demand' 
                                ELSE 'low_demand' 
                            END ) AS demand_slab"""

PRICE_CHANGES_QUERY = """SELECT 
    l.listing_id,
    l.updated_price AS `updated_price`,
    CAST(l.created_at AS DATE) AS start_date,
    CAST(CASE
            WHEN r.created_at IS NULL THEN NOW()
            ELSE r.created_at
        END
        AS DATE) AS end_date
    FROM
    (SELECT 
        @row_no1:=@row_no1 + 1 AS row_no,
            listing_id,
            updated_price,
            created_at, source_update
    FROM
        listing_price_changes, (SELECT @row_no1:=0) t
    ORDER BY listing_id , created_at) l
        LEFT JOIN
    (SELECT 
        @row_no2:=@row_no2 + 1 AS row_no,
            listing_id,
            updated_price,
            created_at, source_update
    FROM
        listing_price_changes, (SELECT @row_no2:=0) p
    ORDER BY listing_id , created_at) r ON l.row_no + 1 = r.row_no
        AND l.listing_id = r.listing_id WHERE {condition} """

INVENTORY_TYPE_QUERY = """SELECT id, inventory_type_id FROM listings WHERE id in {listing_ids}"""

CITY_NAME_QUERY = """ SELECT name FROM cities WHERE id = {id} """

GET_LDP_ENTRY_QUERY = """SELECT * FROM listing_dynamic_pricings WHERE listing_id = {listing_id} """

UPDATE_LDP_MAP_QUERY = """UPDATE listing_dynamic_pricings SET {table}={update_param} WHERE listing_id={listing_id}"""

UPDATE_LDP_MAP_QUERY_NULL = """UPDATE listing_dynamic_pricings SET {table}=NULL WHERE listing_id={listing_id}"""

MAP_LOG_INSERT_QUERY_FLOAT = """INSERT INTO minimum_approval_price_logs(listing_id, source_id, z_score, minimum_approval_price) VALUES ({listing_id}, {source_id}, {z_score}, {map})"""

CAR_QUALITY_ISSUE_LISTINGS = """SELECT t.visit_created_at, t.listing_id, t.price FROM (
SELECT MAX(bvltdl.created_at) AS visit_created_at, bvl.listing_id AS listing_id, l.price
FROM buyer_visit_listing_test_driven_logs bvltdl
    JOIN buyer_visit_listings bvl ON bvltdl.buyer_visit_listing_id = bvl.id
    JOIN listings l ON bvl.listing_id = l.id
WHERE bvltdl.buyer_visit_listing_test_driven_status_id = 7 AND l.status = 'active'
GROUP BY bvl.listing_id) AS t  WHERE DATEDIFF(CURDATE(), DATE (t.visit_created_at)) >= 3;"""

CAR_QUALITY_CARS_IN_REFURB = """SELECT listing_id, MAX(changed_at) AS refurb_date FROM listing_status_track 
WHERE status_to='refurbishing' AND listing_id IN {listings} GROUP BY listing_id"""

Z_SCORE_SCALE_DOWN_FACTOR = {'(0, 1)': 2.5,
                             '(1, 3)': 2,
                             '(3, 7)': 2.5,
                             '(7, 50)': 2.5}

SUPER_SAVER_INSERT_QUERY = """INSERT INTO supersaver_listings (city_id, listing_id, `rank`) VALUES """
