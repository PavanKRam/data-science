import pandas as pd
from dynamic_pricing_code.helpers import sql_result, sql_result_insert, read_sheet, add_log, config_data

try:
	DP_INPUT_CITY_KEYS = config_data['sheet_keys']['factor_sheet']
	existing_listing_ids = sql_result("SELECT listing_id FROM listing_dynamic_pricings").values.reshape(1,-1)[0]
	z_score_update_query = "UPDATE listing_dynamic_pricings SET z_score = {score} WHERE listing_id = {listing_id}"

	for city_name_lower in ['mumbai', 'delhi', 'bangalore']:
		summarized_set = pd.DataFrame(read_sheet(DP_INPUT_CITY_KEYS['key'], city_name_lower + " results!P4:Q"))
		summarized_set.columns = summarized_set.iloc[0]
		summarized_set = summarized_set.drop(0).reset_index(drop=True)

		intersecting_data = summarized_set[summarized_set['Car ID'].astype('int').isin(existing_listing_ids)]

		for listing_id, score in zip(intersecting_data['Car ID'], intersecting_data['Score1']):
			sql_result_insert(z_score_update_query.format(score=score, listing_id=listing_id))
except Exception as e:
        add_log(str(e), True)