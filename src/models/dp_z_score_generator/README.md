## Purpose

This script generates Z Scores for cars everyday. Scores are generated only for those cars whose days since last price revision is a multiple of 7.

### Execution

```cd ~/data-science/src/models && python -m dp_z_score_generator.dynamic_pricing_score```

#### Note

This script is compatible with python 3.7.2