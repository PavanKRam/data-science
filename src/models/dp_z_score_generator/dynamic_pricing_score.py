import collections
import copy
import datetime
import os
from itertools import zip_longest

import dateutil
import numpy as np
import pandas as pd
import pyarrow as pa
import redis
import sentry_sdk
from scipy import stats

from dp_z_score_generator.constants import ACTIVE_DURATION_QUERY, LISTING_LEAD_GENERATED_QUERY, SOLD_DATA_QUERY, \
    OFFER_LEAD_GENERATED_QUERY, CRM_BUYER_QUERY, MARKETPLACE_VB_VD_QUERY, INVENTORY_VB_VD_QUERY, CLUSTER_SUB_QUERY, \
    PRICE_CHANGES_QUERY, INVENTORY_TYPE_QUERY, Z_SCORE_SCALE_DOWN_FACTOR, CITY_NAME_QUERY, CAR_QUALITY_CARS_IN_REFURB, \
    CAR_QUALITY_ISSUE_LISTINGS, ACTIVE_CARS_QUERY

os.environ['SLACK_LOGGING'] = 'TRUE'
os.environ['LOG_FILENAME'] = 'dynamic_pricing_score.log'
from dynamic_pricing_code.helpers import sql_result, write_in_sheet, config_data, sql_result_crm, sql_result_insert, \
    add_log

DAYS_SLAB = {(3.0, 20.0): 1}
WTS = [1, 1]
WEIGHTS_Z_SCORE = [0.5, 0.7, 0.8, 0.9, 1, 1, 1]
CITIES = [1, 2, 5]
CLUSTER_DATA = {}

high_demand_models_7_50 = [496, 379, 314, 205, 167, 137, 132, 477, 554]
low_demand_models_7_50 = [471, 468, 465, 464, 449, 380, 363, 281, 265, 260, 248, 212, 209, 199, 150, 119, 91, 115,
                          376, 513, 37, 67, 121, 145, 156, 288, 304, 374, 398, 478, 479, 482, 483, 485, 502, 47, 61,
                          83, 292, 303, 354, 411, 480, 515, 519, 546, 548, 3, 7, 14, 26, 27, 41, 70, 89, 107, 124,
                          126, 240, 285, 302, 317, 330, 331, 375, 394, 414, 415, 429, 472, 475, 486, 495, 508, 511,
                          512, 521, 533, 537, 544, 558, 562, 564]
mid_demand_models_7_50 = [517, 440, 301, 249, 241, 182, 152, 104, 76, 29, 5, 494, 516, 523, 543, 87, 253, 386, 392,
                          470, 39, 111, 349]
high_demand_models_1_3 = [513, 480, 471, 464, 248, 217, 212, 210, 209, 184, 177, 174, 196]
low_demand_models_1_3 = [504, 484, 481, 465, 461, 449, 438, 363, 301, 229, 160, 156, 121, 70, 365, 299, 2, 470, 249,
                         494, 567, 496, 477, 490, 512, 483, 159, 482, 331, 479, 510, 302, 304, 281, 310, 387, 546,
                         495, 505]
mid_demand_models_1_3 = [502, 497, 478, 469, 468, 463, 330, 314, 306, 260, 242, 202, 179, 148, 123, 76, 49, 376]
high_demand_models_3_7 = [524, 440, 241, 217, 212, 209, 199, 5, 381]
mid_demand_models_3_7 = [521, 515, 513, 480, 471, 468, 461, 374, 363, 310, 249, 205, 196, 182, 179, 177, 174, 121,
                         76, 64, 380, 386, 517, 317, 545]
low_demand_models_3_7 = [512, 484, 479, 449, 376, 365, 306, 285, 265, 264, 260, 242, 202, 160, 152, 150, 123, 115,
                         70, 61, 49, 2, 354, 7, 304, 159, 504, 482, 438, 156, 546, 67, 229, 544, 497, 132, 124, 167,
                         91, 500, 292, 268, 523, 148, 240, 303, 126, 555, 183, 104, 201]


def nested_dict():
    return collections.defaultdict(nested_dict)


def listToStr(List, sep=',', pre='(', post=')'):
    return pre + sep.join(str(x) for x in List) + post


# get active durations of given listing ids
def activeDuration(idList):
    listStr = listToStr(idList)
    result = sql_result(ACTIVE_DURATION_QUERY.format(id_list=listStr))
    return result


# convert date ranges to list of dates between the range
def dateRangeToDateList(start, end):
    dates = pd.date_range(start, end)
    return set(x.date() for x in dates)


# convert series to set
def agg_dates_func(series):
    return set().union(*series.tolist())


# get active dates of listings Df
def activeDates(actDur, col_start='start_date', col_end='end_date', status='status_to', dates='dates',
                grp=['listing_id'], drp=['active']):
    actDat = actDur.drop(actDur[~actDur[status].isin(drp)].index)
    latest_active_dates = actDat.groupby(grp)[col_start].max()
    actDat.drop([status], axis=1, inplace=True)
    actDat[dates] = actDat.astype('object').apply(lambda x: dateRangeToDateList(x[col_start], x[col_end]),
                                                  axis=1).astype('object')
    actDat.drop([col_start, col_end], axis=1, inplace=True)
    actDat = actDat.groupby(grp).agg(agg_dates_func)
    actDat[dates] = actDat.apply(lambda x: dict.fromkeys(x.dates, 0), axis=1)
    actDat['latest_dates'] = actDat.apply(lambda x: dict(), axis=1)  # new line
    actDat = actDat.merge(latest_active_dates.to_frame(), left_index=True, right_index=True, how="left")
    return actDat


# get crf data for given ids
def listingLeadGenerated(idList):
    listStr = listToStr(idList)
    result = sql_result(LISTING_LEAD_GENERATED_QUERY.format(id_list=listStr))
    return result


# get offer data for given ids
def offerLeadGenerated(idList):
    listStr = listToStr(idList)
    result = sql_result(OFFER_LEAD_GENERATED_QUERY.format(id_list=listStr))
    return result


def vb_vd_count(df2, VB_LIST, start, end):
    df2 = df2.drop_duplicates()
    temp_list = []
    for i in range(len(df2)):
        mobile_no = df2.iloc[i, 1]
        if mobile_no in VB_LIST:
            temp_list.append(mobile_no)

    visit_booked_count = 0
    visit_done_count = 0

    for mob_no in temp_list:
        vb_date = 0
        if any(VB_LIST[mob_no]['vb']):
            for date in VB_LIST[mob_no]['vb']:
                if date is not None and start < date < end:
                    vb_date = date
                    visit_booked_count += 1
                    break
            if vb_date and any(VB_LIST[mob_no]['vd']):
                for date in VB_LIST[mob_no]['vd']:
                    if date is not None and vb_date <= date <= end + dateutil.relativedelta.relativedelta(days=7):
                        visit_done_count += 1
                        break

    return visit_booked_count, visit_done_count


# gets crm data for vb vd calculation
def get_crm_data():
    # Extracts data from the CRM database for visit booking
    # VB_LIST = {{}}  [dictionary of dictionaries]
    # key : Mobile Number
    # Stores all the latest entries if repeated, in a list
    df = sql_result_crm(CRM_BUYER_QUERY)
    VB_LIST = {}
    for i in range(len(df)):
        mob_no = (df.mobile[i])
        temp_df = df[df['mobile'] == mob_no]
        vb = list(temp_df['VB'])
        vd = list(temp_df['VD'])
        VB_LIST[mob_no] = {}
        VB_LIST[mob_no]['vb'] = vb
        VB_LIST[mob_no]['vd'] = vd
    return VB_LIST


# gets vb and vd count for a particular time period
def vb_vd_query(start, end, city_id, inventory):
    if inventory == 0:
        query = MARKETPLACE_VB_VD_QUERY
    elif inventory == 1:
        query = INVENTORY_VB_VD_QUERY

    month = dateutil.relativedelta.relativedelta(days=30)
    df2 = sql_result(query.format(start_date=start, end_date=end, city=city_id, start_date_pre=start - month,
                                  type_of_vb_vd=CLUSTER_SUB_QUERY))
    return df2


def get_cluster_wise_vb_vd(df2, VB_LIST, start, end):
    price_slab = ['(1, 3)', '(3, 7)', '(7, 50)']
    demand = ['high_demand', 'mid_demand', 'low_demand']
    vb = []
    vd = []
    for slab in price_slab:
        for dmd in demand:
            df3 = df2[df2['price_slab'] == slab]
            df4 = df3[df3['demand_slab'] == dmd]
            vb_count, vd_count = vb_vd_count(df4, VB_LIST, start, end)
            vb.append(vb_count)
            vd.append(vd_count)

    return vb, vd


# gets vb_vd of last month
def get_vb_vd(city_id, VB_LIST, SEVEN_DAYS_EARLIER):
    month = dateutil.relativedelta.relativedelta(days=21)

    df_cus_mob_no_inv = vb_vd_query(SEVEN_DAYS_EARLIER - month, SEVEN_DAYS_EARLIER, city_id, 1)
    df_cus_mob_no_mp = vb_vd_query(SEVEN_DAYS_EARLIER - month, SEVEN_DAYS_EARLIER, city_id, 0)
    inv_vb, inv_vd = np.array(
        get_cluster_wise_vb_vd(df_cus_mob_no_inv, VB_LIST, SEVEN_DAYS_EARLIER - month, SEVEN_DAYS_EARLIER))
    mp_vb, mp_vd = np.array(
        get_cluster_wise_vb_vd(df_cus_mob_no_mp, VB_LIST, SEVEN_DAYS_EARLIER - month, SEVEN_DAYS_EARLIER))
    vb = inv_vb
    vd = inv_vd + mp_vd

    df_cus_mob_no_inv = df_cus_mob_no_inv.drop(columns={'price_slab', 'demand_slab'}).drop_duplicates()
    df_cus_mob_no_mp = df_cus_mob_no_mp.drop(columns={'price_slab', 'demand_slab'}).drop_duplicates()
    overall_vb_inv, overall_vd_inv = vb_vd_count(df_cus_mob_no_inv, VB_LIST, SEVEN_DAYS_EARLIER - month,
                                                 SEVEN_DAYS_EARLIER)
    overall_vb_mp, overall_vd_mp = vb_vd_count(df_cus_mob_no_mp, VB_LIST, SEVEN_DAYS_EARLIER - month,
                                               SEVEN_DAYS_EARLIER)
    overall_vb = overall_vb_inv
    overall_vd = overall_vd_inv + overall_vd_mp

    return vd / vb, overall_vd / overall_vb


# gets average of last six months vb_vd
def get_vb_vd_benchmark(city_id, VB_LIST, SEVEN_DAYS_EARLIER):
    bench_mark = []
    fin_benchmark = []
    overall_benchmark = []
    month = dateutil.relativedelta.relativedelta(days=21)

    for counter in range(1, 7):
        end = SEVEN_DAYS_EARLIER - (counter * month)
        start = end - month

        df_cus_mob_no_inv = vb_vd_query(start, end, city_id, 1)
        df_cus_mob_no_mp = vb_vd_query(start, end, city_id, 0)
        inv_vb, inv_vd = np.array(get_cluster_wise_vb_vd(df_cus_mob_no_inv, VB_LIST, start, end))
        mp_vb, mp_vd = np.array(get_cluster_wise_vb_vd(df_cus_mob_no_mp, VB_LIST, start, end))
        vb = inv_vb
        vd = inv_vd + mp_vd

        df_cus_mob_no_inv = df_cus_mob_no_inv.drop(columns={'price_slab', 'demand_slab'}).drop_duplicates()
        df_cus_mob_no_mp = df_cus_mob_no_mp.drop(columns={'price_slab', 'demand_slab'}).drop_duplicates()
        overall_vb_inv, overall_vd_inv = vb_vd_count(df_cus_mob_no_inv, VB_LIST, start, end)
        overall_vb_mp, overall_vd_mp = vb_vd_count(df_cus_mob_no_mp, VB_LIST, start, end)
        overall_vb = overall_vb_inv
        overall_vd = overall_vd_inv + overall_vd_mp

        bench_mark.append(list(vd / vb))
        overall_benchmark.append(overall_vd / overall_vb)
    overall_benchmark = np.mean(np.array(overall_benchmark))
    bench_mark = np.array(bench_mark)
    for counter in range(0, 9):
        fin_benchmark.append(np.mean(bench_mark[:, counter]))
    return fin_benchmark, overall_benchmark


# get all leads data for given ids
def lead_genarated(idList, leadList):
    List = []
    if leadList[0]:
        listingLead = listingLeadGenerated(idList)
        List.append(listingLead)
    if leadList[1]:
        offerLead = offerLeadGenerated(idList)
        List.append(offerLead)
    return List


# count leads per day
def countLeadsPerDate(inputDf, outputDf, wt):
    for row in inputDf.itertuples():
        try:
            outputDf.loc[int(row.listing_id)]['dates'][row.created_at] += wt
        except Exception as e:
            e


# remove outliers from a list
def remove_outliers(List):
    outlier = True
    m = np.mean(stats.trimboth(List, 0.05))
    s = np.std(stats.trimboth(List, 0.05))

    up_List = List
    while outlier:
        List = up_List
        up_List = []
        outlier = False
        for i in List:
            if abs(i - m) > 3 * s:
                outlier = True
            else:
                up_List.append(i)
    return up_List


# manipulate the leads per day
def get_seven_days_leads(day_wise_lead_dict, is_benchmark=False):
    i = 1
    final_dict = dict()

    keys = sorted(day_wise_lead_dict) if is_benchmark else sorted(day_wise_lead_dict)[-7:]

    for row in keys:
        final_dict[i] = day_wise_lead_dict[row]
        i += 1
        if is_benchmark and i > 7:
            break
    final_dict = pd.DataFrame.from_dict(final_dict,
                                        orient = 'index').rolling(min_periods=2, window=3, center=True).sum().to_dict()[0]
    return final_dict


def get_active_days_demand(ids):
    list_all_aDu = activeDuration(ids)
    list_all_aDa = activeDates(list_all_aDu)

    List_Leads = lead_genarated(ids, WTS)

    cou = 0
    for i in range(len(WTS)):
        if WTS[i] != 0:
            countLeadsPerDate(List_Leads[cou], list_all_aDa, wt=WTS[i])
            cou += 1

    return ([list_all_aDu, list_all_aDa])


def get_sold_cars_active_dates(sold_cars_df, price_changes_done):
    [sold_data_aDu, sold_data_aDa] = get_active_days_demand(sold_cars_df['id'].tolist())

    sold_data_aDa['latest_dates'] = "{}"

    # get the active days of the last active price
    for row in sold_data_aDa.itertuples():
        count = 0
        sold_data_aDa.loc[row.Index]['latest_dates'] = dict()
        try:
            for date in row.dates.keys():
                if date > max(sold_data_aDa.loc[row.Index]['start_date'],
                              price_changes_done.loc[row.Index]['start_date']):
                    count += 1
                    sold_data_aDa.loc[row.Index]['latest_dates'][date] = row.dates[date]
        except Exception as e:
            sold_data_aDa.loc[row.Index]['latest_dates'] = row.dates
    return sold_data_aDa


def get_demand_model(row1):
    global HIGH_DEMAND_MODELS, LOW_DEMAND_MODELS, MID_DEMAND_MODELS
    all_models = sql_result("""SELECT id FROM models""")
    all_models_db = list(all_models.values.flatten())

    if row1 == (0, 1) or row1 == (7, 50):
        HIGH_DEMAND_MODELS = high_demand_models_7_50
        MID_DEMAND_MODELS = mid_demand_models_7_50
        LOW_DEMAND_MODELS = low_demand_models_7_50
    if row1 == (1, 3):
        HIGH_DEMAND_MODELS = high_demand_models_1_3
        MID_DEMAND_MODELS = mid_demand_models_1_3
        LOW_DEMAND_MODELS = low_demand_models_1_3
    if row1 == (3, 7):
        HIGH_DEMAND_MODELS = high_demand_models_3_7
        MID_DEMAND_MODELS = mid_demand_models_3_7
        LOW_DEMAND_MODELS = low_demand_models_3_7

    combined_models = copy.deepcopy(HIGH_DEMAND_MODELS)
    combined_models.extend(MID_DEMAND_MODELS)
    combined_models.extend(LOW_DEMAND_MODELS)
    low_demand_models_new = [elements
                             for elements in all_models_db
                             if elements not in combined_models]
    LOW_DEMAND_MODELS.extend(low_demand_models_new)

    model = collections.OrderedDict(
        [('High_Demand', HIGH_DEMAND_MODELS), ('Mid_Demand', MID_DEMAND_MODELS), ('Low_Demand', LOW_DEMAND_MODELS)])
    return model


def instantiate_multi_index_dataframe():
    price_slabs = collections.OrderedDict(
        [('(0, 1)', (0, 1)), ('(1, 3)', (1, 3)), ('(3, 7)', (3, 7)), ('(7, 50)', (7, 50))])

    demand = ['High_Demand', 'Mid_Demand', 'Low_Demand']

    price_slabs_keys = list(price_slabs.keys())[1:]

    stats_metric = ['count', 'mean', 'std']
    cols = pd.MultiIndex.from_product([demand, price_slabs_keys, stats_metric],
                                      names=['model', 'price_slabs_keys', 'metric'])
    return cols, price_slabs


def dataframe_type_conversion(df):
    for col in df.columns:
        df[col] = pd.to_numeric(df[col], errors='coerce', downcast='float')
    return df


def get_price_changes(price_change_query):
    # get get price changes of all listings
    price_changes = sql_result(price_change_query)

    # Last price change done
    price_changes_done = price_changes[(price_changes['end_date'] == datetime.date.today()) & (
            price_changes['start_date'] != datetime.date.today())].set_index(['listing_id'])
    return price_changes_done


def get_slab_check(row1, low_demand_price_slab, temp_df_1):
    slab_check = 0
    if row1 == (0.0, 1.0) or row1 == (7.0, 50.0): low_demand_price_slab.append(temp_df_1)
    if len(low_demand_price_slab) == 2:
        temp_df_1 = low_demand_price_slab[0].append(low_demand_price_slab[1])
        slab_check = 1

    if row1 == (1.0, 3.0): slab_check = 1
    if row1 == (3.0, 7.0): slab_check = 2

    return [slab_check, temp_df_1]


# get active inventory cars
def get_leads_per_latest_dates(active_cars_df, price_changes_done):
    list_all_aDu_test = activeDuration(active_cars_df['id'].tolist())
    list_all_aDa_test = activeDates(list_all_aDu_test)

    List_Leads = lead_genarated(active_cars_df['id'].tolist(), WTS)
    count = 0
    for i in range(len(WTS)):
        if WTS[i] != 0:
            countLeadsPerDate(List_Leads[count], list_all_aDa_test, wt=WTS[i])
            count += 1

    for row in list_all_aDa_test.itertuples():
        count = 0
        list_all_aDa_test.loc[row.Index]['latest_dates'] = dict()
        try:
            for date in row.dates.keys():
                if date > max(list_all_aDa_test.loc[row.Index]['start_date'],
                              price_changes_done.loc[row.Index]['start_date']):
                    count += 1
                    list_all_aDa_test.loc[row.Index]['latest_dates'][date] = row.dates[date]
        except KeyError:
            list_all_aDa_test.loc[row.Index]['latest_dates'] = row.dates
    return list_all_aDa_test


def set_vb_vd_factors():
    global VB_VD_DICT, CURRENT_VB_VD, BENCHMARK, OVERALL_CURRENT_BENCHMARK, CURRENT_BENCHMARK_VB_VD
    VB_VD_DICT = nested_dict()
    CURRENT_VB_VD = []
    BENCHMARK = []
    OVERALL_CURRENT_BENCHMARK = []

    for city_id in CITIES:
        corresponding_demand_cluster = 0
        vb_vd, overall_vb_vd = get_vb_vd(city_id, VB_LIST, SEVEN_DAYS_EARLIER)
        vb_vd_benchmark, overall_vb_vd_benchmark = get_vb_vd_benchmark(city_id, VB_LIST, SEVEN_DAYS_EARLIER)

        OVERALL_CURRENT_BENCHMARK.append([overall_vb_vd * 100, overall_vb_vd_benchmark * 100])

        for element1, element2 in zip(vb_vd, vb_vd_benchmark):
            CURRENT_VB_VD.append(element1 * 100)
            BENCHMARK.append(element2 * 100)

        vb_vd = (vb_vd / vb_vd_benchmark) * 1.05
        price_slabs_list = ['(1, 3)', '(3, 7)', '(7, 50)']
        demand = ['High_Demand', 'Mid_Demand', 'Low_Demand']
        for slab in price_slabs_list:
            for model_key in demand:
                VB_VD_DICT[city_id][slab][model_key] = vb_vd[corresponding_demand_cluster]
                corresponding_demand_cluster += 1

    CURRENT_BENCHMARK_VB_VD = [list(pair) for pair in zip(CURRENT_VB_VD, BENCHMARK)]
    write_in_sheet(CURRENT_BENCHMARK_VB_VD, VB_VD_FACTOR_SHEET_KEY, "vb_vd_factors!D5:E")
    write_in_sheet(OVERALL_CURRENT_BENCHMARK, VB_VD_FACTOR_SHEET_KEY, "vb_vd_factors!D32:E")


def z_score_log(final_scores):
    zipped_data = np.dstack(final_scores).squeeze()

    z_score_update_query = "UPDATE listing_dynamic_pricings SET z_score = {score} WHERE listing_id = {listing_id}"
    z_score_log_query = "INSERT INTO z_score_logs (listing_id,z_score) VALUES ({listing_id}, {score})"

    if zipped_data.size > 0:
        listing_ids = np.ravel(zipped_data[0])
        scores = np.ravel(zipped_data[1])
        for listing_id, score in zip(listing_ids, scores):
            sql_result_insert(z_score_update_query.format(listing_id=listing_id, score=score))
            sql_result_insert(z_score_log_query.format(listing_id=listing_id, score=score))


def print_to_sheet(city_name, final_res_others, final_scores):
    add_log(city_name + " done")

    # sheet updating
    final_scores = np.where(np.isnan(final_scores), -999, final_scores).tolist()
    city_name_lower = city_name.lower()

    write_in_sheet(final_res_others, DP_INPUT_CITY_KEYS['key'], city_name_lower + " results!A2:B4")
    write_in_sheet(final_scores, DP_INPUT_CITY_KEYS['key'], city_name_lower + " results!A7:C")
    try:
        if len(final_scores) > 0:
            z_score_log(final_scores)
    except Exception as e:
        add_log(str(e), True)


def calculate_benchmark_for_z_scores(city_id, sold_data_aDa, cols, price_slabs, sold_data):
    low_demand_price_slab = []
    cluster_data = pd.DataFrame(columns=cols)
    for price_slab_key in list(price_slabs.keys()):
        price_slab = price_slabs[price_slab_key]
        price_wise_listings = sold_data.loc[
            (sold_data.price / 10 ** 5 >= price_slab[0]) & (sold_data.price / 10 ** 5 < price_slab[1]) & (
                    sold_data.city_id == city_id)]

        models = get_demand_model(price_slab)

        [slab_check, price_wise_listings] = get_slab_check(price_slab, low_demand_price_slab,
                                                           price_wise_listings)

        if slab_check == 1 or slab_check == 2:

            for model_key in list(models.keys()):
                model = models[model_key]
                if price_slab_key == '(7, 50)':
                    model_and_price_wise_listings = price_wise_listings['id']
                else:
                    model_and_price_wise_listings = \
                        price_wise_listings[price_wise_listings['model_id'].isin(model)]['id']

                cluster_demand = sold_data_aDa.loc[
                    sold_data_aDa.index.intersection(model_and_price_wise_listings)]

                if cluster_demand.empty:
                    continue

                i = 21
                for slab in DAYS_SLAB.keys():

                    while True:

                        demand_after_last_price_change = cluster_demand.apply(
                            lambda x: get_seven_days_leads(x.latest_dates, True) if len(x.latest_dates) <= i and len(
                                x.latest_dates) >= slab[0] else False, axis=1)

                        demand_after_last_price_change = demand_after_last_price_change[
                            demand_after_last_price_change != False]
                        demand_after_last_price_change = demand_after_last_price_change.to_frame().rename(
                            columns={0: 'dates'})

                        demand_dict = demand_after_last_price_change.to_dict()['dates']
                        demand_values_list = []

                        for listing_row in demand_dict:
                            demand_values_list.append(list(demand_dict[listing_row].values()))

                        if len(demand_values_list) <= 1:
                            continue

                        day_wise_listing_count = []

                        for temp_list in zip_longest(*demand_values_list):
                            day_wise_demand_list = remove_outliers(list(filter(None, temp_list)))
                            day_wise_listing_count.append(len(day_wise_demand_list))

                        i += 1
                        if day_wise_listing_count[6] > 30 or i > 45:
                            break

                    averages = [np.mean(remove_outliers(list(filter(None, day_wise_demand_data)))) for
                                day_wise_demand_data in (zip_longest(*demand_values_list))]
                    dev = [np.std(remove_outliers(list(filter(None, day_wise_demand_data)))) for
                           day_wise_demand_data in (zip_longest(*demand_values_list))]

                    if len(day_wise_listing_count) < 7:
                        day_wise_listing_count.extend([None] * (7 - len(day_wise_listing_count)))
                    if len(averages) < 7:
                        averages.extend([None] * (7 - len(averages)))
                    if len(dev) < 7:
                        dev.extend([None] * (7 - len(dev)))

                    for i in range(1, len(dev)):
                        if dev[i] == 0:
                            dev[i] = dev[i - 1]

                    cluster_data[model_key, price_slab_key, 'count'] = day_wise_listing_count[:7]
                    cluster_data[model_key, price_slab_key, 'mean'] = averages[:7]
                    cluster_data[model_key, price_slab_key, 'std'] = dev[:7]

                    cluster_data = dataframe_type_conversion(cluster_data)
    return cluster_data


def dataframe_check(listings_df):
    temp_df_4 = listings_df[listings_df != False]

    if isinstance(temp_df_4, pd.DataFrame):
        del temp_df_4['latest_dates']
        del temp_df_4['dates']
        temp_df_4 = temp_df_4.rename(columns={0: 'dates'})
    else:
        temp_df_4 = temp_df_4.to_frame().rename(columns={0: 'dates'})
    return temp_df_4


def get_seven_days_eligible_listings(cluster_listings_df):

    temp_df_3 = cluster_listings_df.apply(
        lambda x: get_seven_days_leads(x.latest_dates) if len(x.latest_dates) % 7 == 0 and len(
            x.latest_dates) > 0 else False, axis=1)
    temp_df_3 = temp_df_3[temp_df_3 != False]

    if datetime.datetime.today().weekday() == 6:
        temp_df_friday = cluster_listings_df.apply(
            lambda x: get_seven_days_leads(x.latest_dates) if len(x.latest_dates) / 7 >= 1 and (
                        len(x.latest_dates) % 7 == 1 or len(x.latest_dates) % 7 == 2) else False, axis=1)
        temp_df_friday = temp_df_friday[temp_df_friday != False]
        temp_df_3 = pd.concat([temp_df_3, temp_df_friday])

    temp_df_4 = dataframe_check(temp_df_3)
    return temp_df_4


def get_z_score_wrt_leads(scores, days, total_leads, benchmark_total_leads):
    weights = np.array(WEIGHTS_Z_SCORE[-days:])
    if weights.size != scores.size:
        return np.nan
    weighted_mean = np.average(scores, weights=weights)
    weighted_mean = round(float(weighted_mean), 2)
    if weighted_mean < 0 and total_leads >= benchmark_total_leads:
        mean = np.mean(scores)
        mean = round(float(mean), 2)
        return mean

    return weighted_mean


# appends inventory_type_id into the final dataframe
def get_inventory_type(df):
    df = df.reset_index()
    listing_ids = listToStr(df['index'].tolist())
    inventory_type_df = sql_result(INVENTORY_TYPE_QUERY.format(listing_ids=listing_ids))
    df = df.merge(inventory_type_df, left_on='index', right_on='id').drop(columns='id').set_index('index').rename_axis(
        None)
    return df


def get_z_scores(vb_vd_factor, eligible_listings_df, cluster_data, model_key, price_slab_key, car_quality_listings):
    eligible_listings_df = get_inventory_type(eligible_listings_df)
    z_score_factor = Z_SCORE_SCALE_DOWN_FACTOR[price_slab_key]
    final_scores = []
    dates = []
    for row in eligible_listings_df.itertuples():
        car_quality_factor = z_score_factor if row.Index in car_quality_listings else 1
        ss1 = list(row.dates.values())
        total_leads = sum(ss1)
        ss2 = cluster_data[model_key][price_slab_key]['mean']
        ss2 = list(ss2)
        benchmark_total_leads = sum(ss2)
        ss3 = cluster_data[model_key][price_slab_key]['std']
        ss3 = list(ss3)
        ss1 = ss1[-7:]
        length_of_ss1 = len(ss1)
        if length_of_ss1 < 7:
            ss1[-1] *= 1.5
        try:
            ss2 = ss2[:length_of_ss1]
            ss3 = ss3[:length_of_ss1]

        except KeyError:
            continue

        if np.nan in ss2:
            continue
        mean_std_scores = []
        ss = []

        if len(ss1) == len(ss2):
            for element in ss1:
                ss.append(element * vb_vd_factor)

            for row4 in range(len(ss)):
                # new scores
                if ss3[row4] != 0:
                    mean_std_scores.append(((ss[row4] / car_quality_factor) - ss2[row4]) / ss3[row4])

        final_scores.append([row.Index,
                             get_z_score_wrt_leads(np.array([x for x in mean_std_scores if not np.isnan(x)]),
                                                        len(row.dates), total_leads, benchmark_total_leads),
                             row.inventory_type_id])
        dates.append([row.Index, len(row.dates)])
    return final_scores, dates


def calculate_z_scores(list_all_aDa_test, price_slabs, all_active_cars, car_quality_listings):
    for city_id in CITIES:

        final_res_others = []
        final_res_others.extend([[], ["Date", str(datetime.date.today())]])

        cluster_data = CLUSTER_DATA[city_id]

        # testing logic
        list_all_q_test_sub = all_active_cars.loc[all_active_cars['city_id'] == city_id]
        final_scores = []
        low_demand_price_slab = []
        for price_slab_key in list(price_slabs.keys()):
            price_slab = price_slabs[price_slab_key]
            temp_df_1 = list_all_q_test_sub.loc[
                (all_active_cars.price / 10 ** 5 >= price_slab[0]) & (all_active_cars.price / 10 ** 5 < price_slab[1])]

            models = get_demand_model(price_slab)

            [slab_check, temp_df_1] = get_slab_check(price_slab, low_demand_price_slab, temp_df_1)

            if slab_check == 1 or slab_check == 2:
                for model_key in list(models.keys()):
                    # temp_slab = price_slab_key
                    # if price_slab_key == '(0, 1)': temp_slab = '(7, 50)'
                    # final_vb_vd = VB_VD_DICT[city_id][temp_slab][model_key]
                    final_vb_vd = 1

                    model = models[model_key]
                    k = temp_df_1.loc[temp_df_1.model_id.isin(model)]

                    temp_df_2 = list_all_aDa_test.loc[list_all_aDa_test.index.intersection(k['id'])]
                    if not len(temp_df_2.index):
                        continue

                    seven_days_df = get_seven_days_eligible_listings(temp_df_2)
                    if not len(seven_days_df.index):
                        continue

                    cluster_final_score, dates = get_z_scores(final_vb_vd, seven_days_df, cluster_data, model_key,
                                                              price_slab_key, car_quality_listings)

                    final_scores += cluster_final_score
        city_name = sql_result(CITY_NAME_QUERY.format(id=city_id)).iloc[0, 0]
        final_res_others.extend([[city_name]])
        print_to_sheet(city_name, final_res_others, final_scores)


def set_cluster_data_dict(sold_data_aDa, cols, price_slabs, sold_data):
    for city_id in CITIES:
        cluster_data = calculate_benchmark_for_z_scores(city_id=city_id, sold_data_aDa=sold_data_aDa, cols=cols,
                                                        price_slabs=price_slabs, sold_data=sold_data)
        high_demand_count = cluster_data['High_Demand']['(7, 50)']['count'].values[2:9]
        if any(i <= 3 for i in list(high_demand_count)) or pd.isnull(high_demand_count).any():
            cluster_data['High_Demand', '(7, 50)'] = cluster_data['Mid_Demand', '(7, 50)']

        cluster_data = cluster_data.fillna('')
        CLUSTER_DATA[city_id] = cluster_data


def set_cluster_data_in_sheet():
    redis_connection = redis.Redis().from_url(url=config_data['redis']['recommendation'])
    context = pa.default_serialization_context()
    for city in CITIES:
        cluster_data_list = CLUSTER_DATA[city].values.tolist()
        city_name = sql_result(CITY_NAME_QUERY.format(id=city)).iloc[0, 0].lower()
        write_in_sheet(cluster_data_list, DP_INPUT_CITY_KEYS['key'], city_name + " cluster!C5:AD")
        redis_connection.set(city_name + '_dp_cluster_benchmark',
                             context.serialize(CLUSTER_DATA[city]).to_buffer().to_pybytes())


def get_car_quality_issues_listings():
    bad_quality_listings = sql_result(CAR_QUALITY_ISSUE_LISTINGS)
    bad_quality_listings_list = bad_quality_listings['listing_id'].tolist()
    refurbished_cars_listings = sql_result(
        CAR_QUALITY_CARS_IN_REFURB.format(listings=listToStr(bad_quality_listings_list)))
    merged_df = refurbished_cars_listings.merge(bad_quality_listings, left_on='listing_id', right_on='listing_id')
    refurb_list = merged_df.loc[merged_df['refurb_date'] >= merged_df['visit_created_at']]['listing_id'].tolist()
    refurb_set = set(refurb_list)
    bad_quality_listings_set = set(bad_quality_listings_list)
    eligible_listings_set = bad_quality_listings_set - refurb_set
    return eligible_listings_set


if __name__ == "__main__":

    global SEVEN_DAYS_EARLIER, DP_INPUT_CITY_KEYS, VB_VD_FACTOR_SHEET_KEY, VB_LIST, SENTRY_URL
    SENTRY_URL = config_data['sentry']['url']
    sentry_sdk.init(SENTRY_URL)

    try:
        DP_INPUT_CITY_KEYS = config_data['sheet_keys']['factor_sheet']
        VB_VD_FACTOR_SHEET_KEY = config_data['sheet_keys']['vb_vd_factors_sheet']['cities']
        SEVEN_DAYS_EARLIER = datetime.date.today() - dateutil.relativedelta.relativedelta(days=7)
    except Exception as e:
        add_log(str(e), True)

    try:
        cols, price_slabs = instantiate_multi_index_dataframe()
        # VB_LIST = get_crm_data()
        # set_vb_vd_factors()
        price_changes_done = get_price_changes(PRICE_CHANGES_QUERY.format(condition="l.source_update != 'cron'"))
        sold_data = sql_result(SOLD_DATA_QUERY.format(
            start=datetime.date.today() - dateutil.relativedelta.relativedelta(years=1)))

        all_active_cars = sql_result(ACTIVE_CARS_QUERY.format(status_condition="('active')"))

        list_all_aDa_test = get_leads_per_latest_dates(active_cars_df=all_active_cars,
                                                       price_changes_done=price_changes_done)
        price_changes_done = get_price_changes(PRICE_CHANGES_QUERY.format(
            condition=" DATE(l.created_at) NOT IN ('2019-11-18', '2019-10-11', '2019-06-19') "))
        sold_data_aDa = get_sold_cars_active_dates(sold_cars_df=sold_data, price_changes_done=price_changes_done)

        set_cluster_data_dict(sold_data_aDa=sold_data_aDa, sold_data=sold_data, cols=cols, price_slabs=price_slabs)

        set_cluster_data_in_sheet()

        car_quality_issue_listings = get_car_quality_issues_listings()
        calculate_z_scores(list_all_aDa_test=list_all_aDa_test, price_slabs=price_slabs,
                           all_active_cars=all_active_cars, car_quality_listings=car_quality_issue_listings)
    except Exception as e:
        add_log(str(e), True)
