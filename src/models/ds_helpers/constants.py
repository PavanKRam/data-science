TABLE_UPDATE_QUERY = """UPDATE {table_name} SET end_time="{end_time}" WHERE id IN ({row_ids})"""

TABLE_INSERT_QUERY = """INSERT INTO {table_name}({identifier_name}, {column_name}, start_time) VALUES {data}"""

EXISTING_ENTRY_CHECK_QUERY = """SELECT t1.id, t1.{column_name}, t1.status, t1.end_time FROM {table_name} t1 JOIN 
                                (SELECT {column_name}, MAX(id) id FROM {table_name} GROUP BY {column_name}) t2 ON t1.id 
                                = t2.id AND t1.{column_name} = t2.{column_name} WHERE t1.{column_name} IN ({all_ids})"""

LISTINGS_UPDATE_QUERY = """UPDATE listings SET status='{status}' WHERE id IN ({listing_ids})"""

LISTING_STATUS_TRACK_INSERT_QUERY = """INSERT INTO listing_status_track(listing_id, status_to, source) 
VALUES {insertion_data}"""