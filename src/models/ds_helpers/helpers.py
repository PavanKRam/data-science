import datetime

from ds_helpers.constants import LISTINGS_UPDATE_QUERY, LISTING_STATUS_TRACK_INSERT_QUERY
from dynamic_pricing_code.helpers import sql_result, add_log


def get_current_timestamp():
    return str(datetime.datetime.now(datetime.timezone.utc).strftime('%Y-%m-%d %H:%M:%S'))


def get_formatted_list(to_convert_list):
    return ",".join(map(str, to_convert_list))


def get_formatted_columns(data_frame, columns):
    """
    Returns string column to formatted 'columns'
    """
    for col in columns:
        data_frame[col] = data_frame[col].apply("'{}'".format)
    return data_frame


def get_column_wise_data(data_list, column_name, filter_data):
    """
    Filter data frames on basis of some column value
    """
    city_wise_data = []
    for data_frame in data_list:
        data_frame = data_frame[data_frame[column_name] == filter_data].reset_index(drop=True)
        city_wise_data.append(data_frame)
    return city_wise_data


def listing_status_db_update(listings_df, id_column, new_status):
    """
    Updates listing's status data and create logs
    :param listings_df: DataFrame having values to update
    :param id_column: Name of column with listing ids
    :param new_status: Status to update in DB
    """
    insert_formatted_data = get_formatted_list(['({data})'.format(data=get_formatted_list(data_row))
                                                for data_row in listings_df.values.tolist()])
    update_formatted_data = get_formatted_list(listings_df[id_column].tolist())

    if len(listings_df):
        try:
            sql_result(LISTINGS_UPDATE_QUERY.format(status=new_status, listing_ids=update_formatted_data))
            sql_result(LISTING_STATUS_TRACK_INSERT_QUERY.format(insertion_data=insert_formatted_data))
        except Exception as err:
            add_log(str(err), True)


def get_active_cities_dict():
    """
    :return: Dict of active cities data : {id: name}
    """
    cities_data = sql_result("""SELECT id, name FROM cities WHERE is_active = 1 AND id IN (1, 2, 5)""").set_index(
        'id')
    return cities_data['name'].to_dict()
