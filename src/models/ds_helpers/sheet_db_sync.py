import pandas as pd

from dynamic_pricing_code.helpers import convert_column_data_types, sql_result
from dynamic_pricing_code.constants import ACTIVE_LDP_DATA_QUERY


class SheetDbSync:
    """
    Syncs passed sheet data with that in database. Prioritizes over the sheet
    proc_sheet : procurement sheet passed
    fields_dict: {sheet col location: db col name}
    fields_data_types: {sheet col location: required data type}
    capitalize_columns: list of col locations needed to be capitalized
    """

    def __init__(self, proc_sheet, fields_dict, fields_data_types, capitalize_columns):
        self.proc_sheet = proc_sheet
        self.fields_dict = fields_dict
        self.fields_data_types = fields_data_types
        self.capitalize_columns = capitalize_columns
        self.proc_data = pd.DataFrame()

    def data_cleaning(self, data_frame):
        """
        Does basic data cleaning of type conversion and string capitalization
        :param data_frame: data frame
        :return: cleaned data frame
        """
        for col, data_type in self.fields_data_types.items():
            convert_column_data_types(data_frame, self.fields_dict[col], data_type)

        for col_id in self.capitalize_columns:
            data_frame[self.fields_dict[col_id]] = data_frame[self.fields_dict[col_id]].str.upper()

        return data_frame

    def get_proc_sheet_data(self, inventory_type_id, city_id, update_key='listing_id'):
        """
        Calculates data difference between sheet and db and returns the difference
        :param inventory_type_id: Inventory type Id
        :param city_id: city id
        :param update_key: unique identifier for the table, defaults to `listing_id`
        :return: dataframe of data to be synced
        """
        column_numbers = list(self.fields_dict.keys())
        db_column_names = list(self.fields_dict.values())

        # Get active LDP data and for passed columns fetch sheet data
        active_ldp_data = sql_result(ACTIVE_LDP_DATA_QUERY.format(inventory_type_id=inventory_type_id, city_id=city_id))
        self.proc_data = self.proc_sheet[self.proc_sheet.iloc[:, 2].isin(active_ldp_data[update_key].astype('str').
                                                                         values)][column_numbers]
        self.proc_data.columns = db_column_names
        self.proc_data = self.proc_data.sort_values(update_key).reset_index(drop=True)
        proc_ids = self.proc_data[update_key]
        active_ldp_data = active_ldp_data[active_ldp_data[update_key].isin(proc_ids)][
            db_column_names].reset_index(drop=True)

        # Find data difference between sheet and db and return different rows
        active_ldp_data = self.data_cleaning(active_ldp_data)
        self.proc_data = self.data_cleaning(self.proc_data)
        compared_df = active_ldp_data.where(active_ldp_data.values == self.proc_data.values).notna()
        update_indices = compared_df[~compared_df.all(axis=1)].index
        update_data_df = self.proc_data.loc[update_indices, :]
        update_data_df = update_data_df.fillna('NULL')

        return update_data_df
