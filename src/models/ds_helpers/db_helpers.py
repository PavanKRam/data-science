import pandas as pd

from ds_helpers.constants import EXISTING_ENTRY_CHECK_QUERY, TABLE_INSERT_QUERY, TABLE_UPDATE_QUERY
from ds_helpers.helpers import get_current_timestamp, get_formatted_list, get_formatted_columns
from dynamic_pricing_code.helpers import sql_result, add_log


class DbLogger:
    """
    Logs status changes in database for provided table and status data

    table_name: Name of the table to update
    db_column_changed: Name of column in db to update
    changed_column_name: Status column name in data frame
    identifier_name: Name of the unique identifier
    current_time: Current timestamp

    """

    def __init__(self, changed_data_dict):
        self.table_name = changed_data_dict['table_name']
        self.db_column_changed = changed_data_dict['db_column']
        self.changed_column_name = changed_data_dict['changed_column']
        self.identifier_name = changed_data_dict['identifier_name']
        self.current_time = get_current_timestamp()

    def log_existing_data(self, data_to_be_updated):
        """
        Updates timestamp of previous log and creates a new entry for new status
        :param data_to_be_updated: Update data frame
        """
        if len(data_to_be_updated):
            to_be_inserted_data = get_formatted_list([
                '({data})'.format(data=get_formatted_list(row)) for row in data_to_be_updated[
                    [self.identifier_name, self.changed_column_name, 'current_timestamp']].values.tolist()])
            sql_result(TABLE_UPDATE_QUERY.format(row_ids=get_formatted_list(data_to_be_updated.id.values),
                                                 end_time=self.current_time, table_name=self.table_name))
            sql_result(TABLE_INSERT_QUERY.format(data=to_be_inserted_data, table_name=self.table_name,
                                                 column_name=self.db_column_changed,
                                                 identifier_name=self.identifier_name))

    def log_new_data(self, data_to_be_inserted):
        """
        Inserts status data for identifier if not already available
        :param data_to_be_inserted: Insertion data frame
        """
        if len(data_to_be_inserted):
            data_to_be_inserted = data_to_be_inserted[['listing_id', 'current_status', 'current_timestamp']]
            new_data_to_be_inserted = get_formatted_list(
                ['({data})'.format(data=get_formatted_list(row)) for row in data_to_be_inserted.values.tolist()])
            sql_result(TABLE_INSERT_QUERY.format(data=new_data_to_be_inserted, start_time=self.current_time,
                                                 table_name=self.table_name, column_name=self.db_column_changed,
                                                 identifier_name=self.identifier_name))

    def db_status_logging(self, table_df):
        """
        Logs status changes for an identifier in database
        :param table_df: Dataframe having data to be updated
        """
        add_log("LISTINGS STATUS LOGGING STARTED")
        table_df['current_timestamp'] = self.current_time
        columns_to_format = ['current_status', 'current_timestamp']
        table_df = get_formatted_columns(table_df, columns_to_format)
        all_ids = get_formatted_list(table_df[self.identifier_name].tolist())

        if len(table_df):
            existing_data = sql_result(EXISTING_ENTRY_CHECK_QUERY.format(column_name=self.identifier_name,
                                                                         table_name=self.table_name, all_ids=all_ids))
            merged_data = pd.merge(existing_data, table_df, on=self.identifier_name, how='left')
            merged_data = get_formatted_columns(merged_data, [self.db_column_changed])
            to_be_updated_ids = merged_data[merged_data[self.db_column_changed] !=
                                            merged_data[self.changed_column_name]]
            try:
                new_data = table_df[~table_df[self.identifier_name].isin(to_be_updated_ids[self.identifier_name])]
                new_data = new_data[~new_data[self.identifier_name].isin(existing_data[self.identifier_name])]
                self.log_existing_data(to_be_updated_ids)
                self.log_new_data(new_data)
            except Exception as e:
                add_log(str(e), True)
        add_log("LISTINGS STATUS LOGGING COMPLETED")
