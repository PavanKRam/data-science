import redis
from dynamic_pricing_code.helpers import config_data, add_log


class RedisManager:
    """Connection to redis for data fetching and modifying values """
    __redis = 0

    def __init__(self, connection_type):
        self.connection_url = config_data['redis'][connection_type]
        self.__redis = self.get_redis_connection()

    def get_redis_connection(self):
        try:
            pool = redis.ConnectionPool().from_url(self.connection_url)
            redis_con = redis.Redis(connection_pool=pool)
            return redis_con
        except Exception as e:
            add_log(str(e), True)

    def get_value(self, key):
        try:
            value = self.__redis.hgetall(key)
            return value
        except Exception as e:
            add_log(str(e), True)

    def set_value(self, key, value):
        try:
            self.__redis.set(key, value)
        except Exception as e:
            add_log(str(e), True)
